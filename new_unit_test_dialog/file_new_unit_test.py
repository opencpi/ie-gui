# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_unit_test_dialog/new_unit_test_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_UnitTest_Dialog(object):
    def setupUi(self, New_UnitTest_Dialog):
        New_UnitTest_Dialog.setObjectName("New_UnitTest_Dialog")
        New_UnitTest_Dialog.resize(400, 228)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_UnitTest_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_2 = QtWidgets.QLabel(New_UnitTest_Dialog)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_4.addWidget(self.label_2)
        self.Project_comboBox = QtWidgets.QComboBox(New_UnitTest_Dialog)
        self.Project_comboBox.setObjectName("Project_comboBox")
        self.horizontalLayout_4.addWidget(self.Project_comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_5 = QtWidgets.QLabel(New_UnitTest_Dialog)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_5.addWidget(self.label_5)
        self.Library_comboBox = QtWidgets.QComboBox(New_UnitTest_Dialog)
        self.Library_comboBox.setObjectName("Library_comboBox")
        self.horizontalLayout_5.addWidget(self.Library_comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_6 = QtWidgets.QLabel(New_UnitTest_Dialog)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_6.addWidget(self.label_6)
        self.Component_Spec_comboBox = QtWidgets.QComboBox(New_UnitTest_Dialog)
        self.Component_Spec_comboBox.setObjectName("Component_Spec_comboBox")
        self.horizontalLayout_6.addWidget(self.Component_Spec_comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_UnitTest_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_UnitTest_Dialog)
        self.buttonBox.accepted.connect(New_UnitTest_Dialog.accept)
        self.buttonBox.rejected.connect(New_UnitTest_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_UnitTest_Dialog)

    def retranslateUi(self, New_UnitTest_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_UnitTest_Dialog.setWindowTitle(_translate("New_UnitTest_Dialog", "New Unit Test"))
        self.label_2.setText(_translate("New_UnitTest_Dialog", "Associated Project"))
        self.label_5.setText(_translate("New_UnitTest_Dialog", "Associated Library"))
        self.label_6.setText(_translate("New_UnitTest_Dialog", "Component Spec"))

