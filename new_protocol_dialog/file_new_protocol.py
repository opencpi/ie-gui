# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_protocol_dialog/new_protocol_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_Protocol_Dialog(object):
    def setupUi(self, New_Protocol_Dialog):
        New_Protocol_Dialog.setObjectName("New_Protocol_Dialog")
        New_Protocol_Dialog.resize(400, 182)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_Protocol_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(New_Protocol_Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.Protocol_Name_lineEdit = QtWidgets.QLineEdit(New_Protocol_Dialog)
        self.Protocol_Name_lineEdit.setObjectName("Protocol_Name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.Protocol_Name_lineEdit)
        self.label_2 = QtWidgets.QLabel(New_Protocol_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Project_comboBox = QtWidgets.QComboBox(New_Protocol_Dialog)
        self.Project_comboBox.setObjectName("Project_comboBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Project_comboBox)
        self.Library_comboBox = QtWidgets.QComboBox(New_Protocol_Dialog)
        self.Library_comboBox.setObjectName("Library_comboBox")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.Library_comboBox)
        self.label_3 = QtWidgets.QLabel(New_Protocol_Dialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_Protocol_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_Protocol_Dialog)
        self.buttonBox.accepted.connect(New_Protocol_Dialog.accept)
        self.buttonBox.rejected.connect(New_Protocol_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_Protocol_Dialog)

    def retranslateUi(self, New_Protocol_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_Protocol_Dialog.setWindowTitle(_translate("New_Protocol_Dialog", "New Protocol"))
        self.label.setText(_translate("New_Protocol_Dialog", "Protocol Name"))
        self.label_2.setText(_translate("New_Protocol_Dialog", "Associated Project"))
        self.label_3.setText(_translate("New_Protocol_Dialog", "Associated Library"))

