# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './grc_paths_dialog/GRC_path_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GRC_Configuration(object):
    def setupUi(self, GRC_Configuration):
        GRC_Configuration.setObjectName("GRC_Configuration")
        GRC_Configuration.resize(400, 188)
        self.verticalLayout = QtWidgets.QVBoxLayout(GRC_Configuration)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(GRC_Configuration)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.label_2 = QtWidgets.QLabel(GRC_Configuration)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.lineEdit_2 = QtWidgets.QLineEdit(GRC_Configuration)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.verticalLayout.addWidget(self.lineEdit_2)
        self.toolButton_2 = QtWidgets.QToolButton(GRC_Configuration)
        self.toolButton_2.setObjectName("toolButton_2")
        self.verticalLayout.addWidget(self.toolButton_2)
        self.buttonBox = QtWidgets.QDialogButtonBox(GRC_Configuration)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(GRC_Configuration)
        self.buttonBox.accepted.connect(GRC_Configuration.accept)
        self.buttonBox.rejected.connect(GRC_Configuration.reject)
        QtCore.QMetaObject.connectSlotsByName(GRC_Configuration)

    def retranslateUi(self, GRC_Configuration):
        _translate = QtCore.QCoreApplication.translate
        GRC_Configuration.setWindowTitle(_translate("GRC_Configuration", "GRC Configuration"))
        self.label_3.setText(_translate("GRC_Configuration", "Set the path for the Python3 libraries."))
        self.label_2.setText(_translate("GRC_Configuration", "PYTHONPATH"))
        self.toolButton_2.setText(_translate("GRC_Configuration", "..."))

