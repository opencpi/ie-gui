#!/usr/bin/python3
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
OpenCPI GUI is a graphical interface for the command-line tool OpenCPI, itself a companion to GNU Radio. The GUI is
built on Qt5 and Python 3.6.

To run the application, type 'python3 OpenCPI_GUI.py' in the main OpenCPI GUI directory.
"""
from distutils.command.config import LANG_EXT
import json
import os
from posixpath import split
import subprocess
import sys
import time
import uuid
import webbrowser
import xml.etree.ElementTree as ET
import pathlib

is_ocpi_source = True

try:
    import _opencpi.util as ocpiutil
except ModuleNotFoundError as e:
    is_ocpi_source = False

from functools import partial
from os import PathLike
from pathlib import Path, PurePath
from subprocess import CompletedProcess
from typing import Type, Union, Optional, Tuple, List, Any, Dict, TextIO, Sequence

import logo_rc  # Import Qt resource logo_rc.py for About menu
from PyQt5.QtCore import QProcess, QProcessEnvironment, QCoreApplication, QModelIndex, Qt, QObject, pyqtSignal, QRect, \
    QPoint, QTextStream, QRunnable, pyqtSlot, QThreadPool, QAbstractListModel, QTimer, QItemSelectionModel, QEvent
from PyQt5.QtGui import QCursor, QTextCursor, QStandardItemModel, QStandardItem, QPixmap, QColor, QBrush, QPen, QIcon, \
    QPainter, QTextDocument, QCloseEvent
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QFileDialog, QStatusBar, QAction, QLineEdit, \
    QToolButton, QCheckBox, QGroupBox, QComboBox, QButtonGroup, QMessageBox, QFileSystemModel, QTreeView, QMenu, \
    QTextEdit, QWidget, QListView, QSplashScreen, QProgressBar, QDesktopWidget, QStyledItemDelegate, QStyle, \
    QPlainTextEdit, QPushButton, QLabel, QTextBrowser, QTreeWidget, QTreeWidgetItem, QStyleFactory, QDialogButtonBox

from Qt_OpenCPI import Ui_OpenCPI  # Main application
from grc_paths_dialog.GRC_paths import Ui_GRC_Configuration
from new_application_dialog.file_new_application import Ui_New_Application_Dialog
from new_component_dialog.file_new_component import Ui_New_Component_Dialog
from new_filename_dialog.file_new_filename import Ui_New_Filename_Dialog
from new_hdl_assembly.file_new_hdl_assembly import Ui_New_HDL_Assembly_Dialog
from new_hdl_platform_dialog.file_new_hdl_platform import Ui_New_HDL_Platform_Dialog
from new_hdl_prim_core_dialog.file_new_prim_core import Ui_New_HDL_Prim_Core_Dialog
from new_hdl_prim_library.file_new_hdl_prim_library import Ui_New_HDL_Prim_Library_Dialog
from new_library_dialog.file_new_library import Ui_New_Library_Dialog
from new_project_dialog.file_new_project import Ui_New_Project_Dialog
from new_protocol_dialog.file_new_protocol import Ui_New_Protocol_Dialog
from new_registry_dialog.file_new_registry import Ui_New_Registry_Dialog
from new_unit_test_dialog.file_new_unit_test import Ui_New_UnitTest_Dialog
from new_worker_dialog.file_new_worker import Ui_New_Worker_Dialog
from ocpi_source.ocpi_gui import Ui_Ocpi_Setup_Configuration
from set_registry_dialog.set_registry import Ui_Set_Reg_Dialog
from run_arguments_dialogue.run_arguments_dialogue import Ui_Run_Application_Arguments
from new_project_path.Qt_project_path import Ui_Project_Path_Dialog
from deploy_platform_dialog.Qt_deploy_dialog import Ui_Deploy_Platform_Dialog
from installation_info_dialog.config_info import Ui_Installation_Info_Dialog
from install_platform_dialog.Qt_install_dialog import Ui_Install_Platform_Dialog
from about_window.file_about import Ui_About_Dialog
from new_project_path.Qt_project_path import Ui_Project_Path_Dialog

# Status information for job manager
STATUS_WAITING = "waiting"
STATUS_RUNNING = "running"
STATUS_ERROR = "error"
STATUS_COMPLETE = "complete"
STATUS_STOPPED = "stopped"

STATUS_COLORS = STATUS_COLORS_DEFAULT = {
    STATUS_RUNNING: "#DDCC77",
    STATUS_ERROR: "#CC6677",
    STATUS_STOPPED: "#cccccc",
    STATUS_COMPLETE: "#44AA99",
}

STATUS_COLORS_ALT = {
    STATUS_RUNNING: "#F7F7F7",
    STATUS_ERROR: "#F4A582",
    STATUS_STOPPED: "#cccccc",
    STATUS_COMPLETE: "#92C5DE",
}

STATUS_CHARACTERS = {
    STATUS_RUNNING: '\u29D7',
    STATUS_ERROR: '\u2716',
    STATUS_STOPPED: '\u25A0',
    STATUS_COMPLETE: '\u2714'
}

DEFAULT_STATE = {"progress": 0, "status": STATUS_WAITING}


def ocpidev(caller_obj, verb, noun=None, name=None, options=None, 
    do_log=True) -> Union[CompletedProcess, None]:
        """Runs an ocpidev cmd in a subprocess
        
        Will log stdout and cmd run unless 'do_log' is set to False
        """
        cmd = f"ocpidev {verb} "
        if noun is not None:
            cmd += f"{noun} "
        if name is not None:
            if not isinstance(caller_obj, ProjectExplorerTree):
                cmd += f"{name} "
        if options is not None:
            for key, val in options.items():
                if val is True:
                    cmd += f"{key} "
                elif isinstance(val, list):
                    for item in val:
                        cmd += f"{key} {item} "
                elif val:
                    cmd += f"{key} {val} "
        env = os.environ.copy()
        env['TERM'] = 'tty'
        if isinstance(caller_obj, ProjectExplorerTree): 
            if verb == "build":
                caller_obj.start(["bash", "-c", cmd], name)
                command: str = caller_obj.parse_subprocess_cmd(cmd)
                caller_obj.console.on_update_text(command)
            else:
                out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                     subprocess.run(["bash", "-c", cmd],
                                     stderr=subprocess.PIPE, stdout=subprocess.PIPE, 
                                     universal_newlines=True)
                command: str = caller_obj.parse_subprocess_cmd(cmd)
                caller_obj.console.on_update_text(out.stdout)  # Send command output to console
                caller_obj.console.on_update_text(out.stderr)
                caller_obj.console.on_update_text(command)
            return
        else:
            out = subprocess.run(["bash", "-c", cmd],
                                 stderr=subprocess.PIPE, 
                                 stdout=subprocess.PIPE, 
                                 universal_newlines=True, 
                                 env=env)
            if do_log is True:
            # Log cmd run and the stdout to the console
                caller_obj.console_output.on_update_text(out.stdout)
                caller_obj.console_output.on_update_text(out.stderr)
                cmd = "COMMAND RAN: " + cmd
                caller_obj.console_output.on_update_text(cmd)

            return out


def ocpidev_show(caller_obj, noun, options=None, delimiter='\n') -> Union[List[str], dict]:
    """Returns assets specified by 'noun'

    Returns a diction if '--json: True' is in options; otherwise,
    returns a list of asset names.
    Calls ocpidev() to run an 'ocpidev show' command in a subprocess
    Set the delimiter to dictate what the list should be split on.
    """
    if not options or not isinstance(options, dict):
        options = {'--simple': True}
    elif '--simple' not in options and '--json' not in options:
        options['--simple'] = True
    out = ocpidev(caller_obj, 'show', noun, options=options, do_log=False).stdout
    if out:
        if '--json' in options and options['--json'] is True:
            assets = json.loads(out)
            if noun == 'components':
                for component, info in list(assets.items()):
                    path = Path(info['directory'])
                    if path.suffix == '.comp':
                        name = path.stem + '-comp.xml'
                        comp_xml = path.joinpath(name)
                        assets[component]['directory'] = str(comp_xml)
        else:
            out = out.split(delimiter)
            assets = [asset.strip() for asset in out if asset.strip()]
    else:
        assets = []

    return assets


class MainWindow(QMainWindow, Ui_OpenCPI):
    """Primary OpenCPI GUI application

    Handles all the primary GUI operations and calls to external support classes and modules.
    """
    config_path: Path
    test_target: str
    user_proj_path: str
    themes: List[QAction]
    deploy_platform: QAction
    user_settings: QAction
    install_settings: QAction
    proj_path: QAction
    clean_group: QButtonGroup
    launch_test: QPushButton
    clear_jobs: QPushButton
    stop_job: QPushButton
    refresh_rcc: QPushButton
    refresh_hdl: QPushButton
    reg_path: str
    theme: str
    help_docs: QAction
    exit_app: QAction
    new_registry: QAction
    new_unit_test: QAction
    new_hdl_prim_core: QAction
    new_hdl_platform: QAction
    new_hdl_prim_lib: QAction
    new_hdl_assembly: QAction
    new_protocol: QAction
    new_worker: QAction
    source_config: QAction
    ocpi_path: str
    new_component: QAction
    new_library: QAction
    new_project: QAction
    new_application: QAction
    toolbar_open_file: QAction
    menu_open: QAction
    grc: QAction
    opened_file: str
    python: str
    ocpi: str
    proc: Optional[Union[QProcess, QProcess]]
    grc_config: QAction

    def __init__(self) -> None:
        """Initialization of MainWindow GUI class

        Handles the launching of configuration dialog boxes to get the user's /opencpi path and the project path to
        initially show in Project Explorer. The program will first look for a config file w/ the paths; if the config
        file is missing, then the dialog boxes are shown to the user to input the paths.

        Sets up the RCC and HDL platform lists, if not already set in config file.

        Creates instances of main widgets in the GUI: Project Explorer, RCC/HDL Targets, Unit Tests, Job Manager, and
        Output Console.

        Creates and handles function call for GUI menu bar and status bar.
        """
        super().__init__()
        self.splash = ""
        self.theme = ""
        self.proc = None
        self.ocpi = ""
        self.python = ""
        self.opened_file = ""
        self.ocpi_path = ""
        self.reg_path = ""
        self.user_proj_path = ""
        self.hdl_config_list = ""
        self.rcc_config_list = ""
        self.test_target = ""

        self.config_path = Path(__file__).resolve().parent.joinpath("opencpi_config.json")

        if 'OCPI_CDK_DIR' not in os.environ or not is_ocpi_source:
            QMessageBox.warning(self, "OpenCPI Not Set Up", "Run the command 'source cdk/opencpi-setup.sh -r'"
                                    " in the opencpi/ directory before attempting to start the GUI.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            sys.exit("Run the command 'source cdk/opencpi-setup.sh -r'"
                    " in the opencpi/ directory before attempting to start the GUI.")
        # Launch /opencpi dialog on startup
        if self.config_path.exists():
            with self.config_path.open() as config_file:
                data = json.load(config_file)  # Get the JSON data
                self.ocpi_path = data["OPENCPI_DIR"]  # Get opencpi_dir path
                self.user_proj_path = data["PROJECT_EXPLORER_PATH"]  # Get project path
        else:
            if "--ocpi-path" not in sys.argv:
                self.ocpi_install_dir()  # Get new opencpi_dir path
            else:
                index = sys.argv.index('--ocpi-path')
                try:
                    self.ocpi_path = sys.argv[index + 1]
                except IndexError:
                    print("Usage: ocpigui [OPTIONS]\n"
                        "\tprojects-path (string): path to the top level projects folder.\n"
                        "\t--ocpi-path (string): path to the opencpi installation.")
                    sys.exit()
                self.ocpi_dir_save()

        self.create_hdl_list()  # Generate HDL platform list
        self.create_rcc_list()  # Generate RCC platform list
        self.setupUi(self)
        self.center()  # Put main window in center of screen
        path = Path(__file__).resolve().parent.joinpath("doc", "logo_ocpi.png")
        self.setWindowIcon(QIcon(str(path)))
        self.Current_opencpi_Dir_label.setText(self.ocpi_path)  # Display initial startup path to /opencpi

        # Create GUI main panels
        self.job_console: JobQueue = JobQueue(self, self.Job_Queue_listView)
        self.console_output: OutputConsole = OutputConsole(self.Console_Log_plainTextEdit)
        self.rcc_targets: RccTargets = RccTargets(self, self.RCC_Target_listView, self.RCC_Platforms_groupBox,
                                                  self.rcc_config_list)
        self.hdl_targets: HdlTargets = HdlTargets(self, self.HDL_Targets_listView, self.HDL_Platforms_groupBox,
                                                  self.hdl_config_list)
        self.refresh_rcc = self.Refresh_RCC_Platforms
        self.refresh_rcc.clicked.connect(self.refresh_rcc_platforms)
        self.refresh_hdl = self.Refresh_HDL_Platforms
        self.refresh_hdl.clicked.connect(self.refresh_hdl_platforms)
        self.project: ProjectExplorerTree = ProjectExplorerTree(self, self.Proj_Explorer_treeView, self.console_output,
                                                                self.rcc_targets, self.hdl_targets)

        # Job Manager panel
        self.stop_job = self.Stop_Job_pushButton
        self.stop_job.clicked.connect(self.job_console.stop_worker)
        self.clear_jobs = self.Clear_List_pushButton
        self.clear_jobs.clicked.connect(self.job_console.workers.cleanup)

        # Unit Test panel
        self.launch_test = self.Launch_UT_pushButton
        self.clean_group = self.UT_Clean_buttonGroup
        self.launch_test.clicked.connect(self.unit_test)

        # GRC configuration and execution
        self.grc_config = self.actionConfigur_GRC
        self.grc_config.triggered.connect(self.get_grc_config)
        self.grc = self.actionLaunch_GRC
        self.grc.triggered.connect(self.start_grc_process)

        # Node Graph Menu
        self.node_graph = self.actionLaunch_Node_Graph
        self.node_graph.triggered.connect(self.launch_node_graph)

        # Menu bar items
        # Configuration menu
        self.proj_path = self.actionSet_project_path
        self.proj_path.triggered.connect(self.set_proj_path)

        self.source_config = self.actionSet_OCPI_source
        self.source_config.triggered.connect(self.ocpi_install_dir)

        self.install_settings = self.actionView_installation_settings
        self.install_settings.triggered.connect(self.curr_ocpi_settings)

        self.user_settings = self.actionConfigure_User_Settings
        self.user_settings.triggered.connect(self.set_user_settings)

        self.install_platform = self.actionInstall_platform
        self.install_platform.triggered.connect(self.ocpiadmin_install)

        self.deploy_platform = self.actionDeploy_platform
        self.deploy_platform.triggered.connect(self.ocpiadmin_deploy)

        self.exit_app = self.actionExit
        self.exit_app.triggered.connect(self.exit_application)

        # Create menu
        self.new_project = self.actionNew_Project_3
        self.new_project.triggered.connect(self.create_new_project)

        self.new_library = self.actionNew_Library_2
        self.new_library.triggered.connect(self.create_new_library)

        self.new_application = self.actionNew_Application_2
        self.new_application.triggered.connect(self.create_new_application)

        self.new_component = self.actionNew_Component_2
        self.new_component.triggered.connect(self.create_new_component)

        self.new_worker = self.actionNew_Worker_2
        self.new_worker.triggered.connect(self.create_new_worker)

        self.new_protocol = self.actionNew_Protocol_2
        self.new_protocol.triggered.connect(self.create_new_protocol)

        self.new_hdl_assembly = self.actionNew_HDL_Assembly_2
        self.new_hdl_assembly.triggered.connect(self.create_new_hdl_assembly)

        self.new_hdl_platform = self.actionNew_HDL_Platform_2
        self.new_hdl_platform.triggered.connect(self.create_new_hdl_platform)

        self.new_hdl_prim_lib = self.actionNew_HDL_Primitive_Library_2
        self.new_hdl_prim_lib.triggered.connect(self.create_new_hdl_prim_lib)

        self.new_hdl_prim_core = self.actionNew_HDL_Primitive_Core_2
        self.new_hdl_prim_core.triggered.connect(self.create_new_hdl_prim_core)

        self.new_unit_test = self.actionNew_Unit_Test_2
        self.new_unit_test.triggered.connect(self.create_new_unit_test)

        self.new_registry = self.actionNew_Registry
        self.new_registry.triggered.connect(self.create_new_registry)

        # Help menu
        self.help_docs = self.actionHelp_Contents
        self.help_docs.triggered.connect(self.documentation)

        self.help_report = self.actionReport_Bu_Enhancement
        self.help_report.triggered.connect(self.bug_report)

        self.about_panel = self.actionAbout
        self.about_panel.triggered.connect(self.about_gui)

        # Commenting out set and un set for now may need later

        # Set menu
        # self.set_reg = self.actionSet_Registry
        # self.set_reg.triggered.connect(self.set_registry)

        # self.unset_reg = self.actionUnset_Registry
        # self.unset_reg.triggered.connect(self.unset_registry)

        # Show menu
        self.show_reg = self.actionRegistry
        self.show_reg.triggered.connect(self.show_registry)

        self.show_work = self.actionWorkers
        self.show_work.triggered.connect(self.show_workers)

        # Set default theme
        theme_action = QAction(self)
        theme_action.setData('')
        theme_action.setText(QCoreApplication.translate('OpenCPI', 'None'))
        theme_action.triggered.connect(partial(app.setStyleSheet, None))
        self.themes = [theme_action]
        # Set .qss themes in stylesheets directory
        stylesheets_path = Path(Path(__file__).resolve().parent, 'stylesheets')
        for stylesheet_path in stylesheets_path.glob('*.qss'):
            theme_action = QAction(self)
            stylesheet = open(stylesheet_path).read()
            title = ' '.join([x.capitalize() for x in stylesheet_path.stem.split('_')])
            theme_action.setText(QCoreApplication.translate('OpenCPI', title))
            theme_action.triggered.connect(partial(app.setStyleSheet, stylesheet))
            if stylesheet_path.name == 'color_blind.qss':
                theme_action.triggered.connect(partial(
                    self.set_status_colors, STATUS_COLORS_ALT))
            else:
                theme_action.triggered.connect(partial(
                    self.set_status_colors, STATUS_COLORS_DEFAULT))
            self.themes.append(theme_action)
        self.menuThemes.addActions(self.themes)
        self.menubar.addMenu 

        # Status bar
        self.setStatusBar(QStatusBar(self))

    @staticmethod
    def set_status_colors(status_colors: dict) -> None:
        """Set the job manage status colors"""
        global STATUS_COLORS
        STATUS_COLORS = status_colors

    def center(self) -> None:
        """Ensure Main Window is in center of screen, regardless of screen size"""
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    @staticmethod
    def ocpi_dir_not_found() -> None:
        """Error message when /opencpi not set"""
        OcpiDirMessage()

    @staticmethod
    def exit_application() -> None:
        """Exits program, like clicking corner 'X' button"""
        QCoreApplication.quit()

    def ocpi_install_dir(self) -> None:
        """Get location of /opencpi directory"""
        ocpi_setup_dialog: OcpiSourceDialog = OcpiSourceDialog(self)
        ocpi_setup_dialog.show()
        ocpi_setup_dialog.exec_()
        self.ocpi_path = ocpi_setup_dialog.okay()
        self.ocpi_dir_save()

    def ocpi_dir_save(self) -> None:
        """Saves the /opencpi directory path to a config file

        Opens the config file in read/append mode. The key:value should either exist from previous use or, if the file
        didn't exist, the key is empty.

        The JSON file is extracted so the key:value pairs can be parsed. The key's value is updated to the current
        ocpi_path. The cursor is moved to the beginning of the extracted file and the file is updated. Truncation occurs
        to ensure that there aren't extraneous characters left after the update.
        """
        config_file: TextIO
        with self.config_path.open('w') as config_file: # Create new JSON file
            json.dump({"OPENCPI_DIR": self.ocpi_path}, config_file)
            config_file.truncate()  # Clear extra characters if new data is smaller than previous
        try:
            self.create_rcc_list()
            self.rcc_targets.fill_list(self.rcc_config_list)
            self.create_hdl_list()
            self.hdl_targets.fill_list(self.hdl_config_list)
            self.Current_opencpi_Dir_label.setText(self.ocpi_path)
            self.proj_path_save()
        except AttributeError:  # Ignore initial startup
            pass

    def set_proj_path(self) -> None:
        """Sets the project path that will be shown in Project Explorer

        It checks whether an existing path has been provided from the popup dialog. If so, that provided path
        is used as the display path for Project Explorer. If not, the default path is used.

        It also tries to create the default path, if it doesn't already exist. If so, the resulting exception is
        ignored. Then the path is saved to the JSON configuration file.

        If the display path is changed, the refresh method of Project Explorer is called to auto-refresh the displayed
        path. However, this refresh is ignored on initial startup to avoid problems.

        The path that is displayed in Project Explorer is relayed to the Current Displayed Path box at the bottom of the
        Project Explorer panel.
        """
        proj_path_dialog = ProjectExplorerPath(self)
        proj_path_dialog.show()
        proj_path_dialog.exec_()
        dialog_path = proj_path_dialog.okay()
        self.user_proj_path = dialog_path  # Assign path
        self.proj_path_save()
        try:
            self.project.refresh_view()
            self.project.current_pe_path.setText(self.user_proj_path)
            self.project.proj_path = self.user_proj_path
        except AttributeError:  # Ignore initial startup error
            pass
        
    def proj_path_save(self) -> None:
        """Saves the current project's directory path to a config file

        Opens the config file in read/append mode. The key:value should either exist from previous use or, if the file
        didn't exist, the key is empty.

        The JSON file is extracted so the key:value pairs can be parsed. The key's value is updated to the current
        ocpi_path. The cursor is moved to the beginning of the extracted file and the file is updated. Truncation occurs
        to ensure that there aren't extraneous characters left after the update.
        """
        config_file: TextIO
        with self.config_path.open('r+') as config_file:
            file_data: json = json.load(config_file)
            file_data["PROJECT_EXPLORER_PATH"] = str(self.user_proj_path)
            config_file.seek(0)  # Set file's current position at offset
            json.dump(file_data, config_file)
            config_file.truncate()  # Clear extra characters if new data is smaller than previous

    @staticmethod
    def curr_ocpi_settings() -> None:
        """Checks for current OpenCPI environment settings and passes them to display methods."""
        settings_dialog: UserEnvSettings = UserEnvSettings()
        settings_dialog.show_cdk_path()
        settings_dialog.show_user_env()
        settings_dialog.exec()

    def set_user_settings(self) -> None:
        """Calls external text editor to show current user environment settings."""
        subprocess.run(["xdg-open", f"{self.ocpi_path}/user-env.sh"])

    def ocpiadmin(self, verb, noun, name=None, options=None, 
        threading=True) -> Union[subprocess.CompletedProcess, None]:
        """Creates and executes an ocpiadmin command in a subprocess
        
        If threading is True, will call ProjectExplorerTree.start() to
        execute command using multithreading and will return None.

        If threading is False, will call subprocess.run() to execute
        command and will return the CompletedProcess.
        """
        cmd = 'ocpiadmin'
        if options is not None:
            for key,val in options.items():
                if val is True:
                    cmd += ' {}'.format(key)
                elif val:
                    cmd += ' {} {}'.format(key,val)
        if name is not None:
            cmd = ' '.join([cmd, verb, noun, name])
        if threading == True:
            ProjectExplorerTree.start(['bash', '-c', cmd], name)
            out = None
        else:
            env = os.environ.copy()
            env['TERM'] = 'tty'
            out = subprocess.run(['bash', '-c', cmd], 
                                 stderr=subprocess.PIPE, 
                                 stdout=subprocess.PIPE, 
                                 universal_newlines=True,
                                 env=env)
            self.console_output.on_update_text(out.stdout)
            self.console_output.on_update_text(out.stderr)
        self.console_output.on_update_text('COMMAND RAN: {}'.format(cmd))

        return out

    def ocpiadmin_install(self) -> None:
        """Allows user to user ocpiadmin install command from GUI
        
        Different arguments allow for different installation.
        Calls ocpiadmin() to execute command.
        """
        name: str
        pack_id: str
        url: str
        git_rev: str
        minimal: bool

        install_platform: InstallPlatformDialog = InstallPlatformDialog()
        install_platform.show()

        while install_platform.exec_():
            name, pack_id, url, git_rev, minimal = install_platform.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not name:
                QMessageBox.warning(self, "Missing name", 
                    "A platform name is required.", QMessageBox.Ok, 
                    QMessageBox.Ok)
            else:
                options = {
                    '-p': pack_id,
                    '-u': url,
                    '-g': git_rev,
                    '--minimal': minimal
                }
                self.ocpiadmin('install', 'platform', name=name, options=options)

                break

    def ocpiadmin_deploy(self) -> None:
        """Deploys an installed platform onto system
        
        Calls ocpiadmin() to execute command.
        """
        deploy_platform = DeployPlatformDialog(self)
        deploy_platform.show()

        while deploy_platform.exec_():
            platforms = ' '.join(deploy_platform.okay())
            self.ocpiadmin('deploy', 'platform', platforms, threading=False)

            break

    def create_new_project(self) -> None:
        """Opens dialog box for new project parameters

        With four different options, there are 16 possible combinations.
        """
        proj_location: str
        proj_dependency: str
        reg_project: bool
        proj_name: str
        package_prefix: str
        package_name: str

        proj_dep_flag = "-D "
        dependencies = ""

        new_project: NewProjectDialog = NewProjectDialog()
        new_project.show()
        dep_list = []

        while new_project.exec_():
            proj_name, proj_location, package_prefix, package_name, proj_dependency, reg_project = new_project.okay()
            if proj_dependency:
                count = 0
                dep_list = proj_dependency.split(", ")

            if not proj_location:
                proj_location = self.user_proj_path
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not proj_name:
                QMessageBox.warning(self, "Missing name", "A new project requires a project name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                options = {
                    '-d': proj_location
                }
                options['-F'] = package_prefix
                options['-N'] = package_name
                options['-D'] = dep_list
                options['--register'] = reg_project
                ocpidev(self, 'create', 'project', name=proj_name, options=options)

                break

    @staticmethod
    def parse_subprocess_cmd(cmd: str) -> str:
        """Checks subprocess command for number of concatenated commands. Returns the resultant ocpidev command that is
        executed.

        :param cmd: Command string passed to subprocess
        :return: Executed ocpidev command
        """
        counter: int = cmd.count("&&")
        if counter < 2:
            split = cmd.split("&&")
            command = f"COMMAND RAN: {split[1]}\n"
        elif counter == 2:
            split = cmd.split("&&")
            command = f"COMMAND RAN: {split[2]}\n"
        else:
            split = cmd.split("&&")
            command = f"COMMAND RAN: {split[3]}\n"
        return command

    def create_new_library(self) -> None:
        """Opens dialog box for new library parameters

        Dialog requires the user to provide new library name and associated project. Optionally, the user can provide
        a package ID (package_prefix.package_name). If none is provided, then a default value is applied to the final
        command.
        """
        lib_project: str

        new_library: NewLibraryDialog = NewLibraryDialog(self)
        new_library.show()

        while new_library.exec_():
            name, lib_project = new_library.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not name:
                QMessageBox.warning(self, "Missing name", "A new library requires a library name. Type 'components' for"
                                                          " the default library.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif lib_project == "":
                QMessageBox.warning(self, "Missing project", "A new library requires a project for it to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{lib_project}"
                options = {
                    '-d': directory
                }
                ocpidev(self, 'create', 'library', name=name, options=options)

                break

    def create_new_application(self) -> None:
        """Opens dialog box for new application parameters"""
        app_name: str
        app_project: str

        new_application = NewApplicationDialog(self)
        new_application.show()

        while new_application.exec_():
            app_name, app_project, language = new_application.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not app_name:
                QMessageBox.warning(self, "Missing name", "A new application requires an application name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif app_project == "":
                QMessageBox.warning(self, "Missing project", "A new application requires a project to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{app_project}"
                options = {
                    '-d': directory
                }
                if language == 'XML in a Sub-directory':  # Check for -x flag
                    options['-x'] = ' '
                elif language == 'XML Only':  # Check for -X flag
                    options['-X'] = ' '
                ocpidev(self, 'create', 'application', name=app_name, options=options)

                break

    def create_new_component(self) -> None:
        """Opens dialog box for new component parameters"""
        component_name: str
        project: str
        library: str

        new_component = NewComponentDialog(self)
        new_component.show()

        while new_component.exec_():
            component_name, project, library = new_component.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not component_name:
                QMessageBox.warning(self, "Missing name", "A new component requires a component name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif project == "":
                QMessageBox.warning(self, "Missing project",
                                    "A new component requires a project for the component to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{project}"
                options = {
                    '-d': directory
                }
                options['-l'] = library
                ocpidev(self, 'create', 'component', name=component_name, options=options)

                break

    def create_new_worker(self) -> None:
        """Opens dialog box for new component parameters.

        Requires checking of all possible combinations of worker type, language output, component spec, etc.
        """
        new_worker = NewWorkerDialog(self)
        new_worker.show()
        while new_worker.exec_():
            worker_name, worker_project, model, language, worker_library, comp_spec = new_worker.okay()
            if worker_name == "":
                QMessageBox.warning(self, "Missing Name", "A new worker requires a name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif worker_project == "":
                QMessageBox.warning(self, "Missing Project", "A new worker requires a Project.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif worker_library == "":
                QMessageBox.warning(self, "Missing Library", "A new worker requires a Library.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif comp_spec == "\n":
                QMessageBox.warning(self, "Missing Component",
                                    "A new worker requires a Component."
                                    " If you do not have one, select 'None' and one"
                                    " will be created for you.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                if not self.ocpi_path:
                    self.ocpi_dir_not_found()
                else:
                    options = {
                    }
                    if worker_library == 'components':
                        options['-d'] = f"{self.user_proj_path}/{worker_project}/components"
                    else:
                        options['-d'] = f"{self.user_proj_path}/{worker_project}/components/{worker_library}"
                    if comp_spec == 'None':
                        dummy_path = Path(self.user_proj_path, worker_project, 'components', 
                            'specs', 'dummy-spec.xml')
                        if not dummy_path.is_file():
                            ocpidev(self, 'create', 'component', name='dummy', options=options)
                        options['-S'] = "dummy"
                    else:
                        options['-S'] = comp_spec
                    if model == "HDL":
                        if worker_name[-4:] != '.hdl':
                            worker_name += ".hdl"
                        ocpidev(self, 'create', 'worker', name=worker_name, options=options)
                    else:
                        if worker_name[-4:] != '.rcc':
                            worker_name += ".rcc"
                        options['-L'] = language
                        ocpidev(self, 'create', 'worker', name=worker_name, options=options)

                    break
                    
    def create_new_protocol(self) -> None:
        """Opens dialog box for new protocol parameters"""
        protocol_name: str
        protocol_project: str
        protocol_library: str
        new_protocol = NewProtocolDialog(self)
        new_protocol.show()
        while new_protocol.exec_():
            protocol_name, protocol_project, protocol_library = new_protocol.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not protocol_name:
                QMessageBox.warning(self, "Missing name", "A new protocol requires a protocol name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{protocol_project}"
                options = {
                    '-d': directory
                }
                options['-l'] = protocol_library
                ocpidev(self, 'create', 'protocol', name=protocol_name, options=options)

                break

    def create_new_hdl_assembly(self) -> None:
        """Opens dialog box for new HDL assembly parameters"""
        hdl_assembly_project: str
        hdl_assembly_name: str

        hdl_assembly = NewHdlAssemblyDialog(self)
        hdl_assembly.show()
        while hdl_assembly.exec_():
            hdl_assembly_name, hdl_assembly_project = hdl_assembly.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not hdl_assembly_name:
                QMessageBox.warning(self, "Missing name", "A new HDL Assembly requires an HDL Assembly name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif hdl_assembly_project == "":
                QMessageBox.warning(self, "Missing project",
                                    "A new HDL Assembly requires a project for it to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{hdl_assembly_project}"
                options = {
                    '-d': directory
                }
                ocpidev(self, 'create', 'hdl assembly', name=hdl_assembly_name, options=options)

                break

    def create_new_hdl_platform(self) -> None:
        """Opens dialog box for new HDL platform parameters"""
        hdl_platform_project: str
        hdl_platform_name: str

        hdl_platform = NewHdlPlatformDialog(self)
        hdl_platform.show()
        while hdl_platform.exec_():
            hdl_platform_name, hdl_platform_project, hdl_part_num, hdl_time_freq = hdl_platform.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not hdl_platform_name:
                QMessageBox.warning(self, "Missing name", "A new HDL Platform requires a HDL Platform name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif hdl_platform_project == "":
                QMessageBox.warning(self, "Missing project",
                                    "A new HDL Platform requires a project for it to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{hdl_platform_project}"
                options = {
                    '-d': directory
                }
            
                options['-g'] = hdl_part_num
                options['-q'] = hdl_time_freq
                ocpidev(self, 'create', 'hdl platform', name=hdl_platform_name, options=options)

                break

    def create_new_hdl_prim_lib(self) -> None:
        """Opens dialog box for new HDL primitive library parameters"""
        hdl_prim_lib_project: str
        hdl_prim_lib_name: str

        hdl_prim_lib = NewHdlPrimLibDialog(self)
        hdl_prim_lib.show()
        while hdl_prim_lib.exec_():
            hdl_prim_lib_name, hdl_prim_lib_project = hdl_prim_lib.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not hdl_prim_lib_name:
                QMessageBox.warning(self, "Missing name",
                                    "A new HDL Primitive Library requires an HDL Primitive Library name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif hdl_prim_lib_project == "":
                QMessageBox.warning(self, "Missing project",
                                    "A new HDL Primitive Library requires a project for it to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{hdl_prim_lib_project}"
                options = {
                    '-d': directory
                }
                ocpidev(self, 'create', 'hdl primitive library', name=hdl_prim_lib_name, options=options)

                break

    def create_new_hdl_prim_core(self) -> None:
        """Opens dialog box for new HDL primitive core parameters"""
        hdl_prim_core_project: str
        hdl_prim_core_name: str

        hdl_prim_core = NewHdlPrimCoreDialog(self)
        hdl_prim_core.show()
        while hdl_prim_core.exec_():
            hdl_prim_core_name, hdl_prim_core_project = hdl_prim_core.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not hdl_prim_core_name:
                QMessageBox.warning(self, "Missing name",
                                    "A new HDL Primitive Core requires a HDL Primitive Core name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif hdl_prim_core_project == "":
                QMessageBox.warning(self, "Missing project",
                                    "A new HDL Primitive Core requires a project for it to be added to.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                directory = f"{self.user_proj_path}/{hdl_prim_core_project}"
                options = {
                    '-d': directory
                }
                ocpidev(self, 'create', 'hdl primitive core', name=hdl_prim_core_name, options=options)
            
                break

    def create_new_unit_test(self) -> None:
        """Opens dialog box for new unit test parameters"""
        unit_test = NewUnitTestDialog(self)
        unit_test.show()
        while unit_test.exec_():
            unit_test_lib, unit_test_project, unit_test_spec = unit_test.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not unit_test_project:
                QMessageBox.warning(self, "Missing Project", "A new unit test requires a project.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif not unit_test_lib:
                QMessageBox.warning(self, "Missing library", "A new unit test requires a library.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif not unit_test_spec:
                QMessageBox.warning(self, "Missing Component Spec", "A new unit test requires a component spec.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                if unit_test_lib == 'components':
                    directory = f"{self.user_proj_path}/{unit_test_project}/{unit_test_lib}"
                else:
                    directory = f"{self.user_proj_path}/{unit_test_project}/components/{unit_test_lib}"
                options = {
                    '-d': directory
                }
                ocpidev(self, 'create', 'test', name=unit_test_spec, options=options)

                break
                    
    def create_new_registry(self) -> None:
        """Opens dialog box for new registry parameters"""
        registry_dialog: NewRegistryDialog = NewRegistryDialog()
        registry_dialog.show()

        while registry_dialog.exec_():
            registry_name, registry_dir = registry_dialog.okay()
            if not self.ocpi_path:
                self.ocpi_dir_not_found()
            elif not registry_name:
                QMessageBox.warning(self, "Missing name", "A new registry requires a registry name.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            elif not registry_dir:
                QMessageBox.warning(self, "Missing Directory", "A new registry requires a directory to register.",
                                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                options = {
                    '-d': registry_dir
                }
                ocpidev(self, 'create', 'registry', name=registry_name, options=options)

                break

    def get_grc_config(self) -> None:
        """Gets the path for PYTHONPATH, needed to call GNU Radio Companion"""
        grc_dialog: GRCDialog = GRCDialog()
        grc_dialog.show()
        grc_dialog.exec_()
        self.python = grc_dialog.okay()

    def start_grc_process(self) -> None:
        """Executes call to GNU Radio Companion

        First checks to see if GRC is installed; this check is for both Ubuntu and CentOS install paths. If GRC is
        present, then a new Qt process is created to launch GRC, separate from the rest of the GUI. If not present,
        an exception is generated and a popup appears informing the user that GNU Radio needs to be installed.

        self.proc is also checked initially to see if it is present or not. If not, meaning GRC is not already running,
        then a GRC process is started. If present, then nothing happens as there is no need to re-launch GRC.
        """
        if Path("/usr/local/bin/gnuradio-companion").exists() or Path(
                "/usr/bin/gnuradio-companion").exists() and self.proc is None:
            self.proc = QProcess()
            env: Union[QProcessEnvironment, Type[QProcessEnvironment]] = QProcessEnvironment.systemEnvironment()
            env.insert("PYTHONPATH", f"{self.python}")
            self.proc.setProcessEnvironment(env)
            self.proc.finished.connect(self.grc_process_finished)
            self.proc.start("gnuradio-companion")
        else:
            QMessageBox.information(self, "GRC Not Installed", "To run GRC, you must have GNU Radio installed.",
                                    QMessageBox.Ok, QMessageBox.Ok)

    def grc_process_finished(self) -> None:
        """Ensures that the GRC process is killed when closed out"""
        self.proc = None

    def launch_node_graph(self) -> None:
        if self.proc is None:
            self.proc = QProcess()
            node_graph_path = Path(__file__).resolve().parent.joinpath('node_graph', 'main.py')
            self.proc.finished.connect(self.grc_process_finished)
            self.proc.start("python3", [str(node_graph_path)])

    def node_graph_finished(self) -> None:
        """Ensures that the Node Graph process is killed when closed out"""
        self.proc = None
    def unit_test(self):
        """Runs the desired test phases. If invalid choices are made, an error is sent to the output console."""
        verbose: str = ""
        mode: str = ""
        view: str = ""
        accum_err: str = ""
        keep_sim: str = ""
        only_plat: str = ""
        exclude_plat: str = ""
        command: str = ""
        rcc: list = self.rcc_targets.return_items()
        hdl: str = self.hdl_targets.return_items()
        rcc_counter: int = 0
        hdl_counter: int = 0
        rcc_arg: str = "--rcc-platform "
        hdl_arg: str = "--hdl-platform "
        rcc_targets: str = ""
        hdl_targets: str = ""
        library: Path = ""
        name: str = ""
        rcc_only_targs: str = ""
        hdl_only_targs: str = ""
        rcc_exclude_targs: str = ""
        hdl_exclude_targs: str = ""
        only_targs: str = "--only-platform "
        exclude_targs: str = "--exclude-platform "
        test_case_arg: str = ""
        self.test_target = str(Path(self.project.get_item()).name)

        # Extract individual RCC targets and create command strings for default, --only-platform, and --exclude-platform
        try:
            if rcc != [""]:
                while rcc_counter < len(rcc):
                    rcc_targets += rcc_arg + (rcc[rcc_counter] + " ")
                    rcc_only_targs += only_targs + (rcc[rcc_counter] + " ")
                    rcc_exclude_targs += exclude_targs + (rcc[rcc_counter] + " ")
                    rcc_counter += 1
        except TypeError:
            pass

        # Extract individual HDL targets and create command strings for default, --only-platform, and --exclude-platform
        try:
            if hdl != [""]:
                while hdl_counter < len(hdl):
                    hdl_targets += hdl_arg + (hdl[hdl_counter] + " ")
                    hdl_only_targs += only_targs + (hdl[hdl_counter] + " ")
                    hdl_exclude_targs += exclude_targs + (hdl[hdl_counter] + " ")
                    hdl_counter += 1
        except TypeError:
            pass

        if self.Start_Phase_comboBox.currentText() == "All":
            mode = "all"
        elif self.Start_Phase_comboBox.currentText() == "Generate" \
                and self.End_Phase_comboBox.currentText() == "Generate":
            mode = "gen"
        elif self.Start_Phase_comboBox.currentText() == "Generate" \
                and self.End_Phase_comboBox.currentText() == "Build":
            mode = "gen_build"
        elif self.Start_Phase_comboBox.currentText() == "Prepare" \
                and self.End_Phase_comboBox.currentText() == "Verify":
            mode = "prep_run_verify"
        elif self.Start_Phase_comboBox.currentText() == "Prepare" \
                and self.End_Phase_comboBox.currentText() == "Prepare":
            mode = "prep"
        elif self.Start_Phase_comboBox.currentText() == "Run" \
                and self.End_Phase_comboBox.currentText() == "Run":
            mode = "run"
        elif self.Start_Phase_comboBox.currentText() == "Prepare" \
                and self.End_Phase_comboBox.currentText() == "Run":
            mode = "prep_run"
        elif self.Start_Phase_comboBox.currentText() == "Verify" \
                and self.End_Phase_comboBox.currentText() == "Verify":
            mode = "verify"
        else:
            self.console_output.on_update_text("Invalid option\n")

        if self.Clean_Actions_groupBox.isChecked():
            if self.clean_group.checkedButton().text() == "Clean All Files":
                mode = "clean_all"
            elif self.clean_group.checkedButton().text() == "Clean Run Phase Files":
                mode = "clean_run"
            elif self.clean_group.checkedButton().text() == "Clean Sim Files":
                mode = "clean_sim"

        if self.View_Script_checkBox.isChecked():
            view = "--view"
        if self.Keep_Sims_checkBox.isChecked():
            keep_sim = "--keep-simulations"
        if self.Accum_Errors_checkBox.isChecked():
            accum_err = "--accumulate-errors"
        if self.Verbose_checkBox.isChecked():
            verbose = "--verbose"
        if self.test_case_lineEdit.text() != "":
            test_case_arg = f"--case {self.test_case_lineEdit.text()}"
        if self.PRV_groupBox.isChecked():
            if self.PVR_buttonGroup.checkedButton().text() == "Only-Platform":
                only_plat = "--only-platform"
            else:
                exclude_plat = "--exclude-platform"

        try:
            index: str = self.project.get_item()
            index_path: Path = Path(index)  # Convert string to path-type
            library = index_path.parents[0]  # Get the path to the library directory
            name = index.split("/")[-1]  # Get name of test
        except IndexError:
            QMessageBox.warning(self, "Missing test components", "You must select the correct test assets prior to "
                                                                 "running a unit test",
                                QMessageBox.Ok, QMessageBox.Ok)

        if not hdl:  # HDL panel is not checked
            if only_plat:  # Limit the platforms the unit test is executed on
                if not rcc:  # RCC panel is not checked
                    QMessageBox.information(self, "Only Platforms Selected", "To use the --only-platform argument, you "
                                                                             "must have at least one RCC or HDL target "
                                                                             "selected and 'NONE' cannot be selected.")
                elif "NONE" in rcc:  # Doesn't matter if other targets are selected
                    QMessageBox.information(self, "Only Platforms Selected", "To use the --only-platform argument, "
                                                                             "'NONE' cannot be in the target platform "
                                                                             "selection.")
                elif "" in rcc:  # RCC panel is not checked
                    QMessageBox.information(self, "Only Platforms Selected", "To use the --only-platform argument, you "
                                                                             "must have at least one RCC or HDL target "
                                                                             "selected and 'NONE' cannot be selected.")
                else:  # Execute the RCC-only targets
                    cmd = f"cd {library} && " \
                          f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_only_targs} --mode {mode} " \
                          f"test {self.test_target} {test_case_arg}"
                    ProjectExplorerTree.start(["bash", "-c", cmd], name)
                    command = self.parse_subprocess_cmd(cmd)
            elif exclude_plat:  # Limit the platforms the unit test is executed on
                if not rcc:  # RCC panel is not checked
                    QMessageBox.information(self, "Exclude Platforms Selected",
                                            "To use the --exclude-platform argument, "
                                            "you must have at least one RCC or HDL "
                                            "target selected and 'NONE' cannot be selected.")
                elif "NONE" in rcc:  # Doesn't matter if other targets are selected
                    QMessageBox.information(self, "Exclude Platforms Selected",
                                            "To use the --exclude-platform argument, "
                                            "'NONE' cannot be in the target platform "
                                            "selection.")
                elif "" in rcc:  # RCC panel is not checked
                    QMessageBox.information(self, "Exclude Platforms Selected",
                                            "To use the --exclude-platform argument, "
                                            "you must have at least one RCC or HDL "
                                            "target selected and 'NONE' cannot be selected.")
                else:  # Ignore selected RCC targets
                    cmd = f"cd {library} && " \
                          f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_exclude_targs} --mode {mode} " \
                          f"test {self.test_target} {test_case_arg}"
                    ProjectExplorerTree.start(["bash", "-c", cmd], name)
                    command = self.parse_subprocess_cmd(cmd)
            else:  # Normal unit test
                try:
                    if not rcc:  # RCC panel is not checked
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif "NONE" not in rcc and rcc != [""]:  # Legit RCC targets
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_targets} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif "NONE" in rcc:  # Doesn't matter if other targets are selected
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                except UnboundLocalError:  # Ignore error if no unit test assets are being called
                    pass
        else:  # HDL panel checked
            if only_plat:  # Limit the platforms the unit test is executed on
                if rcc:  # RCC panel active
                    if "NONE" not in rcc and rcc != [""] and "" in hdl:  # RCC target only
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_only_targs}" \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif rcc == [""] and "" not in hdl:  # HDL target only
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_only_targs} " \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif "NONE" in rcc and "" not in hdl:  # Invalid RCC with valid HDL target
                        QMessageBox.information(self, "Only Platforms Selected",
                                                "To use the --only-platform argument, "
                                                "you must have at least one RCC or HDL "
                                                "target selected and 'NONE' cannot be selected.")
                    else:  # Neither has valid targets
                        QMessageBox.information(self, "Only Platforms Selected",
                                                "To use the --only-platform argument, "
                                                "you must have at least one RCC or HDL "
                                                "target selected and 'NONE' cannot be selected.")
                else:  # HDL only
                    if "" not in hdl:  # Valid target
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_only_targs} " \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    else:
                        QMessageBox.information(self, "Only Platforms Selected", "To use the --only-platform "
                                                                                 "argument, you must have at least "
                                                                                 "one RCC or HDL target selected and "
                                                                                 "'NONE' cannot be a selection.")
            elif exclude_plat:  # Limit the platforms the unit test is executed on
                if rcc:  # RCC and HDL panels active
                    if "NONE" not in rcc and rcc != [""] and "" in hdl:  # RCC target only
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_exclude_targs}" \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif rcc == [""] and "" not in hdl:  # HDL target only
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_exclude_targs} " \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    else:  # No valid targets
                        QMessageBox.information(self, "Exclude Platforms Selected",
                                                "To use the --exclude-platform argument, "
                                                "you must have at least one RCC or HDL "
                                                "target selected and 'NONE' cannot be selected.")
                else:  # Only HDL panel active
                    if "" not in hdl:
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_exclude_targs} " \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    else:
                        QMessageBox.information(self, "Exclude Platforms Selected", "To use the --exclude-platform "
                                                                                    "argument, you must have at least "
                                                                                    "one RCC or HDL target selected "
                                                                                    "and 'NONE' cannot be selected.")
            else:  # Normal unit tests
                if not rcc:  # RCC panel inactive
                    if "" in hdl:  # Empty value equals NONE selected
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    else:  # Legit targets selected
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_targets} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                else:  # RCC panel active
                    if rcc == [""] and "" not in hdl:  # Only HDL targets selected
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_targets} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif rcc != [""] and "NONE" not in rcc and "" not in hdl:  # Legit RCC and HDL targets
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_targets} {hdl_targets} " \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif rcc != [""] and "NONE" not in rcc and "" in hdl:  # Only RCC targets selected
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {rcc_targets} " \
                              f"--mode {mode} test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif rcc == [""] and "" in hdl:  # NONE selected in both HDL panel and nothing in RCC
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    elif "NONE" in rcc and "" in hdl:  # Both RCC and HDL = "NONE"
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)
                    else:  # RCC explicitly set to NONE
                        cmd = f"cd {library} && " \
                              f"ocpidev run {view} {keep_sim} {accum_err} {verbose} {hdl_targets} --mode {mode} " \
                              f"test {self.test_target} {test_case_arg}"
                        ProjectExplorerTree.start(["bash", "-c", cmd], name)
                        command = self.parse_subprocess_cmd(cmd)

        self.console_output.on_update_text(command)

    @staticmethod
    def documentation() -> None:
        """Opens the latest version of OpenCPI documentation in default web browser"""
        webbrowser.open("https://opencpi.gitlab.io/releases/latest/")

    @staticmethod
    def bug_report() -> None:
        """Opens web site for users to submit bug reports and feature requests"""
        webbrowser.open("https://gitlab.com/groups/opencpi/-/issues")

    # commenting out set and unset because we may need it later

    # def set_registry(self) -> None:
    #     """(Re)sets the selected project's registry to something other than default"""
    #     registry_dialog = SetRegistryDialog()
    #     registry_dialog.show()
    #     if registry_dialog.exec_():
    #         self.reg_path = registry_dialog.okay()
    #
    #         index: Union[str, PurePath, PathLike[str]] = self.project.get_item()
    #         out = subprocess.run(
    #             ["bash", "-c", f"cd {index} &&"
    #                            f"ocpidev set registry {self.reg_path}"],
    #             stderr=subprocess.STDOUT, stdout=subprocess.PIPE, universal_newlines=True)
    #         command = "COMMAND RAN: ocpidev set registry {self.reg_path}"
    #         self.console_output.on_update_text(out.stdout)  # Send command output to console
    #         self.console_output.on_update_text(command)
    #
    # def unset_registry(self) -> None:
    #     """Restores selected project to default registry"""
    #     index: Union[str, PurePath, PathLike[str]] = self.project.get_item()
    #     if not index:
    #         QMessageBox.warning(self, "Nothing Selected",
    #                             "Cannot unset an unselected item. Please select an item to unset.",
    #                             QMessageBox.Ok, QMessageBox.Ok)
    #     else:
    #         out = subprocess.run(
    #             ["bash", "-c", f"cd {index} &&"
    #                            f"ocpidev unset registry"],
    #             stderr=subprocess.STDOUT, stdout=subprocess.PIPE, universal_newlines=True)
    #         command = "COMMAND RAN: ocpidev unset registry"
    #         self.console_output.on_update_text(out.stdout)  # Send command output to console
    #         self.console_output.on_update_text(command)

    @staticmethod
    def about_gui():
        """Populate About menu item with license and copyright info"""
        about_dialog = AboutWindow()
        about_dialog.show()
        about_dialog.exec_()

    def create_hdl_list(self):
        """Generate HDL platform list.

        Upon initial build, the GUI does not have the information to populate the HDL target list. This runs the ocpidev
        'show' command to scan the system for all HDL platforms then fills out the JSON config file with the list of
        platforms.
        """
        platform_list = []

        output: Union[CompletedProcess[Any], CompletedProcess[str]] = subprocess.run(
            ["bash", "-c", f"ocpidev show --json hdl platforms"],
            stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        platform_dict = json.loads(output.stdout)  # Make a dict from output
        for key in platform_dict:
            if platform_dict[key]["built"]:  # Look for "built" key in nested dicts
                platform_list.append(key)  # Add the target platform
            else:
                platform_list.append(key + "*")
        self.hdl_config_list = platform_list  # Separate targets returned by Bash command
        with self.config_path.open('r+') as config_file:
            file_data: json = json.load(config_file)
            file_data["HDL_PLATFORM_LIST"] = self.hdl_config_list
            config_file.seek(0)  # Set cursor to beginning of file
            json.dump(file_data, config_file)
            config_file.truncate()  # Remove extraneous characters

    def create_rcc_list(self):
        """Generate RCC platform list.

        Upon initial build, the GUI does not have the information to populate the RCC target list. This runs the ocpidev
        'show' command to scan the system for all RCC platforms then fills out the JSON config file with the list of
        platforms.
        """
        output: Union[CompletedProcess[Any], CompletedProcess[str]] = subprocess.run(
            ["bash", "-c", f"ocpidev show --simple rcc platforms"],
            stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        self.rcc_config_list = output.stdout.split()  # Separate targets returned by Bash command
        with self.config_path.open('r+') as config_file:
            file_data: json = json.load(config_file)
            file_data["RCC_PLATFORM_LIST"] = self.rcc_config_list
            config_file.seek(0)
            json.dump(file_data, config_file)
            config_file.truncate()

    def refresh_rcc_platforms(self) -> None:
        """When the button is pressed, the list is refreshed"""
        self.create_rcc_list()
        self.rcc_targets.fill_list(self.rcc_config_list)
    
    def refresh_hdl_platforms(self) -> None:
        """When the button is pressed, the list is refreshed"""
        self.create_hdl_list()
        self.hdl_targets.fill_list(self.hdl_config_list)

    def show_registry(self) -> None:
        """ Display information on the registry."""
        out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
            subprocess.run(["bash", "-c", f"ocpidev show --table registry"],
                           stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        self.console_output.on_update_text(out.stdout)
        self.console_output.on_update_text(out.stderr)

    def show_workers(self) -> None:
        """Display all workers in registered projects."""
        out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
            subprocess.run(["bash", "-c", f"ocpidev show workers"],
                           stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        self.console_output.on_update_text(out.stdout)
        self.console_output.on_update_text(out.stderr)


class OcpiDirMessage(QMessageBox):
    """Message when attempting to execute ocpidev commands but /opencpi isn't set for the session."""
    title: str

    def __init__(self) -> None:
        super().__init__()
        self.title = "OpenCPI directory not set"
        self.setIcon(QMessageBox.Information)
        self.setText("You need to set the /opencpi directory before continuing.")
        self.init_ui()

    def init_ui(self) -> None:
        self.setWindowTitle(self.title)
        self.exec()
        self.show()


class ProjectExplorerPath(QDialog, Ui_Project_Path_Dialog):
    """Get the path that should be shown in Project Explorer"""

    def __init__(self, window) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.proj_field = self.Proj_Path_lineEdit
        self.proj_field.setReadOnly(True)
        self.button_box = self.buttonBox
        self.button_box.button(QDialogButtonBox.Ok).setEnabled(False)
        self.button_box.button(QDialogButtonBox.Cancel).hide()
        self.proj_field.textChanged.connect(self.enable_ok)
        self.user_proj_path = window.user_proj_path  # Set path to the path set on startup
        self.proj_field.setText(self.user_proj_path)
        self.dir_dialog = self.toolButton
        self.dir_dialog.clicked.connect(self.show_paths)

    def enable_ok(self):
        if not self.proj_field.text():
            self.button_box.button(QDialogButtonBox.Ok).setEnabled(False)
        else:
            self.button_box.button(QDialogButtonBox.Ok).setEnabled(True)

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def show_paths(self) -> None:
        """Show the default location of path shown in Project Explorer"""
        show_path = QFileDialog.getExistingDirectory(self, "Open Directory", f"{str(Path.home())}",
            QFileDialog.ShowDirsOnly)
        if show_path:
            self.proj_field.setText(show_path)

    def okay(self) -> str:
        """Returns path provided by user

        :return: New path
        :rtype: str
        """
        return self.proj_field.text()

    def closeEvent(self, a0: QCloseEvent) -> None:
        if not self.user_proj_path:
            os.remove("opencpi_config.json")
            sys.exit()


class NewFilenameDialog(QDialog, Ui_New_Filename_Dialog):
    """Open dialog to get new filename"""

    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()

    def okay(self) -> str:
        """Returns new name for selected item

        :return: New item name
        :rtype: str
        """
        self.New_Filename_lineEdit.setText()
        new_name: str = self.New_Filename_lineEdit.text()
        return new_name


class GRCDialog(QDialog, Ui_GRC_Configuration):
    """GRC configuration dialog"""
    python_dir: QToolButton
    dir_dialog: QToolButton
    python_field: QLineEdit

    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()

        self.python_field = self.lineEdit_2

        self.python_field.setText("/usr/local/lib/python3/dist-packages")
        self.python_dir = self.toolButton_2
        self.python_dir.clicked.connect(self.python_file_dialog)

    def python_file_dialog(self) -> None:
        """Display the assumed location of PYTHONPATH"""
        python_path: str = QFileDialog.getExistingDirectory(self, "Python Path", "/usr/local/lib/python3/dist-packages",
                                                            QFileDialog.ShowDirsOnly)
        self.lineEdit_2.setText(python_path)

    def okay(self) -> str:
        """Get the specified location of PYTHONPATH

        :return: PYTHONPATH location
        :rtype: str
        """
        python: str = self.lineEdit_2.text()
        return python


class OcpiSourceDialog(QDialog, Ui_Ocpi_Setup_Configuration):
    """OpenCPI directory configuration dialog

    Checks to see whether the opencpi directory has been written to a file. If so, then that path is shown in the
    dialog. If not, then a default path is shown.
    """
    ocpi_path: str
    dir_dialog: QToolButton
    ocpi_field: QLineEdit
    button_box: QDialogButtonBox

    def __init__(self, window) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.ocpi_field = self.Ocpi_Path_lineEdit
        self.ocpi_field.setReadOnly(True)
        self.button_box = self.buttonBox
        self.button_box.button(QDialogButtonBox.Ok).setEnabled(False)
        self.button_box.button(QDialogButtonBox.Cancel).hide()
        self.ocpi_field.textChanged.connect(self.enable_ok)
        self.ocpi_path = window.ocpi_path  # Set path to user provided path at startup
        self.ocpi_field.setText(self.ocpi_path)  # Set path to config file listing
        self.dir_dialog = self.toolButton
        self.dir_dialog.clicked.connect(self.show_ocpi_path)

    def enable_ok(self):
        if not self.ocpi_field.text():
            self.button_box.button(QDialogButtonBox.Ok).setEnabled(False)
        else:
            self.button_box.button(QDialogButtonBox.Ok).setEnabled(True)

    def show_ocpi_path(self) -> None:
        """Show the assumed location of /opencpi directory"""
        ocpi_path = QFileDialog.getExistingDirectory(self, "Open Directory", f"{str(Path.home())}",
                                                          QFileDialog.ShowDirsOnly)
        if ocpi_path:
            self.ocpi_field.setText(ocpi_path)

    def okay(self) -> str:
        """Returns the location of the /opencpi path

        :return: Text name of /opencpi path
        :rtype:: str
        """
        return self.ocpi_field.text()

    def closeEvent(self, a0: QCloseEvent) -> None:
        if not self.ocpi_path:
            sys.exit()


class UserEnvSettings(QDialog, Ui_Installation_Info_Dialog):
    """Shows the current user environment settings"""
    user_env: QTextBrowser
    cdk_path: QLabel

    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()

        self.cdk_path = self.CDK_Location_label
        self.user_env = self.User_Env_Settings_textBrowser

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def show_cdk_path(self) -> None:
        """Display current CDK location"""
        cdk: Path = Path(f"{window.ocpi_path}").joinpath("cdk")
        if cdk.exists():
            self.cdk_path.setText(str(cdk))
        else:
            self.cdk_path.setText("Invalid path")

    def show_user_env(self) -> None:
        """Display the settings in user-env.sh"""
        options: List[str] = []
        try:
            env_file: TextIO
            with open(f"{window.ocpi_path}/user-env.sh") as env_file:
                line: str
                for line in env_file:
                    if line.startswith("export"):
                        options.append(line.split("export", 1)[1])
            if not options:
                self.user_env.setText("Default values set")
            else:
                item: str
                for item in options:
                    self.user_env.append(item)
        except IOError:
            self.user_env.setText("Unable to access user-env.sh")


class NewRegistryDialog(QDialog, Ui_New_Registry_Dialog):
    """New Registry asset dialog"""
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.Registry_Dir_Path_lineEdit.setText("")
        self.toolButton.clicked.connect(self.show_reg_path)

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def show_reg_path(self) -> None:
        """Display the path of the default registry"""
        reg_path: str = QFileDialog.getExistingDirectory(self, "Open Directory", f"{window.user_proj_path}",
                                                         QFileDialog.ShowDirsOnly)
        self.Registry_Dir_Path_lineEdit.setText(reg_path)

    def okay(self) -> Tuple[str, str]:
        """Get the new registry name and path

        :return: Tuple with registry name and path
        :rtype: tuple
        """
        reg_name: str = self.Registry_Name_lineEdit.text()
        reg_dir: str = self.Registry_Dir_Path_lineEdit.text()

        return reg_name, reg_dir


class NewProjectDialog(QDialog, Ui_New_Project_Dialog):
    """New Project asset dialog"""
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.setup_ui()
        self.show()
        self.toolButton.clicked.connect(self.show_dirs)

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def show_dirs(self) -> None:
        """Show the assumed location of /opencpi directory"""
        proj_path: str = QFileDialog.getExistingDirectory(self, "Open Directory", f"{str(Path.home())}",
                                                          QFileDialog.ShowDirsOnly)
        self.New_Project_Location_lineEdit.setText(proj_path)

    def okay(self) -> Tuple[str, str, str, str, str, bool]:
        """Get the new project information

        :return: Tuple with project name, location, package prefix and name, project dependencies, and if the project
        is registered
        :rtype: tuple
        """
        project_name = self.Project_Name_lineEdit.text()
        location = self.New_Project_Location_lineEdit.text()
        package_prefix = self.Package_Prefix_lineEdit.text()
        package_name = self.Package_Name_lineEdit.text()
        dependency = self.Proj_Dependency_lineEdit.text()
        register_proj = self.Register_Proj_checkBox.isChecked()

        return project_name, location, package_prefix, package_name, dependency, register_proj


class NewLibraryDialog(QDialog, Ui_New_Library_Dialog):
    """New Library asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Library_Project_comboBox.addItem(str(project.name))
        self.Library_Project_comboBox.setCurrentIndex(-1)

    def setup_ui(self) -> None:
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str]:
        """Gathers and returns library info when Okay button clicked

        :return: Library name and associated project
        :rtype: tuple
        """
        lib_name = self.Library_Name_lineEdit.text()
        project = self.Library_Project_comboBox.currentText()

        return lib_name, project


class NewApplicationDialog(QDialog, Ui_New_Application_Dialog):
    """New Application asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        self.Project_comboBox.setCurrentIndex(-1)

    def setup_ui(self) -> None:
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[Union[str, Any], Union[str, Any]]:
        """Gathers and returns application info when Okay button clicked

        :return: Application name and associated project
        :rtype: tuple
        """
        application_name = self.Application_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()
        language = self.Language_buttonGroup.checkedButton().text()
        return application_name, project, language


class NewComponentDialog(QDialog, Ui_New_Component_Dialog):
    """New Component asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.main_window = main_window
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Component_Project_comboBox.addItem(str(project.name))
        if self.Component_Project_comboBox.count() > 0:
        # set default
            self.Component_Project_comboBox.setCurrentIndex(-1)
        else:
            self.on_combobox_changed()
        self.Component_Project_comboBox.currentTextChanged.connect(self.on_combobox_changed)

    def setup_ui(self) -> None:
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def on_combobox_changed(self) -> None:
        """Update the Library combobox items when libraries are changed"""
        self.Component_Library_comboBox.clear()
        project: str = self.Component_Project_comboBox.currentText()
        project_path = Path(self.main_window.user_proj_path, project)
        options = {
            '-d': project_path,
            '--local': True
        }
        libraries = ocpidev_show(self, 'libraries', options=options)
        libraries = [Path(library).name for library in libraries]
        self.Component_Library_comboBox.addItems(libraries)

    def okay(self) -> Tuple[Union[str, Any], Union[str, Any], Union[str, Any]]:
        """Gathers and returns component spec information when Okay button clicked

        :return: Component spec name, associated project, and associated library
        :rtype: tuple
        """
        component_name = self.Component_Name_lineEdit.text()
        project = self.Component_Project_comboBox.currentText()
        library = self.Component_Library_comboBox.currentText()

        return component_name, project, library


class NewWorkerDialog(QDialog, Ui_New_Worker_Dialog):
    """New Worker asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.main_window = main_window
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        if self.Project_comboBox.count() > 0:
        # set default
            self.Project_comboBox.setCurrentIndex(-1)
        else:
            self.on_library_combobox_changed()
            self.on_component_combobox_changed()
        self.Project_comboBox.currentTextChanged.connect(self.on_library_combobox_changed)
        self.Project_comboBox.currentTextChanged.connect(self.on_component_combobox_changed)
        self.Vhd_radioButton.setEnabled(False)
        self.RCC_radioButton.toggled.connect(self.on_rcc_clicked)
        self.HDL_radioButton.toggled.connect(self.on_hdl_clicked)

    def on_hdl_clicked(self) -> None:
        """Conditions when HDL checkbox is checked"""
        self.Vhd_radioButton.setEnabled(True)
        self.Vhd_radioButton.setChecked(True)
        self.C_radioButton.setEnabled(False)
        self.Cpp_radioButton.setEnabled(False)

    def on_rcc_clicked(self) -> None:
        """Conditions when RCC checkbox is checked"""
        self.C_radioButton.setEnabled(True)
        self.Cpp_radioButton.setEnabled(True)
        self.Cpp_radioButton.setChecked(True)
        self.Vhd_radioButton.setEnabled(False)

    def setup_ui(self) -> None:
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def on_library_combobox_changed(self) -> None:
        """Update library combobox when library list changes"""
        self.Library_comboBox.clear()
        project = self.Project_comboBox.currentText()
        project_path = Path(self.main_window.user_proj_path, project)
        options = {
            '--local': True,
            '-d': project_path
        }
        libraries = ocpidev_show(self, 'libraries', options=options)
        libraries = [Path(library).name for library in libraries]
        self.Library_comboBox.addItems(libraries)
        if self.Library_comboBox.count() > 1:
        # If more than one library available, don't set a default
            self.Library_comboBox.setCurrentIndex(-1)

    def on_component_combobox_changed(self) -> None:
        """Update component combobox when component list changes"""
        self.Component_Spec_comboBox.clear()
        self.Component_Spec_comboBox.addItem('None')
        project = self.Project_comboBox.currentText()
        project_path = Path(self.main_window.user_proj_path, project)
        options = {
            '-d': project_path,
            '--local': True
        }
        components = ocpidev_show(self, 'components', options=options, delimiter=' ')
        self.Component_Spec_comboBox.addItems(components)

    def okay(self) -> Tuple[str, str, str, str, str, str]:
        """Gathers and returns user input when Okay button clicked

        :return: Worker name, associated project, model type, language used, associated library, and
        associated component spec.
        :rtype: tuple
        """
        worker_name = self.Worker_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()
        model = self.Model_buttonGroup.checkedButton().text()
        lang = self.Language_buttonGroup.checkedButton().text()
        library = self.Library_comboBox.currentText()
        comp_spec = self.Component_Spec_comboBox.currentText()
        
        return worker_name, project, model, lang, library, comp_spec


class NewProtocolDialog(QDialog, Ui_New_Protocol_Dialog):
    """New Protocol asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.main_window = main_window
        projects_path = Path(self.main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        if self.Project_comboBox.count() > 0:
        # set default
            self.Project_comboBox.setCurrentIndex(-1)
        else:
            self.on_combobox_changed()
        self.Project_comboBox.currentTextChanged.connect(self.on_combobox_changed)

    def setup_ui(self) -> None:
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def on_combobox_changed(self):
        """Update libraries combobox when libraries list changes"""
        self.Library_comboBox.clear()
        project = self.Project_comboBox.currentText()
        project_path = Path(self.main_window.user_proj_path, project)
        options = {
            '-d': project_path,
            '--local': True
        }
        libraries = ocpidev_show(self, 'libraries', options=options)
        libraries = [Path(library).name for library in libraries]
        self.Library_comboBox.addItems(libraries)

    def okay(self) -> Tuple[str, str, bool]:
        """Gathers and returns protocol info when Okay button clicked

        :return: Protocol name, associated project, and associated library
        :rtype: tuple
        """
        protocol_name = self.Protocol_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()
        library = self.Library_comboBox.currentText()

        return protocol_name, project, library


class NewHdlAssemblyDialog(QDialog, Ui_New_HDL_Assembly_Dialog):
    """New HDL Assembly asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        self.Project_comboBox.setCurrentIndex(-1)

    def setup_ui(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str]:
        """Gathers and returns HDL assembly info when Okay button clicked

        :return: Assembly name and associated project
        :rtype: tuple
        """
        assembly_name = self.HDL_Assem_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()

        return assembly_name, project


class NewHdlPlatformDialog(QDialog, Ui_New_HDL_Platform_Dialog):
    """New HDL Platform asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        self.Project_comboBox.setCurrentIndex(-1)

    def setup_ui(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str, str, str]:
        """Gathers and returns HDL Platform info when Okay button clicked

        :return: Platform name, associated project, part number, and time freq
        :rtype: tuple
        """
        platform_name = self.Platform_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()
        part_number = self.Part_Number_lineEdit.text()
        time_freq = self.Time_Server_Freq_lineEdit.text()

        return platform_name, project, part_number, time_freq


class NewHdlPrimLibDialog(QDialog, Ui_New_HDL_Prim_Library_Dialog):
    """New HDL Primitive Library asset dialog"""
    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        self.Project_comboBox.setCurrentIndex(-1)

    def setup_ui(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str]:
        """Gathers and returns HDL Primitive Library info when Okay button clicked

        :return: Primitive library name and associated project
        :rtype: tuple
        """
        hdl_prim_lib_name = self.HDL_Prim_Lib_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()

        return hdl_prim_lib_name, project


class NewHdlPrimCoreDialog(QDialog, Ui_New_HDL_Prim_Core_Dialog):
    """New Primitive Core asset dialog"""
    hdl_prim_core_project: QComboBox
    hdl_prim_core_name: QLineEdit
    projects_path: Path
    project: Path
    qt_rectangle: QRect
    center_point: QPoint

    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        projects_path = Path(main_window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        self.Project_comboBox.setCurrentIndex(-1)

    def setup_ui(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str]:
        """Gathers and returns HDL Primitive Core info when Okay button clicked

        :return: Core name and associated project
        :rtype: tuple
        """
        hdl_prime_core_name = self.HDL_Prim_Core_Name_lineEdit.text()
        project = self.Project_comboBox.currentText()

        return hdl_prime_core_name, project


class NewUnitTestDialog(QDialog, Ui_New_UnitTest_Dialog):
    """New Unit Test asset dialog"""
    def __init__(self, main_window) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        self.main_window = main_window
        projects_path = Path(window.user_proj_path)
        for project in projects_path.iterdir():
            if project.is_dir():
                self.Project_comboBox.addItem(str(project.name))
        if self.Project_comboBox.count() > 0:
        # set default
            self.Project_comboBox.setCurrentIndex(-1)
        else:
            self.on_library_combobox_changed()
            self.on_component_combobox_changed()
        self.Project_comboBox.currentTextChanged.connect(self.on_library_combobox_changed)
        self.Project_comboBox.currentTextChanged.connect(self.on_component_combobox_changed)

    def setup_ui(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def on_library_combobox_changed(self):
        """Update Library combobox when library list changes"""
        self.Library_comboBox.clear()
        project = self.Project_comboBox.currentText()
        project_path = Path(window.user_proj_path, project)
        options = {
            '--local': True,
            '-d': project_path
        }
        libraries = ocpidev_show(self, 'libraries', options=options)
        libraries = [Path(library).name for library in libraries]
        self.Library_comboBox.addItems(libraries)
        if self.Library_comboBox.count() > 1:
        # If more than one library available, don't set a default
            self.Library_comboBox.setCurrentIndex(-1)

    def on_component_combobox_changed(self):
        """Update Component Spec combobox when component list changes"""
        self.Component_Spec_comboBox.clear()
        project = self.Project_comboBox.currentText()
        project_path = Path(window.user_proj_path, project)
        options = {
            '--local': True,
            '-d': project_path
        }
        components = ocpidev_show(self, 'components', options=options, delimiter=' ')
        self.Component_Spec_comboBox.addItems(components)

    def okay(self) -> Tuple[str, str, str]:
        """Gathers and returns Unit Test info when Okay button clicked

        :return: Unit test associated library, associated project, and associated component specs
        :rtype: tuple
        """
        library = self.Library_comboBox.currentText()
        project = self.Project_comboBox.currentText()
        component_spec = self.Component_Spec_comboBox.currentText()

        return library, project, component_spec


class ProjectExplorerTree(QTreeView):
    """Handles all functionality for the Project Explorer panel.

    Creates context menu for Build, Clean, Register, and Delete OpenCPI assets.
    Provides access to selected assets for use in other methods, such as Set/UnSet
    """
    current_pe_path: str
    proj_path: Path
    default_dir: str
    fileSystemModel: Union[QFileSystemModel, QFileSystemModel]
    treeView: Union[QTreeView, QTreeView]
    rcc_targ_list: str
    hdl_targ_list: str
    rcc_arg: str
    hdl_arg: str
    root: Optional[QModelIndex]

    def __init__(self, main_window, tree: QTreeView, console, rcc, hdl) -> None:
        """Creates the ListView/FileSystemModel for viewing"""
        super().__init__()
        self.treeView = tree
        self.default_dir = main_window.ocpi_path
        self.proj_path = main_window.user_proj_path
        self.console = console
        self.rcc_target: RccTargets = rcc
        self.hdl_target: HdlTargets = hdl
        self.rcc_targ_list = ""
        self.hdl_targ_list = ""
        self.rcc_arg = "--rcc-platform "
        self.hdl_arg = "--hdl-platform "
        self.current_pe_path = main_window.Current_PE_Path_Output_label

        self.fileSystemModel = QFileSystemModel(self.treeView)
        self.fileSystemModel.setReadOnly(False)
        root = self.fileSystemModel.setRootPath(f"{self.proj_path}")
        self.treeView.setModel(self.fileSystemModel)
        self.treeView.hideColumn(1)
        self.treeView.hideColumn(2)
        self.treeView.hideColumn(3)
        self.treeView.setRootIndex(root)
        self.treeView.setSelectionMode(self.treeView.ExtendedSelection)
        self.treeView.setRootIsDecorated(True)
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setDragEnabled(True)  # Allow items to be dragged from this panel

        self.treeView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeView.customContextMenuRequested.connect(self.context_menu)

    def context_menu(self) -> None:
        """Creates nearly all the menus and submenu actions for OpenCPI assets, except for creation and set/unset
        registry

        Based on the menu type selected, the correct actions for the asset are shown. This allows the context menu to
        be dynamically generated based on the metadata associated with the OpenCPI assets within the project.
        """
        menu: Union[QMenu, QMenu] = QMenu(self)
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        project_path: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        menu_type: str
        register_submenu: Union[QMenu, QMenu]

        if index_path.suffix == ".xml":
            if str(project_path).split("/")[-2] == "applications" and index.split("/")[-1] != "applications.xml":
                menu.addAction("Run", self.run_application)
                menu.addAction("Edit File", self.edit_file)
            elif str(project_path).split("/")[-1] == "applications" and index.split("/")[-1] != "applications.xml":
                menu.addAction("Delete", self.del_app)
                menu.addAction("Run", self.run_application)
                menu.addAction("Edit File", self.edit_file)
            elif str(project_path).split("/")[-1] == "specs":
                menu.addAction("Delete", self.del_spec)
                menu.addAction("Edit File", self.edit_file)
            else:
                menu.addAction("Edit File", self.edit_file)
                
        elif index_path.suffix in [".xml", ".cpp", ".txt", ".cc", ".c", ".vhd", ".v", ".py", ".sh", ".deps", ".mk", ".h", ".hh", ".rst", "Makefile", ".exports", "package-id", "workers"]:
                menu.addAction("Edit File", self.edit_file)
        else:
            menu_type = ocpiutil.get_dirtype(index)
            if menu_type == "project":
                menu.addAction("Build", lambda: self.build_selection(menu_type, index_path))
                menu.addAction("Clean", lambda: self.clean_selection(index_path))
                menu.addAction("Delete", self.del_project)

                register_submenu = QMenu(menu)
                register_submenu.setTitle("(Un)Register")
                menu.addMenu(register_submenu)
                register_submenu.addAction("Register Project", self.reg_project)
                register_submenu.addAction("Unregister Project", self.unreg_project)

            elif menu_type == "application":
                menu.addAction("Build", lambda: self.build_selection(menu_type, index_path))
                menu.addAction("Clean", lambda: self.clean_selection(index_path))
                menu.addAction("Delete", self.del_app)
                menu.addAction("Run", self.run_application)

            elif menu_type == "applications":
                menu.addAction("Build", lambda: self.build_selection(menu_type, index_path))
                menu.addAction("Clean", lambda: self.clean_selection(index_path))
                # menu.addAction("Run", self.run_application)

            elif menu_type == "component":
                menu.addAction("Delete", self.del_spec)

            elif menu_type in ["library", "hdl-assembly", "hdl-platform", "hdl-core", "hdl-library", "test", "worker"]:
                menu.addAction("Build", lambda: self.build_selection(menu_type, index_path))
                menu.addAction("Clean", lambda: self.clean_selection(index_path))
                menu.addAction("Delete", lambda: self.delete_selection(menu_type))

            elif menu_type in ['hdl-assemblies', 'hdl-platforms', 'hdl-primitives']:
                menu.addAction("Build", lambda: self.build_selection(menu_type, index_path))
                menu.addAction("Clean", lambda: self.clean_selection(index_path))

        menu.addAction("Refresh", self.refresh_view)  # Manual refresh of Project Explorer if needed
        menu.exec_(QCursor.pos())  # Get the position of the mouse selection

    @staticmethod
    def start(cmd: str, name: str) -> None:
        """Create a new thread and add to job manager.

        :param cmd: The command is used to launch the subprocess
        :param name: The name is used to identify the specific worker
        """
        JobQueue.start_worker(window.job_console, cmd, name)

    @staticmethod
    def parse_subprocess_cmd(cmd: str) -> str:
        """Checks subprocess command for number of concatenated commands. Returns the resultant ocpidev command that is
        executed.

        :param cmd: Command string passed to subprocess
        :return: Executed ocpidev command
        :rtype: str
        """
        counter: int = cmd.count("&&")
        if counter == 0:
            command = f"COMMAND RAN: {cmd}"
        elif counter == 1:
            split = cmd.split("&&")
            command: str = f"COMMAND RAN: {split[1]}\n"
        elif counter == 2:
            split = cmd.split("&&")
            command = f"COMMAND RAN: {split[2]}\n"
        else:
            split = cmd.split("&&")
            command = f"COMMAND RAN: {split[3]}\n"
        return command

    def build_selection(self, asset_type, index_path: str) -> None:
        """Calls the proper build method based on the project asset selected.

        :param asset_type: Project asset
        """
        rcc_target = self.get_rcc_targets()
        hdl_target = self.get_hdl_targets()

        options = {
            '-d': index_path,
            '--rcc-platform': rcc_target,
            '--hdl-platform': hdl_target
        }
        if asset_type in ['hdl-assembly', 'hdl-assemblies']:
            options['--workers-as-needed'] = True
        ocpidev(self, "build", name=index_path.name, options=options)

    def clean_selection(self, index_path: str) -> None:
        """Calls the proper clean method based on the project asset selected.

        :param asset_type: Project asset
        """
        options = {
            '-d': index_path
        }
        ocpidev(self, "clean", options=options)

    def delete_selection(self, asset_type: str) -> None:
        """Calls the proper delete method based on the project asset selected.

        :param asset_type: Project asset
        """
        if asset_type == 'library':
            self.del_lib()
        if asset_type == 'hdl-assembly':
            self.del_assembly()
        if asset_type == 'hdl-platform':
            self.del_platform()
        if asset_type == 'hdl-library':
            self.del_prim_lib()
        if asset_type == 'hdl-core':
            self.del_prim_core()
        if asset_type == 'test':
            self.del_test()
        if asset_type == 'worker':
            self.del_worker()

    def build_folder(self, project_path: str, parent: str) -> None:
        """Walks through all projects in a directory then call build tree to process each projects' XML tree

        :param project_path: Path to projects
        :param parent: Parent directory for the projects
        :return: None
        """
        ocpi_path = self.default_dir
        for project in os.listdir(project_path):  # Walk through all projects
            path = project_path + f'/{project}'
            if os.path.isdir(str(path)):  # Check if parent directory
                if len(os.listdir(path)) == 1:
                    continue
                else:  # Build XML tree if project
                    cmd = f"cd {path} && ocpidev refresh project"
                    subprocess.run(["bash", "-c", cmd], stderr=subprocess.PIPE, stdout=subprocess.PIPE,
                                   universal_newlines=True)
                    self.build_tree(project_path + f'/{project}/project-metadata.xml', parent)

    def build_tree(self, project_path: str, parent: str) -> None:
        """Build the XML tree of a particular project

        :param project_path: Path to the project
        :param parent: Parent directory of the project
        :return: None
        """
        tree = ET.parse(project_path)  # Generate project tree
        root = tree.getroot()  # Determine project root
        root_dict = root.attrib  # Create a dictionary of project attributes
        item = QTreeWidgetItem(parent)
        item.setText(0, root_dict['name'].split(".")[1])  # Assign project to tree
        for elem in root:  # Walk through all assets
            elem_dict = elem.attrib  # Create dictionary from asset attributes
            children = list(elem)
            if elem_dict:  # Process asset attributes
                if elem.tag == 'built':
                    target = child_dict['target']
                    if 'configID' in child_dict.keys():
                        config_id = child_dict['configID']
                        text = f'configID {config_id} built for {target}'
                    else:
                        text = f'built for {target}'
                    item1 = QTreeWidgetItem(item)
                    item1.setText(0, text)
                else:  # No attributes so just set the name
                    item1 = QTreeWidgetItem(item)
                    item1.setText(0, elem_dict['name'])
                    if children:  # Process children for current asset
                        self.build_children(children, item1)
            else:  # Special check for RCC or OpenCPI Components libraries
                if elem.tag == 'rcc' or elem.tag == 'components':
                    if children:
                        self.build_children(children, item)
                else:
                    item1 = QTreeWidgetItem(item)
                    item1.setText(0, elem.tag)
                    if children:
                        self.build_children(children, item1)

    def build_children(self, children: str, parent: str) -> None:
        """Since build_tree() cannot be recursive by itself, this allows it to recursively generate the tree elements.
        Processing format follows build_tree().

        :param children: Project assets
        :param parent: Parent directory of project
        :return: None
        """
        for child in children:
            child_dict = child.attrib
            grand_children = child.getchildren()
            if child_dict:
                if child.tag == 'built':
                    target = child_dict['target']
                    if 'configID' in child_dict.keys():
                        config_id = child_dict['configID']
                        text = f'configID {config_id} built for {target}'
                    else:
                        text = f'built for {target}'
                    item = QTreeWidgetItem(parent)
                    item.setText(0, text)
                else:
                    item = QTreeWidgetItem(parent)
                    item.setText(0, child_dict['name'])
                    if grand_children:
                        self.build_children(grand_children, item)
            else:
                if child.tag == 'rcc':
                    if grand_children:
                        self.build_children(grand_children, parent)
                else:
                    item = QTreeWidgetItem(parent)
                    item.setText(0, child.tag)
                    if grand_children:
                        self.build_children(grand_children, item)

    def get_rcc_targets(self) -> str:
        """Get the selected items from the RCC platform panel and separate them into individual --rcc-platform
        arguments. If 'NONE' is selected, the entire RCC target list is nullified.

        :returns: Listing of RCC platforms
        :rtype: str
        """
        rcc_counter = 0
        rcc_targets = ""

        self.rcc_targ_list = self.rcc_target.return_items()  # Get selected RCC targets
        return self.rcc_targ_list

    def get_hdl_targets(self) -> str:
        """Get the selected items from the HDL platform panel and separate them into individual --hdl-platform
        arguments. If 'NONE' is selected, the entire HDL target list is nullified.

        Functionally the same as get_rcc_targets().

        :returns: Listing of HDL platforms
        :rtype: str
        """
        hdl_counter = 0
        hdl_targets = ""

        self.hdl_targ_list = self.hdl_target.return_items()
        return self.hdl_targ_list

    def run_application(self) -> None:
        """Calls the appropriate 'ocpidev run' command for the selected OpenCPI asset based on which, if any, args are
        provided. If no args are provided, the default run command is executed."""
        before_arguments: str
        after_arguments: str
        run_arguments: str

        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)
        application: Union[Path, Any] = index_path.parents[0]
        application_dialog: RunApplicationDialogue = RunApplicationDialogue()
        application_dialog.show()
        if application_dialog.exec_():
            before_arguments, after_arguments, run_arguments = application_dialog.okay()
            if before_arguments != "" and after_arguments == "" and run_arguments == "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --before='{before_arguments}'"
                else:
                    cmd: str = f"cd {application} && " \
                               f"ocpidev run application {index_path.name} --before='{before_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command: str = self.parse_subprocess_cmd(cmd)
            elif before_arguments == "" and after_arguments != "" and run_arguments == "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --after='{after_arguments}'"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name} --after='{after_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            elif before_arguments == "" and after_arguments == "" and run_arguments != "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --run-arg='{run_arguments}'"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name} --run-arg='{run_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            elif before_arguments != "" and after_arguments != "" and run_arguments == "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --before='{before_arguments}' --after='{after_arguments}'"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name} --before='{before_arguments}' " \
                          f"--after='{after_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            elif before_arguments != "" and after_arguments == "" and run_arguments != "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --before='{before_arguments}' --run-arg='{run_arguments}'"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name} --before='{before_arguments}' " \
                          f"--run-arg='{run_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            elif before_arguments == "" and after_arguments != "" and run_arguments != "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --after='{after_arguments}' --run-arg='{run_arguments}'"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name} --after='{after_arguments}' " \
                          f"--run-arg='{run_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            elif before_arguments != "" and after_arguments != "" and run_arguments != "":
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications --before='{before_arguments}' --after='{after_arguments}' " \
                          f"--run-arg='{run_arguments}'"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name} --before='{before_arguments}' " \
                          f"--after='{after_arguments}' --run-arg='{run_arguments}'"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            else:
                if str(index_path).split('/')[-1] == 'applications':
                    cmd = f"cd {application} && " \
                          f"ocpidev run applications"
                else:
                    cmd = f"cd {application} && " \
                          f"ocpidev run application {index_path.name}"
                self.start(["bash", "-c", cmd], index_path.name)
                command = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(command)

    def reg_project(self):
        """Register selected project in default registry."""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        cmd: str = f"cd {index_path} && ocpidev register project"
        out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
            subprocess.run(["bash", "-c", cmd],
                           stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        command: str = self.parse_subprocess_cmd(cmd)
        self.console.on_update_text(out.stdout)  # Send command output to console
        self.console.on_update_text(out.stderr)
        self.console.on_update_text(command)

    def unreg_project(self):
        index: str = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        response: bool = self.confirm_delete(delete=False)
        if response:
            cmd: str = f"cd {index_path} && " \
                       f"ocpidev -f unregister project"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def refresh_proj(self, index_path):
        cmd: str = f"cd {index_path} && " \
                   f"ocpidev refresh project"
        out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
            subprocess.run(["bash", "-c", cmd],
                           stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        command: str = self.parse_subprocess_cmd(cmd)
        self.console.on_update_text(out.stdout)  # Send command output to console
        self.console.on_update_text(out.stderr)
        self.console.on_update_text(command)

    def del_project(self) -> None:
        """Deletes the selected project and all associated assets"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"" \
                       f"ocpidev -f delete project {index}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    # Commented out in case of future need
    # def del_registry(self) -> None:
    #     index: Union[str, PurePath, PathLike[str]] = self.get_item()
    #     response = self.confirm_delete(delete=True)
    #     if response:
    #         out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = subprocess.run(
    #             ["bash", "-c", f""
    #                            f"ocpidev -f delete registry {index}"],
    #             stderr=subprocess.STDOUT, stdout=subprocess.PIPE, universal_newlines=True)
    #         command = "COMMAND RAN: ocpidev -f delete registry {}".format(index)
    #         self.console.on_update_text(out.stdout)  # Send command output to console
    #         self.console.on_update_text(command)

    def del_app(self) -> None:
        """Deletes an application from the Project Explorer.

        Due to the way ocpidev functions, it can't handle a direct path index like 'delete project'. Thus the need to
        change to the index location and then move to its parent location to run the command.
        """
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)
        applications: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {applications} && cd .. && " \
                       f"ocpidev -f delete application {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_spec(self) -> None:
        """Delete a component specification.

        Assumes that the default path for the spec is
        ~/User_OpenCPI_Projects/<project>/components/<library>/specs/<spec>.
        Also assumes that the spec to delete is located in the parent library and not within the project or a different
        location.
        Has to change to the parent directory of the component spec, which should be the library, so that the spec
        is deleted without generating an error.
        """
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[1]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        path = pathlib.Path(index_path)
        library = path.parent
        if response:
            if index_path.suffix == 'xml':
                cmd: str = f"cd {library} && " \
                        f"ocpidev -f delete component {index_path.name}"
            else:
                cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete component {index_path.name.split('.')[0]}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_protocol(self) -> None:
        """Delete the selected protocol"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[1]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete protocol {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_lib(self) -> None:
        """Delete selected library"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete library {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_test(self) -> None:
        """Delete selected unit test"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete test {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_worker(self) -> None:
        """Delete the selected worker"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete worker {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_assembly(self) -> None:
        """Delete selected HDL assembly"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete hdl assembly {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_platform(self) -> None:
        """Delete selected HDL platform"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete hdl platform {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_prim_lib(self) -> None:
        """Delete selected HDL primitive library"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete hdl primitive library {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def del_prim_core(self) -> None:
        """Delete selected HDL primitive core"""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        library: Union[Path, Any] = index_path.parents[0]  # Get the path to the library directory
        response: bool = self.confirm_delete(delete=True)
        if response:
            cmd: str = f"cd {library} && " \
                       f"ocpidev -f delete hdl primitive core {index_path.name}"
            out: Union[CompletedProcess[Any], CompletedProcess[bytes]] = \
                subprocess.run(["bash", "-c", cmd],
                               stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
            command: str = self.parse_subprocess_cmd(cmd)
            self.console.on_update_text(out.stdout)  # Send command output to console
            self.console.on_update_text(out.stderr)
            self.console.on_update_text(command)

    def get_item(self) -> str:
        """Returns the string representation of the currently selected items index value.

        Gets the Qt index of the selected item and determines the string value of the item's file path.

        :return: File path for selected item
        :rtype: str
        """
        return self.fileSystemModel.filePath(self.treeView.currentIndex())

    def get_items(self) -> list:
        """Returns the items selected in the Project Explorer panel.

        Gets the Qt index values for the selected items, determines the string representation of the items' file paths,
        and appends those strings to a list.

        :return: File paths for selected items
        :rtype: list
        """
        items: List[str] = []
        item: Union[str, PurePath, PathLike[str]]
        for item in self.treeView.selectedIndexes():
            items.append(self.fileSystemModel.filePath(item))
        return items

    @staticmethod
    def confirm_delete(delete: bool) -> bool:
        """Verify that the user wants to delete selection

        :returns: Affirmative or negative confirmation
        :rtype: bool
        """
        msg = DeleteConfirm(delete)
        response = msg.message()
        return response

    def edit_file(self) -> None:
        """Allows editing of selected file in external editor. Editor is system default for selected file type."""
        index: Union[str, PurePath, PathLike[str]] = self.get_item()
        index_path: Union[Path, Any] = Path(index)  # Convert string to path-type
        if index_path.suffix == ".xml" or index_path.suffix == ".cpp" or index_path.suffix == ".txt" or \
                index_path.suffix == ".cc" or index_path.suffix == ".c" or index_path.suffix == ".vhd" or \
                index_path.suffix == ".v" or index_path.suffix == ".py" or index_path.suffix == ".sh" or \
                index_path.suffix == ".deps" or index_path.suffix == ".mk" or index_path.suffix == ".h" or \
                index_path.suffix == ".hh" or index_path.suffix == ".rst" or index_path.suffix == ".exports" or \
                index_path.name == "Makefile" or index_path.name == "package-id" or index_path.name == "workers":
            subprocess.run(["xdg-open", index])

    def refresh_view(self) -> None:
        """Refreshes Project Explorer panel with items in updated user_proj_path"""
        new_path: Optional[QModelIndex] = self.fileSystemModel.setRootPath(str(window.user_proj_path))
        self.treeView.setRootIndex(new_path)
        # do not delete this code because it will be for the ocpi assets menu for a later release

        # if self.checkBoxOcpiAssetsView.isChecked():
        #     treewidget = QTreeWidget()
        #     self.build_folder(str(window.user_proj_path), treewidget)
        #     root: Optional[QModelIndex] = self.fileSystemModel.setRootPath(str(treewidget))
        #     self.treeView.setRootIndex(root)
        # if not self.checkBoxOcpiAssetsView.isChecked():
        #     root: Optional[QModelIndex] = self.fileSystemModel.setRootPath(f"{self.proj_path}")


class OutputConsole(QWidget):
    """Collects output data from subprocess calls to ocpidev. Prints the output to the console view."""
    cursor: Union[QTextCursor, QTextCursor]
    process: Union[QPlainTextEdit, QPlainTextEdit]

    def __init__(self, process: QPlainTextEdit) -> None:
        """Create the process that manages text editing on the console"""
        super().__init__()

        self.process = process
        self.cursor = self.process.textCursor()  # Create cursor to move in console
        self.process.moveCursor(QTextCursor.Start)  # Ensure text starts at beginning of console
        self.process.ensureCursorVisible()  # Move scroll window to keep cursor position visible

    def on_update_text(self, text: str) -> None:
        """Add new text to the console as stdout gets more data"""
        self.process.appendPlainText(text)
        self.process.setTextCursor(self.cursor)  # Assign the cursor object to the output console
        self.process.moveCursor(QTextCursor.End)  # Scroll window to end of text


class RccTargets(QListView):
    """Populates RCC target list and returns selected target"""
    rcc_active: bool
    stand_item: Union[QStandardItem, QStandardItem]
    selection: QStandardItemModel
    model: Union[QStandardItemModel, QStandardItemModel]
    default_dir: str
    rcc_list: Union[QListView, QListView]

    def __init__(self, main_window, list_view: QListView, rcc_active, rcc_config_list) -> None:
        """Populate the RCC panel with OpenCPI RCC platforms"""
        super().__init__()
        self.rcc_list = list_view  # Connect to appropriate widget in MainWindow
        self.default_dir = main_window.ocpi_path
        self.rcc_active = rcc_active
        self.rcc_config_list = rcc_config_list
        self.model = QStandardItemModel(self.rcc_list)  # Generic data storage model
        self.selection = QStandardItem("")  # Set selection to empty value
        self.rcc_list.clicked.connect(self.find_item)  # Select an item
        self.fill_list(self.rcc_config_list)

    def fill_list(self, rcc_list: list) -> None:
        """Populate the RCC target platform widget"""
        self.model.clear()
        self.model.appendRow(QStandardItem("NONE"))
        for target in rcc_list:
            self.stand_item = QStandardItem(target)  # Create items for use in self.model
            self.model.appendRow(self.stand_item)

        self.rcc_list.setModel(self.model)  # Assign the model to the view
        self.rcc_list.setSelectionMode(self.rcc_list.ExtendedSelection)
        self.rcc_list.show()  # Display the view

    def find_item(self, index: QStandardItem) -> None:
        """Get the index of the selected item"""
        self.selection = self.model.itemFromIndex(index)

    def return_items(self) -> list:
        """Get the text of the selected item(s) based on index value.

        Checks if RCC panel is active. If not, a list with an empty string is returned. If so, it then checks whether
        the RCC selection is "NONE"; if so, a list with an empty string is passed, otherwise the string value(s) of the
        selected item(s) is appended to a list and returned.
        """
        rcc_targets: List[str] = []
        if self.rcc_active.isChecked():
            selections: Union[object, Any] = self.rcc_list.selectedIndexes()
            selection: QModelIndex
            for selection in selections:
                if self.model.itemFromIndex(selection).text() == "NONE":
                    rcc_targets.append("NONE")
                else:
                    rcc_targets.append(self.model.itemFromIndex(selection).text())
            return rcc_targets
        else:
            return rcc_targets.append("")  # Ensures unit test isn't executed with "--rcc-platform None" argument

    def refresh_rcc_list(self):
        pass


class HdlTargets(QListView):
    """Populates HDL target list and returns selected target.

    Checks if RCC panel is active. If not, an empty string is returned. If so, it then checks whether the RCC
    selection is "NONE"; if so, an empty string is passed, otherwise the string value of the selected item is
    returned.
    """
    hdl_active: bool
    selection: QStandardItemModel
    model: Union[QStandardItemModel, QStandardItemModel]
    default_dir: str
    hdl_list: Union[QListView, QListView]

    def __init__(self, main_window, list_view: QListView, hdl_active, hdl_config_list) -> None:
        """Populate HDL panel with OpenCPI HDL platforms"""
        super().__init__()
        self.hdl_list = list_view  # Connect to appropriate widget in MainWindow
        self.default_dir = main_window.ocpi_path
        self.hdl_active = hdl_active
        self.hdl_config_list = hdl_config_list
        self.model = QStandardItemModel(self.hdl_list)  # Generic data storage model
        self.selection = QStandardItem("")  # Set selection to empty value
        self.stand_item = QStandardItem("")  # Items for use in self.model
        self.hdl_list.clicked.connect(self.find_item)  # Select an item
        self.fill_list(self.hdl_config_list)

    def fill_list(self, hdl_list: list) -> None:
        """Populate HDL target platform widget"""
        self.model.clear()
        self.model.appendRow(QStandardItem("NONE"))
        for target in hdl_list:
            self.stand_item = QStandardItem(target)
            self.model.appendRow(self.stand_item)

        self.hdl_list.setModel(self.model)  # Assign the model to the view
        self.hdl_list.setSelectionMode(self.hdl_list.ExtendedSelection)
        self.hdl_list.show()  # Display the view

    def find_item(self, index: QStandardItem) -> None:
        """Get the index of the selected item"""
        self.selection = self.model.itemFromIndex(index)

    def return_items(self) -> list:
        """Get the text of the selected item(s) based on index value.

        Checks if HDL panel is active. If not, a list with an empty string is returned. If so, it then checks whether
        the HDL selection is "NONE"; if so, a list with an empty string is passed, otherwise the string value(s) of the
        selected item(s) is appended to a list and returned.
        """
        hdl_targets: List[str] = []
        if self.hdl_active.isChecked():
            selections: Union[object, Any] = self.hdl_list.selectedIndexes()
            selection: QModelIndex
            for selection in selections:
                if self.model.itemFromIndex(selection).text() == "NONE":
                    hdl_targets.append("")
                else:
                    hdl_targets.append(self.model.itemFromIndex(selection).text())
            return hdl_targets


class RunApplicationDialogue(QDialog, Ui_Run_Application_Arguments):
    """Creates the Run Application dialog to capture any optional user-provided arguments"""

    run_arguments: QLineEdit
    after_arguments: QLineEdit
    before_arguments: QLineEdit

    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()

        self.before_arguments = self.Befor_Arguments_lineEdit
        self.after_arguments = self.Afte_Arguments_lineEdit
        self.run_arguments = self.Run_Arguments_lineEdit

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str, str]:
        """Gathers and returns Run Application arguments when Okay button clicked

        :returns: Arguments for Run Application command
        :rtype: tuple
        """
        return self.before_arguments.text(), self.after_arguments.text(), self.run_arguments.text()


class SetRegistryDialog(QDialog, Ui_Set_Reg_Dialog):
    """Creates the dialog box for the user to specify the new registry for the selected project"""
    dir_dialog: QToolButton
    registry_field: QLineEdit

    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()

        self.registry_field = self.Reg_Path_lineEdit
        self.registry_field.setText(f"{window.user_proj_path}")
        self.dir_dialog = self.toolButton
        self.dir_dialog.clicked.connect(self.show_users_proj_dir)

    def show_users_proj_dir(self) -> None:
        """Displays the assumed location of /opencpi directory"""
        reg_path: str = QFileDialog.getExistingDirectory(self, "Open Directory", f"{window.user_proj_path}",
                                                         QFileDialog.ShowDirsOnly)
        self.registry_field.setText(reg_path)

    def okay(self) -> str:
        """Returns the name of the new registry

        :returns: Registry name
        :rtype: str
        """
        return self.registry_field.text()


class DeleteConfirm(QWidget):
    """Creates warning message to confirm that user wants to delete selected Project Explorer item"""
    delete: bool
    proceed: bool

    def __init__(self, delete: bool, parent: object = None) -> None:
        super().__init__(parent)
        self.proceed = False
        self.delete = delete
        self.setup_ui()
        self.setWindowFlags(Qt.CustomizeWindowHint | Qt.FramelessWindowHint)

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def message(self) -> bool:
        """Checks which asset type is selected for deletion and provides the appropriate message box. If the user's
        response is True, the deletion occurs.

        :returns: Confirmation of deletion
        :rtype: bool
        """
        if self.delete:
            response: Optional[bool] = QMessageBox.question(self, "Delete",
                                                            "Are you sure you want to delete this item?",
                                                            QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        else:
            response = QMessageBox.question(self, "Unregister", "Are you sure you want to unregister this item?",
                                            QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if response == QMessageBox.Yes:
            self.proceed = True
        return self.proceed


class WorkerSignals(QObject):
    """Signals available from a running worker thread

    Supported signals are:
    finished
        No data
    error
        `tuple` (exctype, value, traceback.format_exc()
    result
        `str` data returned from processing, job ID
    progress
        `int` indicating % progress
    """
    error: Union[pyqtSignal, pyqtSignal] = pyqtSignal(str, str)
    result: Union[pyqtSignal, pyqtSignal] = pyqtSignal(str, str)

    finished: Union[pyqtSignal, pyqtSignal] = pyqtSignal(str)
    progress: Union[pyqtSignal, pyqtSignal] = pyqtSignal(str, int)
    status: Union[pyqtSignal, pyqtSignal] = pyqtSignal(str, str)


class WorkerKilledException(Exception):
    """Create a new exception but don't do anything about it"""
    pass


class Worker(QRunnable):
    """Worker thread

    Inherits from QRunnable to handle worker thread setup, signals and wrap-up.

    :param command: Command for subprocess to execute
    :param name: Index name of the OpenCPI asset
    """
    output: CompletedProcess
    is_killed: bool
    job_id: str
    command: list
    signals: WorkerSignals

    def __init__(self, command: list, name: str) -> None:
        super().__init__()
        self.command = command
        self.signals = WorkerSignals()  # Store constructor arguments (re-used for processing).
        self.job_id = name + "--" + str(uuid.uuid4())  # Give this job a unique ID.
        self.signals.status.emit(self.job_id, STATUS_WAITING)
        self.is_killed = False

    @pyqtSlot()
    def run(self) -> None:
        """Initialize the runner method with passed command and asset name

        Generates the "running" status for the job manager and, when complete, the "complete" status. When complete,
        the entire subprocess output is dumped to the output console. The emit signals use threads to communicate w/
        the rest of the program. Directly calling JobQueue.display_results() is not necessary and, in fact, will cause
        a queue error.

        If the job is stopped via the pushbutton, the subprocess is terminated and the job color changed.

        General exceptions are caught and indicated by a red job color. The specific error is not considered, as it is
        ocpidev related.
        """
        self.signals.status.emit(self.job_id, STATUS_RUNNING)
        self.signals.progress.emit(self.job_id, 100)  # Fill queue job w/ running color
        try:
            env = os.environ.copy()
            env['TERM'] = 'tty'
            self.output = subprocess.run(self.command, 
                                         stderr=subprocess.PIPE, 
                                         stdout=subprocess.PIPE,
                                         universal_newlines=True, 
                                         env=env)
            if self.is_killed:  # Job is manually stopped
                raise WorkerKilledException
            self.signals.progress.emit(self.job_id, 100)  # Fill queue job w/ completed color
            if self.output.returncode != 0:
                raise RuntimeError
            else:
                self.signals.result.emit(self.output.stderr, self.job_id)  # Dump output data and get worker name
        except WorkerKilledException:  # Worker stopped
            self.signals.status.emit(self.job_id, STATUS_STOPPED)  # Set job color to grey
        except RuntimeError:  # Catch any possible errors, whether Python or ocpidev based
            self.signals.status.emit(self.job_id, STATUS_ERROR)  # Set job color to red
            self.signals.result.emit(self.output.stderr, self.job_id)  # Dump error information to output console
        else:
            self.signals.status.emit(self.job_id, STATUS_COMPLETE)  # Set job color to green
        self.signals.finished.emit(self.job_id)

    def kill(self) -> None:
        """The job is manually stopped by the user"""
        self.is_killed = True


class WorkerManager(QAbstractListModel):
    """Manager to handle worker queues and state.

    Also functions as a Qt data model for a view displaying progress for each worker.

    _workers is a dictionary that holds the job ID and worker object.
    _state is a dictionary that holds the job progress and status information.
    """
    status_timer: Union[QTimer, QTimer]
    max_threads: int
    threadpool: Union[QThreadPool, QThreadPool]
    _workers: Dict[Any, Any] = {}
    _state: Dict[Any, Any] = {}
    status: Union[pyqtSignal, pyqtSignal] = pyqtSignal(str)

    def __init__(self) -> None:
        """Create the worker threadpool and timer.

        Worker threads are set to the default for the OS. Ideal thread count is based on number of processor cores
        available, both physical and logical. Minimum is 1 thread.

        The timer is used to update the worker thread information as jobs are created and finished.
        """
        super().__init__()

        self.threadpool = QThreadPool()  # Create a threadpool for our workers.
        self.max_threads = self.threadpool.maxThreadCount()

        self.status_timer = QTimer()  # Create timer for worker thread status
        self.status_timer.setInterval(100)
        self.status_timer.timeout.connect(self.worker_status)
        self.status_timer.start()
        self.cmd: str = ""

    def worker_status(self) -> None:
        """Check thread status for number of running and waiting threads, plus the max threads available"""
        n_workers: int = len(self._workers)
        running: int = min(n_workers, self.max_threads)
        waiting: int = max(0, n_workers - self.max_threads)

        window.Running_Jobs_label.setText(f"{running}")
        window.Jobs_Waiting_label.setText(f"{waiting}")
        window.Thread_Count_label.setText(f"{self.max_threads}")

    def enqueue(self, worker: Any) -> None:
        """Add new jobs to the worker queue.

        If a worker thread is available, the job will start running. Otherwise, it will sit in the queue until a thread
        is available.

        :param worker: Job in queue
        """
        worker.signals.status.connect(self.receive_status)
        worker.signals.progress.connect(self.receive_progress)
        worker.signals.finished.connect(self.done)

        self.threadpool.start(worker)
        self._workers[worker.job_id] = worker  # Add worker to dictionary
        self._state[worker.job_id] = DEFAULT_STATE.copy()  # Set default status to waiting, 0 progress.
        self.cmd = worker.command
        self.layoutChanged.emit()

    def receive_status(self, job_id: str, status: str) -> None:
        """Get the status of a worker

        :param job_id: Unique worker identification
        :param status: Worker status
        """
        self._state[job_id]["status"] = status
        self.layoutChanged.emit()

    def receive_progress(self, job_id: str, progress: str) -> None:
        """Get the progress of a worker.

        Normally this would be used to update a progress bar, but it is used here to check whether a job is simply
        running or completed.

        :param job_id: Unique worker identification
        :param progress: Completion state of worker
        """
        self._state[job_id]["progress"] = progress
        self.layoutChanged.emit()

    def done(self, job_id: str) -> None:
        """Task/worker complete.

        Remove job from the active workers dictionary but keep it in worker_state, as this is used to display
        past/complete workers too.

        :param job_id: Unique worker identification
        """
        del self._workers[job_id]
        self.layoutChanged.emit()

    def cleanup(self) -> None:
        """Remove any complete/failed/stopped workers from worker_state."""
        job_id: str
        status_info: Tuple[Union[int, slice]]

        for job_id, status_info in list(self._state.items()):
            if status_info["status"] in (STATUS_COMPLETE, STATUS_ERROR, STATUS_STOPPED):
                del self._state[job_id]
        self.layoutChanged.emit()

    def del_selected(self, job_id):
        """Delete a selected worker from the job manager list"""
        del self._state[job_id]
        self.layoutChanged.emit()

    def kill(self, job_id: str) -> None:
        """Terminate a worker when the Stop button is pressed

        :param job_id: Unique worker identification
        """
        if job_id in self._workers:
            self._workers[job_id].kill()
            

    def data(self, index: int, role: str) -> Tuple:
        """Model interface

        Gets the job ID and current state of each worker.

        :param index: Index value of worker in queue
        :param role: The key data to be rendered in the form of text.
        :return: job ID and worker state
        :rtype: tuple
        """

        if role == Qt.DisplayRole:
            job_ids = list(self._state.keys())

            # Check for workers
            if not self._state.items():
                return None, None, None
           
            job_id = job_ids[index.row()]
            return job_id, self._state[job_id], self.cmd

    def rowCount(self, index: int) -> int:
        """Get the number of jobs in the queue

        :param index: Position of worker in _state dictionary
        :return: Number of items in _state dictionary
        """
        return len(self._state)

    @property
    def state(self) -> dict:
        """Returns the dictionary of the worker's current state"""
        return self._state


class ProgressBarDelegate(QStyledItemDelegate):
    """Creates the object that will be shown in the Job Manager.

    Includes both the name and unique hash of the worker, as well as handling the color changes of the job as its status
    changes.
    """

    def paint(self, painter: QPainter, option: object, index: int) -> None:
        """Low-level painter to populate worker queue.

        Provides the name of the worker as well as coloring the queue line with the appropriate color, based on the job
        status.

        :param painter: QPainter instance
        :param option: Selected options for painting to queue object
        :param index: Position of worker in queue
        """
        self.job_id: str
        self.data: Tuple[Union[int, slice]]
        self.job_id, self.data, self.cmd = index.model().data(index, Qt.DisplayRole)
        text: str = self.job_id
        if self.data["progress"] > 0:
            text = ' '.join([STATUS_CHARACTERS[self.data["status"]], text])
            color: Union[QColor, QColor] = QColor(STATUS_COLORS[self.data["status"]])
            
            brush: Union[QBrush, QBrush] = QBrush()
            brush.setColor(color)
            brush.setStyle(Qt.SolidPattern)

            width: Union[float, Any] = option.rect.width() * self.data["progress"] / 100

            rect: Union[QRect, QRect] = QRect(option.rect)  # Copy of the rect, so we can modify.
            rect.setWidth(int(width))

            painter.fillRect(rect, brush)
            
        pen: Union[QPen, QPen] = QPen()
        pen.setColor(Qt.black)
        painter.drawText(option.rect, Qt.AlignLeft, text)

        if option.state & QStyle.State_Selected:
            painter.drawRect(option.rect)


class JobQueue(QAbstractListModel):
    job_actions: Union[QMenu, QMenu]
    text: Union[QPlainTextEdit, QPlainTextEdit]
    workers: WorkerManager
    progress: Union[QListView, QListView]

    def __init__(self, main_window: QMainWindow, list_view: QListView) -> None:
        """"Sets up the worker queue for build jobs.

        Creates instance of WorkerManager and fills it with jobs as they are created. Also manages the starting and
        stopping of each job, as well as sending data to the output console.

        :param main_window: Main window for GUI application
        :param list_view: Job queue panel in main GUI
        """
        super().__init__()
        self.workers = WorkerManager()

        self.progress = list_view  # Connect to panel in MainWindow
        self.progress.setModel(self.workers)
        self.delegate: ProgressBarDelegate = ProgressBarDelegate()
        self.progress.setItemDelegate(self.delegate)
        self.job_actions = QMenu(self.progress)

        self.text = QPlainTextEdit()
        self.text.setReadOnly(True)

        stop: QPushButton = main_window.Stop_Job_pushButton
        stop.pressed.connect(self.stop_worker)

        clear: QPushButton = main_window.Clear_List_pushButton
        clear.pressed.connect(self.workers.cleanup)
        self.job_menu()

    def job_menu(self) -> None:
        """Adds right-click context menu to the Job Manager panel"""
        self.job_actions.addAction("Jump to location", self.job_jump)
        self.job_actions.addAction("Delete job", self.del_job)
        self.job_actions.addAction("Rerun job", self.rerun_job)
        self.progress.installEventFilter(self)

    def eventFilter(self, source: QListView, event: QEvent) -> bool:
        """Execute the correct context menu function call.

        Checks if the Qt event is the a call to a Qt context menu. If so, it then checks if the caller of the event is
        the Job Manager panel. If it is, the method call to execute it caught and the method executed.

        :returns: Confirmation of Qt event
        :rtype: bool
        """
        if event.type() == QEvent.ContextMenu:
            if source == self.progress:
                self.job_actions.exec_(event.globalPos())
                return True

        return super().eventFilter(source, event)

    def start_worker(self, command: list, name: str) -> None:
        """Creates a new subprocess thread with the passed command and worker name.

        :param command: ocpidev command
        :param name: Name of OpenCPI asset from Project Explorer panel
        """
        self.subproc_worker: Worker = Worker(command, name)
        self.subproc_worker.signals.result.connect(self.display_result)
        self.workers.enqueue(self.subproc_worker)

    def stop_worker(self) -> None:
        """Stop a job in progress.

        Get the tuple provided by self.workers.data(). Extract the job ID from the selected worker, ignoring the other
        two results, and kill that job.
        """
        selected: Union[object, Any] = self.progress.selectedIndexes()
        index: int
        for index in selected:
            job_id: str
            
            job_id, _, _ = self.workers.data(index, Qt.DisplayRole)
            if job_id is not None:
                self.workers.kill(job_id)

    @staticmethod
    def display_result(data: str, name: str) -> None:
        """Dump ocpidev output to output console. Provides a separator line to help clarify individual job output.

        :param data: Results of ocpidev command
        :param name: Name of worker
        """
        window.console_output.on_update_text("***" + name.upper() + "***\n")
        window.console_output.on_update_text(data)
        window.console_output.on_update_text("************************************************************\n\n")

    def job_jump(self) -> None:
        """Jump to the correct location in the output console for the job selected in job manager.

        While the for loop is supposed to iterate through a number of selections, only one selection should be active,
        based on the rest of the code.
        """
        console: Union[QPlainTextEdit, QPlainTextEdit] = window.console_output.process
        selected: Union[Any, Any] = self.progress.selectedIndexes()  # Get the job from job manager

        index: Any
        for index in selected:
            job_id: str
            job_id = self.workers.data(index, Qt.DisplayRole)[0]
            console.moveCursor(QTextCursor.Start)  # Move the output console cursor to the beginning of the console
            console.find(job_id)  # Jump to the correct location

    def del_job(self) -> None:
        """Delete selected job from job manager"""
        selected: Union[Any, Any] = self.progress.selectedIndexes()  # Get the job from job manager
        index: Any
        for index in selected:
            job_id: str
            job_id = self.workers.data(index, Qt.DisplayRole)[0]
            self.workers.del_selected(job_id)

    def rerun_job(self) -> None:
        """Rerun selected job from job manager.

        Gets the index of the selected item and extracts the job ID and command that was executed for that job (ignores
        unnecessary data from the worker data). A new thread is created that runs the command while using the same job
        ID.

        The job ID for the new worker simply appends another has value to the end. This allows the user to verify that
        the same job has been reran, as all the original hash identifiers are included each time.
        """
        selected: Union[Any, Any] = self.progress.selectedIndexes()  # Get the job from job manager
        index: Any
        for index in selected:
            job_id: str
            cmd: str
            job_id, _, cmd = self.workers.data(index, Qt.DisplayRole)
            ProjectExplorerTree.start(cmd, job_id)


class InstallPlatformDialog(QDialog, Ui_Install_Platform_Dialog):
    """Runs ocpiadmin Install Platform command"""
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.setup_ui()
        self.show()

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str, str, str]:
        """Return the platform name and, optionally, the package ID, URL to download from, and Git revision hash.

        :returns: Platform name, package ID, platform URL, and platform Git revision hash digest
        :rtype: tuple
        """
        platform_name = self.Platform_Name_lineEdit.text()
        package_id = self.Package_ID_lineEdit.text()
        url = self.URL_lineEdit.text()
        git_rev = self.Git_Rev_lineEdit.text()
        minimal = self.minimal_install_checkBox.isChecked()

        return platform_name, package_id, url, git_rev, minimal


class DeployPlatformDialog(QDialog, Ui_Deploy_Platform_Dialog):
    """Allow user to specify RCC and HDL targets to deploy platform"""
    hdl_platform: QComboBox
    rcc_platform: QComboBox

    def __init__(self, main_window: MainWindow) -> None:
        super().__init__()
        self.setupUi(self)
        self.show()
        rcc_platforms = ocpidev_show(self, 'rcc platforms', delimiter=' ')
        self.RCC_Platform_comboBox.addItems(rcc_platforms)
        hdl_platforms = ocpidev_show(self, 'hdl platforms', delimiter=' ')
        self.HDL_Platform_comboBox.addItems(hdl_platforms)

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def okay(self) -> Tuple[str, str]:
        """Return the selected RCC and HDL platform names

        :returns: RCC and HDL selected platforms
        :rtype: tuple
        """
        rcc_platform = self.RCC_Platform_comboBox.currentText()
        hdl_platform = self.HDL_Platform_comboBox.currentText()

        return rcc_platform, hdl_platform


class AboutWindow(QDialog, Ui_About_Dialog):
    """Sets up the GUI About window with logo, version information, and links to copyright and license info."""
    license: QPushButton
    copyright: QPushButton

    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.setup_ui()
        self.show()

        self.copyright = self.Copyright_pushButton
        self.copyright.clicked.connect(self.open_copyright)

        self.license = self.License_pushButton
        self.license.clicked.connect(self.open_license)

    def setup_ui(self) -> None:
        qt_rectangle: QRect = self.frameGeometry()
        center_point: QPoint = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    @staticmethod
    def open_copyright() -> None:
        """Open external text editor to display OpenCPI copyright information."""
        subprocess.run(["xdg-open", f"{window.ocpi_path}/COPYRIGHT"])

    @staticmethod
    def open_license() -> None:
        """Open external text editor to display OpenCPI license information."""
        subprocess.run(["xdg-open", f"{window.ocpi_path}/LICENSE.txt"])


if __name__ == "__main__":
    import signal

    signal.signal(signal.SIGINT, signal.SIG_DFL)  # Close application if Ctl-C in terminal

    app: Union[QApplication, QApplication] = QApplication(sys.argv)
    if 'Fusion' in QStyleFactory.keys():
    # If 'Fusion' style is available, set it to standardize UI between OSs
        app.setStyle('Fusion')
    splash_pix = QPixmap("opencpi_logo_2.png")  # Get image for splash screen
    splash = QSplashScreen(splash_pix)  # Create splash screen
    progressBar = QProgressBar(splash)  # Add a progress bar to keep users happy while GUI loads
    progressBar.setMaximum(20)  # Determines how long it takes progress bar to finish
    progressBar.setGeometry(0, splash_pix.height() - 150, splash_pix.width(), 20)
    splash.show()  # Display the logo
    window: MainWindow = MainWindow()  # Create the main GUI
    options = ['--projects-path', '--ocpi-path']
    if len(sys.argv) > 1:
        if "--help" in sys.argv or "-h" in sys.argv:
            print("Usage: ocpigui [OPTIONS]\n"
                            "\t--projects-path (string): path to the top level projects folder.\n"
                            "\t--ocpi-path (string): path to the opencpi installation.")
            sys.exit()
        for option in options:
            if option not in sys.argv:
                continue
            index = sys.argv.index(option)
            try:
                arg = sys.argv[index + 1]
                sys.argv.pop(index+1)
                sys.argv.pop(index)
            except IndexError:
                print('Expected argument after option "{}"'.format(option))
                print("Usage: ocpigui [OPTIONS]\n"
                            "\t--projects-path (string): path to the top level projects folder.\n"
                            "\t--ocpi-path (string): path to the opencpi installation.")
                sys.exit()
            if option == '--ocpi-path':
                window.ocpi_path = arg
                window.ocpi_dir_save()
            elif option == '--projects-path':
                window.user_proj_path = arg
                window.proj_path_save()
                try:
                    window.project.refresh_view()
                    window.project.current_pe_path.setText(window.user_proj_path)
                    window.project.proj_path = window.user_proj_path
                except AttributeError:  # Ignore initial startup error
                    pass
        if len(sys.argv) > 1:
            print('Unrecognized option "{}"'.format(sys.argv[1]))
            print("Usage: ocpigui [OPTIONS]\n"
                "\t--projects-path (string): path to the top level projects folder.\n"
                "\t--ocpi-path (string): path to the opencpi installation.")
            sys.exit()
    else:
        window.set_proj_path()  # Call new project path dialog
    for i in range(1, 21):  # Run splash screen progress bar; final value should be same as max value above
        progressBar.setValue(i)
        t = time.time()
        while time.time() < t + 0.05:  # Determines speed of progress bar
            app.processEvents()  # Run the progress bar
            splash.showMessage("Initializing GUI...", Qt.AlignBottom)
            splash.clearMessage()
    
    window.show()  # Display the GUI
    splash.finish(window)  # Close splash screen when GUI launches

    sys.exit(app.exec_())
