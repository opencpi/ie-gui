# OpenCPI GUI
OpenCPI GUI is a graphical environment that supplements traditional command-line functionality of OpenCPI. While not 
designed to have complete feature-parity with command-line tools, the GUI supports many of the commands and features 
available in CLI OpenCPI.

## Installation
Source installation is the primary way of using OpenCPI GUI. Prior to running, the user will have to install PyQt5 by 
running the command `sudo apt install python3-pyqt5` for Debian-based distros and 
`sudo yum install python36-qt5.x86_64` for Red Hat-based distros.

### Cloning from Gitlab repository
To obtain the GUI source code, it needs to be cloned from the Gitlab repository using `git`. If `git` is not already
installed, use the command `sudo apt install git` (Debian-based) or `sudo yum install git` (Red Hat-based).

Change to the directory where you want the GUI code to reside. The command 
`git clone https://gitlab.com/opencpi/ie-gui.git` will clone the GUI source code to the local system. 

## Running OpenCPI GUI
To launch OpenCPI GUI, use `cd <path_to>/ie-gui` directory and execute the command `python3 ./OpenCPI_GUI.py`.
There is a new way to run the OpenCPI GUI is to `cd <path_to>/ie-gui` and then execute `./install-gui.sh`.
Once ran you can run in your terminal `ocpigui` and this will launch the GUI. There are 2 flags for this command
run `ocpigui --help` to get more information on those flags

### Initial setup
Upon first launch, the GUI will have two dialogs appear. One will ask for the installation path of `/opencpi`, the other
will ask for the project path to display. If opening a pre-existing project, it is recommended to use the path
`~/User_OpenCPI_Projects` for new projects.

## Documentation
The user guide for OpenCPI GUI can be found within the cloned source code, at 
`<path_to>/ie-gui/doc/odt/OpenCPI_GUI_User_Guide.fodt`. 

## License
OpenCPI GUI follows the same licensing as the OpenCPI project. More information can be found at 
https://gitlab.com/opencpi/opencpi/-/blob/develop/LICENSE.txt.