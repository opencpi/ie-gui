from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestUnitTest:
    def test_app_name(self, qtbot):
        window = OpenCPI_GUI.NewUnitTestDialog()
        window.show()
        qtbot.addWidget(window)

        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Library_comboBox, "dummy_lib")
        qtbot.keyClicks(window.Component_Spec_comboBox, "dummy_comp")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Component_Spec_comboBox.currentText() == "dummy_comp"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Library_comboBox.currentText() == "dummy_lib"
