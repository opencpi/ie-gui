# -*- coding: utf-8 -*- 
import squish
import test
from pathlib import Path
from os import environ 
from pageobjects import ocpi
from pageobjects import init_env_paths

def start():
    """Launchs the GUI and confirms GUI opens."""
    test.log('[OCPI GUI] Start OCPI GUI')
     # Grabbing the ocpigui path
    ocpi_gui = str(Path(environ['OCPI_ROOT_DIR'], 'exports', environ['OCPI_TOOL_PLATFORM'], 
         'bin', 'ocpigui'))
    squish.startApplication(ocpi_gui)
    squish.clickButton(squish.waitForObject(init_env_paths.EXPLORER_OK_BUTTON))
    squish.waitForObject(ocpi.MAIN_WINDOW)