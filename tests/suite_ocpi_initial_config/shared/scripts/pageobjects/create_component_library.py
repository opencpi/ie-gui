# -*- coding: utf-8 -*-
import squish
import test 
 
FRAME = {"name": "New_Library_Dialog", "type": "NewLibraryDialog"}
LIB_NAME_LINE = {"name": "Library_Name_lineEdit", "type": "QLineEdit", "window": FRAME}
LIB_PROJECT = {"name": "Library_Project_comboBox", "type": "QComboBox","window": FRAME}

def enter():
    """Creates a coponent library."""
    test.log('[Component Library] Fill Fields With Data')
    squish.type(squish.waitForObject(LIB_NAME_LINE), 'components')
    squish.mouseClick(squish.waitForObjectItem(LIB_PROJECT, 'Demo'))
    
