# -*- coding: utf-8 -*-
import squish
import test 

FRAME = {"name": "New_Component_Dialog", "type": "NewComponentDialog"}
NAME_LINE = {"name": "Component_Name_lineEdit", "type": "QLineEdit", "window": FRAME}
PROJ_NAME = {"name": "Component_Project_comboBox", "type": "QComboBox", "window": FRAME}

def enter(spec_name, project_name):
    """Fills in the component spec fields.
    
    Args:
        spec_name: name of the component spec
        project_name: name of the associated project
    """
    test.log('[Component Spec] Enter Spec Data')
    squish.type(squish.waitForObject(NAME_LINE), spec_name)
    squish.mouseClick(squish.waitForObjectItem(PROJ_NAME, project_name))