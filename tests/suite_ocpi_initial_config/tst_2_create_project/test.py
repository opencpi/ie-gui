# -*- coding: utf-8 -*-
import ocpi_gui
import test 
from pageobjects import ocpi, create_project
from pathlib import Path
from os import environ

def main():
    """Will create a new project.
    
    Confirms that correct dirs/files are generated.
    Determines that the project is registered.
    """
    ocpi_gui.start()
    
    test.startSection('New Project Cancel')
    ocpi.open_project_dialog()
    # Confirming that the New Project window opens.
    test.compare(waitForObjectExists(create_project.FRAME).visible, True, 
        'Verify New Project Window Displays')
    # Confirming that the Cancel button works here.
    ocpi.cancel(ocpi.PROJ_CANCEL_BUTTON)
    # Snooze for 2 seconds before confirming that the new project window is closed.
    snooze(2)
    test.compare(object.exists(create_project.FRAME), False, 'Verify New Project Window Closes')
    test.endSection()
    
    test.startSection('New Project OK')
    ocpi.open_project_dialog()
    # Confirming that the New Project window opens.
    test.compare(waitForObjectExists(create_project.FRAME).visible, True, 
        'Verify New Project Window Displays')
    file = Path(environ['HOME'], 'User_OpenCPI_Projects', 'Demo') 
    if (file.exists()):
        test.fail("Demo Project already exists")
    create_project.enter('Demo', 'User\\_OpenCPI\\_Projects', 'test', 'test_project',
     'ocpi.assets', register=True)
    ocpi.confirm(ocpi.PROJ_OK_BUTTON)
    # Snooze for 2 seconds before confirming that the new project is created.
    snooze(2) 
    file = Path(environ['HOME'], 'User_OpenCPI_Projects', 'Demo') 
    # Verifies that the Demo directory was created.
    test.verify(file.exists(), 'Demo Project Created')
    test.endSection()
    
    test.startSection('XML Files Comparison')
    project_xml = str(Path(environ['HOME'], 'User_OpenCPI_Projects', 'Demo', 'Project.xml'))
    # Compares generated project xml to an expected project xml.
    test.compareXMLFiles('testdata/Project.xml', project_xml)
    test.endSection()
  
    test.startSection('Verify Newly Created Project is Registered')
    # Check in the /project-registry directory that the new project is registered.
    file = Path(environ['OCPI_ROOT_DIR'], 'project-registry', 'test.test_project')
    test.verify(file.exists(), 'Project is Registered')
    test.endSection()