from PyQt5.QtCore import Qt

import OpenCPI_GUI

class TestHDLPlatform:
    def test_platform_name(self, qtbot):
        window = OpenCPI_GUI.NewHdlPlatformDialog()
        window.show()
        qtbot.addWidget(window)

        window.Platform_Name_lineEdit.clear()
        qtbot.keyClicks(window.Platform_Name_lineEdit, "dummy_platform")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Platform_Name_lineEdit.text() == "dummy_platform"


    def test_platform_project(self, qtbot):
        window = OpenCPI_GUI.NewHdlPlatformDialog()
        window.show()
        qtbot.addWidget(window)

        window.Platform_Name_lineEdit.clear()
        qtbot.keyClicks(window.Platform_Name_lineEdit, "dummy_platform")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Platform_Name_lineEdit.text() == "dummy_platform"
        assert window.Project_comboBox.currentText() == "dummy_proj"


    def test_platform_part_num(self, qtbot):
        window = OpenCPI_GUI.NewHdlPlatformDialog()
        window.show()
        qtbot.addWidget(window)

        window.Platform_Name_lineEdit.clear()
        qtbot.keyClicks(window.Platform_Name_lineEdit, "dummy_platform")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Part_Number_lineEdit, "dummy_part_num")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Platform_Name_lineEdit.text() == "dummy_platform"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Part_Number_lineEdit.text() == "dummy_part_num"


    def test_platform_frequency(self, qtbot):
        window = OpenCPI_GUI.NewHdlPlatformDialog()
        window.show()
        qtbot.addWidget(window)

        window.Platform_Name_lineEdit.clear()
        qtbot.keyClicks(window.Platform_Name_lineEdit, "dummy_platform")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Part_Number_lineEdit, "dummy_part_num")
        qtbot.keyClicks(window.Time_Server_Freq_lineEdit, "dummy_freq")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Platform_Name_lineEdit.text() == "dummy_platform"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Part_Number_lineEdit.text() == "dummy_part_num"
        assert window.Time_Server_Freq_lineEdit.text() == "dummy_freq"