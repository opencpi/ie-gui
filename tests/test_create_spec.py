from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestCompSpecDialog:
    def test_app_name(self, qtbot):
        window = OpenCPI_GUI.NewComponentDialog()
        window.show()
        qtbot.addWidget(window)

        window.Component_Name_lineEdit.clear()
        qtbot.keyClicks(window.Component_Name_lineEdit, "dummy_spec")
        qtbot.keyClicks(window.component_project, "dummy_proj")
        qtbot.keyClicks(window.component_library, "dummy_lib")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Component_Name_lineEdit.text() == "dummy_spec"
        assert window.Component_Project_comboBox.currentText() == "dummy_proj"
        assert window.Component_Library_comboBox.currentText() == "dummy_lib"
