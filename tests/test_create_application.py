from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestApplicationDialog:
    def test_app_name(self, qtbot):
        window = OpenCPI_GUI.NewApplicationDialog()
        window.show()
        qtbot.addWidget(window)

        window.Application_Name_lineEdit.clear()
        qtbot.keyClicks(window.Application_Name_lineEdit, "dummy_app")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Application_Name_lineEdit.text() == "dummy_app"

    def test_app_project(self, qtbot):
        window = OpenCPI_GUI.NewApplicationDialog()
        window.show()
        qtbot.addWidget(window)

        window.Application_Name_lineEdit.clear()
        qtbot.keyClicks(window.Application_Name_lineEdit, "dummy_app")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Application_Name_lineEdit.text() == "dummy_app"
        assert window.Project_comboBox.currentText() == "dummy_proj"
