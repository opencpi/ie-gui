from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestProjectDialog:
    def test_project_name(self, qtbot):
        window = OpenCPI_GUI.NewProjectDialog()
        window.show()
        qtbot.addWidget(window)

        window.Project_Name_lineEdit.clear()
        qtbot.keyClicks(window.Project_Name_lineEdit, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Project_Name_lineEdit.text() == "dummy_proj"

    def test_package_prefix(self, qtbot):
        window = OpenCPI_GUI.NewProjectDialog()
        window.show()
        qtbot.addWidget(window)

        window.Project_Name_lineEdit.clear()
        qtbot.keyClicks(window.Project_Name_lineEdit, "dummy_proj")
        qtbot.keyClicks(window.Package_Prefix_lineEdit, "loc")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Project_Name_lineEdit.text() == "dummy_proj"
        assert window.Package_Prefix_lineEdit.text() == "loc"

    def test_package_name(self, qtbot):
        window = OpenCPI_GUI.NewProjectDialog()
        window.show()
        qtbot.addWidget(window)

        window.Project_Name_lineEdit.clear()
        qtbot.keyClicks(window.Project_Name_lineEdit, "dummy_proj")
        qtbot.keyClicks(window.Package_Prefix_lineEdit, "loc")
        qtbot.keyClicks(window.Package_Name_lineEdit, "dummy_package")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Project_Name_lineEdit.text() == "dummy_proj"
        assert window.Package_Prefix_lineEdit.text() == "loc"
        assert window.Package_Name_lineEdit.text() == "dummy_package"