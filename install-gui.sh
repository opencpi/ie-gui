#!/bin/bash 
guiDurr=$(dirname "$(readlink -f "$0")")
if [ -d "$OCPI_ROOT_DIR/exports/$OCPI_TOOL_PLATFORM/bin" ]; then
    cd $OCPI_ROOT_DIR/exports/$OCPI_TOOL_PLATFORM/bin && ln -f -s $guiDurr/OpenCPI_GUI.py ocpigui
else
    echo "Error: Must have OpenCPI installed and sourced before running this script!"
    exit 1
fi