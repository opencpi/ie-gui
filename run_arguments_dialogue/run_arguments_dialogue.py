# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './run_arguments_dialogue.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Run_Application_Arguments(object):
    def setupUi(self, Run_Application_Arguments):
        Run_Application_Arguments.setObjectName("Run_Application_Arguments")
        Run_Application_Arguments.resize(312, 166)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Run_Application_Arguments)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Run_Application_Arguments)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.Befor_Arguments_lineEdit = QtWidgets.QLineEdit(Run_Application_Arguments)
        self.Befor_Arguments_lineEdit.setClearButtonEnabled(False)
        self.Befor_Arguments_lineEdit.setObjectName("Befor_Arguments_lineEdit")
        self.horizontalLayout.addWidget(self.Befor_Arguments_lineEdit)
        self.formLayout.setLayout(0, QtWidgets.QFormLayout.LabelRole, self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(Run_Application_Arguments)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.Afte_Arguments_lineEdit = QtWidgets.QLineEdit(Run_Application_Arguments)
        self.Afte_Arguments_lineEdit.setText("")
        self.Afte_Arguments_lineEdit.setPlaceholderText("")
        self.Afte_Arguments_lineEdit.setClearButtonEnabled(False)
        self.Afte_Arguments_lineEdit.setObjectName("Afte_Arguments_lineEdit")
        self.horizontalLayout_2.addWidget(self.Afte_Arguments_lineEdit)
        self.formLayout.setLayout(1, QtWidgets.QFormLayout.LabelRole, self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(Run_Application_Arguments)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.Run_Arguments_lineEdit = QtWidgets.QLineEdit(Run_Application_Arguments)
        self.Run_Arguments_lineEdit.setText("")
        self.Run_Arguments_lineEdit.setPlaceholderText("")
        self.Run_Arguments_lineEdit.setClearButtonEnabled(False)
        self.Run_Arguments_lineEdit.setObjectName("Run_Arguments_lineEdit")
        self.horizontalLayout_3.addWidget(self.Run_Arguments_lineEdit)
        self.formLayout.setLayout(2, QtWidgets.QFormLayout.LabelRole, self.horizontalLayout_3)
        self.verticalLayout_2.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(Run_Application_Arguments)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout_2.addWidget(self.buttonBox)

        self.retranslateUi(Run_Application_Arguments)
        self.buttonBox.accepted.connect(Run_Application_Arguments.accept)
        self.buttonBox.rejected.connect(Run_Application_Arguments.reject)
        QtCore.QMetaObject.connectSlotsByName(Run_Application_Arguments)

    def retranslateUi(self, Run_Application_Arguments):
        _translate = QtCore.QCoreApplication.translate
        Run_Application_Arguments.setWindowTitle(_translate("Run_Application_Arguments", "Run Application Arguments"))
        self.label.setText(_translate("Run_Application_Arguments", "Before Argument"))
        self.label_2.setText(_translate("Run_Application_Arguments", "After Argument"))
        self.label_3.setText(_translate("Run_Application_Arguments", "Run Arguments"))

