# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './ocpi_source/opencpi_location.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Ocpi_Setup_Configuration(object):
    def setupUi(self, Ocpi_Setup_Configuration):
        Ocpi_Setup_Configuration.setObjectName("Ocpi_Setup_Configuration")
        Ocpi_Setup_Configuration.resize(400, 241)
        self.verticalLayout = QtWidgets.QVBoxLayout(Ocpi_Setup_Configuration)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(Ocpi_Setup_Configuration)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.label = QtWidgets.QLabel(Ocpi_Setup_Configuration)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.Ocpi_Path_lineEdit = QtWidgets.QLineEdit(Ocpi_Setup_Configuration)
        self.Ocpi_Path_lineEdit.setObjectName("Ocpi_Path_lineEdit")
        self.verticalLayout.addWidget(self.Ocpi_Path_lineEdit)
        self.toolButton = QtWidgets.QToolButton(Ocpi_Setup_Configuration)
        self.toolButton.setObjectName("toolButton")
        self.verticalLayout.addWidget(self.toolButton)
        self.buttonBox = QtWidgets.QDialogButtonBox(Ocpi_Setup_Configuration)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Ocpi_Setup_Configuration)
        self.buttonBox.accepted.connect(Ocpi_Setup_Configuration.accept)
        self.buttonBox.rejected.connect(Ocpi_Setup_Configuration.reject)
        QtCore.QMetaObject.connectSlotsByName(Ocpi_Setup_Configuration)

    def retranslateUi(self, Ocpi_Setup_Configuration):
        _translate = QtCore.QCoreApplication.translate
        Ocpi_Setup_Configuration.setWindowTitle(_translate("Ocpi_Setup_Configuration", "OpenCPI Source Setup"))
        self.label_3.setText(_translate("Ocpi_Setup_Configuration", "To properly execute OpenCPI commands, you can set the path for your OpenCPI installation directory."))
        self.label.setText(_translate("Ocpi_Setup_Configuration", "OpenCPI directory"))
        self.toolButton.setText(_translate("Ocpi_Setup_Configuration", "..."))

