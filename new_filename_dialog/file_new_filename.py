# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_filename_dialog/create_new_filename.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_Filename_Dialog(object):
    def setupUi(self, New_Filename_Dialog):
        New_Filename_Dialog.setObjectName("New_Filename_Dialog")
        New_Filename_Dialog.resize(400, 112)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_Filename_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(New_Filename_Dialog)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_2 = QtWidgets.QLabel(New_Filename_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.New_Filename_lineEdit = QtWidgets.QLineEdit(New_Filename_Dialog)
        self.New_Filename_lineEdit.setObjectName("New_Filename_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.New_Filename_lineEdit)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_Filename_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_Filename_Dialog)
        self.buttonBox.accepted.connect(New_Filename_Dialog.accept)
        self.buttonBox.rejected.connect(New_Filename_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_Filename_Dialog)

    def retranslateUi(self, New_Filename_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_Filename_Dialog.setWindowTitle(_translate("New_Filename_Dialog", "New Filename"))
        self.label.setText(_translate("New_Filename_Dialog", "Enter the new name for the file."))
        self.label_2.setText(_translate("New_Filename_Dialog", "Filename"))

