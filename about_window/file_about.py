# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './about_window/about_gui.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_About_Dialog(object):
    def setupUi(self, About_Dialog):
        About_Dialog.setObjectName("About_Dialog")
        About_Dialog.resize(649, 530)
        self.verticalLayout = QtWidgets.QVBoxLayout(About_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(About_Dialog)
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap(":/newPrefix/opencpi_logo_2.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(About_Dialog)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Copyright_pushButton = QtWidgets.QPushButton(About_Dialog)
        self.Copyright_pushButton.setObjectName("Copyright_pushButton")
        self.horizontalLayout.addWidget(self.Copyright_pushButton)
        self.License_pushButton = QtWidgets.QPushButton(About_Dialog)
        self.License_pushButton.setObjectName("License_pushButton")
        self.horizontalLayout.addWidget(self.License_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(About_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(About_Dialog)
        self.buttonBox.accepted.connect(About_Dialog.accept)
        self.buttonBox.rejected.connect(About_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(About_Dialog)

    def retranslateUi(self, About_Dialog):
        _translate = QtCore.QCoreApplication.translate
        About_Dialog.setWindowTitle(_translate("About_Dialog", "Dialog"))
        self.label_2.setText(_translate("About_Dialog", "<html><head/><body><p>OpenCPI Graphical User Interface <br/>Version 1.0</p></body></html>"))
        self.Copyright_pushButton.setText(_translate("About_Dialog", "Copyright Notice"))
        self.License_pushButton.setText(_translate("About_Dialog", "License Information"))

import logo_rc
