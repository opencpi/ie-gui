# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_registry_dialog/new_registry_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_Registry_Dialog(object):
    def setupUi(self, New_Registry_Dialog):
        New_Registry_Dialog.setObjectName("New_Registry_Dialog")
        New_Registry_Dialog.resize(400, 129)
        self.gridLayout = QtWidgets.QGridLayout(New_Registry_Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(New_Registry_Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.Registry_Name_lineEdit = QtWidgets.QLineEdit(New_Registry_Dialog)
        self.Registry_Name_lineEdit.setObjectName("Registry_Name_lineEdit")
        self.gridLayout.addWidget(self.Registry_Name_lineEdit, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(New_Registry_Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.Registry_Dir_Path_lineEdit = QtWidgets.QLineEdit(New_Registry_Dialog)
        self.Registry_Dir_Path_lineEdit.setObjectName("Registry_Dir_Path_lineEdit")
        self.gridLayout.addWidget(self.Registry_Dir_Path_lineEdit, 1, 1, 1, 1)
        self.toolButton = QtWidgets.QToolButton(New_Registry_Dialog)
        self.toolButton.setObjectName("toolButton")
        self.gridLayout.addWidget(self.toolButton, 1, 2, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_Registry_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 2)

        self.retranslateUi(New_Registry_Dialog)
        self.buttonBox.accepted.connect(New_Registry_Dialog.accept)
        self.buttonBox.rejected.connect(New_Registry_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_Registry_Dialog)

    def retranslateUi(self, New_Registry_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_Registry_Dialog.setWindowTitle(_translate("New_Registry_Dialog", "New Registry"))
        self.label.setText(_translate("New_Registry_Dialog", "Registry Name"))
        self.label_2.setText(_translate("New_Registry_Dialog", "Directory Location"))
        self.toolButton.setText(_translate("New_Registry_Dialog", "..."))

