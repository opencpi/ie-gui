# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_application_dialog/new_application_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_New_Application_Dialog(object):
    def setupUi(self, New_Application_Dialog):
        New_Application_Dialog.setObjectName("New_Application_Dialog")
        New_Application_Dialog.resize(445, 148)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_Application_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(New_Application_Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.Application_Name_lineEdit = QtWidgets.QLineEdit(New_Application_Dialog)
        self.Application_Name_lineEdit.setObjectName("Application_Name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.Application_Name_lineEdit)
        self.label_2 = QtWidgets.QLabel(New_Application_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Project_comboBox = QtWidgets.QComboBox(New_Application_Dialog)
        self.Project_comboBox.setObjectName("Project_comboBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Project_comboBox)
        self.verticalLayout.addLayout(self.formLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.Default_radioButton = QtWidgets.QRadioButton(New_Application_Dialog)
        self.Default_radioButton.setChecked(True)
        self.Default_radioButton.setObjectName("Default_radioButton")
        self.Language_buttonGroup = QtWidgets.QButtonGroup(New_Application_Dialog)
        self.Language_buttonGroup.setObjectName("Language_buttonGroup")
        self.Language_buttonGroup.addButton(self.Default_radioButton)
        self.horizontalLayout_2.addWidget(self.Default_radioButton)
        self.UCX_radioButton = QtWidgets.QRadioButton(New_Application_Dialog)
        self.UCX_radioButton.setObjectName("UCX_radioButton")
        self.Language_buttonGroup.addButton(self.UCX_radioButton)
        self.horizontalLayout_2.addWidget(self.UCX_radioButton)
        self.LCx_radioButton = QtWidgets.QRadioButton(New_Application_Dialog)
        self.LCx_radioButton.setObjectName("LCx_radioButton")
        self.Language_buttonGroup.addButton(self.LCx_radioButton)
        self.horizontalLayout_2.addWidget(self.LCx_radioButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_Application_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_Application_Dialog)
        self.buttonBox.accepted.connect(New_Application_Dialog.accept)
        self.buttonBox.rejected.connect(New_Application_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_Application_Dialog)

    def retranslateUi(self, New_Application_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_Application_Dialog.setWindowTitle(_translate("New_Application_Dialog", "New Application"))
        self.label.setText(_translate("New_Application_Dialog", "Application Name"))
        self.label_2.setText(_translate("New_Application_Dialog", "Associated Project"))
        self.Default_radioButton.setText(_translate("New_Application_Dialog", "ACI"))
        self.UCX_radioButton.setText(_translate("New_Application_Dialog", "XML Only"))
        self.LCx_radioButton.setText(_translate("New_Application_Dialog", "XML in a Sub-directory"))
