from pathlib import Path
from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMainWindow, QAction, QFileDialog, QMessageBox, QLabel
if TYPE_CHECKING:
    from PyQt5.QtWidgets import QMdiSubWindow, QGraphicsScene

from node_editor import NodeEditorWidget
from ui.node_graph_ui import Ui_MainWindow

class MainWindow(QMainWindow, Ui_MainWindow):
    """The NodeGraph's main window and menu bar."""
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self._init_actions()
        self._init_menu()
        self._init_canvas()
        self._warning_dialog = QMessageBox(self)
        self._warning_dialog.setIcon(QMessageBox.Warning)
        self.show()
        self._splash_message_visible = True

    def _init_actions(self) -> None:
        """Initializes QActions for the main menu bar."""
        self._action_new_app = QAction('&New Application', self, shortcut='Ctrl+N', 
            statusTip="Create new opencpi application", triggered=self.new_app) #type: ignore
        # self.actNewAssemb = QAction('&New Assembly', self, shortcut='Ctrl+N', 
        #     statusTip="Create new graph", triggered=partial(self.onFileNew, 'assembly'))
        self._action_open = QAction('&Open', self, shortcut='Ctrl+O', statusTip="Open file", 
            triggered=self.open) #type: ignore
        self._action_save = QAction('&Save', self, shortcut='Ctrl+S', statusTip="Save file", 
            triggered=self.save) #type: ignore
        self._action_save.setDisabled(True)
        self._action_export = QAction('&Export', self, shortcut='Ctrl+E', 
            statusTip="Export Application", triggered=self.export) #type: ignore
        self._action_export.setDisabled(True)
        self._action_import = QAction('&Import', self, shortcut='Ctrl+I', 
            statusTip="Import Application", triggered=self.import_) #type: ignore
        self._action_save_as = QAction('Save &As...', self, shortcut='Ctrl+Shift+S', 
            statusTip="Save file as...", triggered=self.save_as) #type: ignore
        self._action_save_as.setDisabled(True)
        self.mdiArea.subWindowActivated.connect(self.set_menus)
        # self.actExit = QAction('E&xit', self, shortcut='Ctrl+Q', statusTip="Exit application", 
        #     triggered=self.close)
        # self.actUndo = QAction('&Undo', self, shortcut='Ctrl+Z', statusTip="Undo last operation", 
        #     triggered=self.onEditUndo)
        # self.actRedo = QAction('&Redo', self, shortcut='Ctrl+Shift+Z', 
        #     statusTip="Redo last operation", triggered=self.onEditRedo)

    def _init_menu(self) -> None:

        """Initializes the main QMenuBar."""
        self._menu_file = self.menuBar.addMenu('&File')
        new_menu = self._menu_file.addMenu('&New')
        new_menu.addAction(self._action_new_app)
        self._menu_file.addAction(self._action_open)
        self._menu_file.addAction(self._action_save)
        self._menu_file.addAction(self._action_save_as)
        self._menu_file.addAction(self._action_export)
        self._menu_file.addAction(self._action_import)
        self._menu_edit = self.menuBar.addMenu('&Edit')
        self._menu_edit.setEnabled(False)

    def _init_canvas(self) -> None:
        """Creates splash window tab, and remove the close option.
        
        Called when the main window class is initialized. Creates the splash window,
        which will automatically be closed when a application is created, opened, or imported.
        The splash window contains text to inform the user how to create, open, or import an app.

        Returns:
            None
        """
        self._splash_message = QLabel(self)
        self._splash_message.setText("No Application Open!\n"
                                    "-----------------------------------\n"
                                    "New Application: File -> New -> New application\n"
                                    "Open Application: File -> Open -> Browse to a .json file\n"
                                    "Import Application: File -> Import -> Browse to a .xml file")
        self._splash_message.setAlignment(Qt.AlignCenter)
        # Set Tabs as non-closable for the splash screen
        self.mdiArea.setTabsClosable(False)
        self._splash_subwindow = self.mdiArea.addSubWindow(self._splash_message)
        self._splash_subwindow.setWindowTitle(" ")
        self._splash_subwindow.showMaximized()

    def _clean_init_canvas(self) -> None:
        """Closes splash subwindow and makes tabs closeable for applications
        
        Called by new_app(), import(), and open() such that the splash window
        is closed when an app is created, opened, or imported.

        Returns:
            None
        """
        if self._splash_message_visible:
            self._splash_message_visible = False
            # Close splash subwindow
            self._splash_subwindow.close()
            # Set Tabs as closable for applications
            self.mdiArea.setTabsClosable(True)

    def new_app(self) -> None:
        """Creates a new subwindow in the main window for a new application."""
        self._clean_init_canvas() 
        
        node_editor_widget = NodeEditorWidget(self)
        self.mdiArea.addSubWindow(node_editor_widget)
        node_editor_widget.show()

    def save(self) -> bool:
        """Saves the currently active application subwindow.
        
        Uses the node_editor_widget's path attribute to save the application 
        to.

        Returns:
            Whether saving was successful.
        """
        node_editor_widget = self.get_active_widget()
        return node_editor_widget.save()

    def save_as(self) -> bool:
        """Saves the currently active application subwindow.
        
        Calls QFileDialog.getSaveFileName() to allow the user to set the Path
        to save the application to. Ensures ".json" suffix in Path.

        Returns:
            Whether saving was successful.
        """
        node_editor_widget = self.get_active_widget()
        cwd = str(Path.cwd())
        out_path = Path(QFileDialog.getSaveFileName(self, 'Save to file', cwd, '*')[0])
        if out_path == '': return False
        return node_editor_widget.save_as(out_path.with_suffix('.json'))

    def export(self) -> bool:
        """Exports the currently active application subwindow.
        
        Calls QFileDialog.getSaveFileName() to allow the user to set the Path
        to export the application to. Ensures ".xml" suffix in Path.

        Returns:
            Whether exporting was successful.
        """
        node_editor_widget = self.get_active_widget()
        cwd = str(Path.cwd())
        out_path = Path(QFileDialog.getSaveFileName(self, 'Save to file', cwd, '*')[0])
        if out_path == '': return False
        return node_editor_widget.export(out_path.with_suffix('.xml'))

    def import_(self) -> bool:
        """Imports an application and creates a new subwindow for it.
        
        Calls QFileDialog.getSaveFileName() to allow the user to set the Path
        to import the application from.

        Returns:
            Whether importing was successful.
        """
        self._clean_init_canvas() 
        
        cwd = str(Path.cwd())
        in_path = Path(QFileDialog.getOpenFileName(self, 'File to import', cwd, '*')[0])
        if not in_path.is_file(): return False
        try:
            node_editor_widget = NodeEditorWidget.import_(in_path, window=self)
            if node_editor_widget:
                self.mdiArea.addSubWindow(node_editor_widget)
                node_editor_widget.show()
                return True
        except Exception as e:
            self._warning_dialog.setWindowTitle("Import File Error")
            self._warning_dialog.setText(f'Unable to import file: <br> "{in_path}"')
            self._warning_dialog.setDetailedText(str(e))
            self._warning_dialog.show()
        return False

    def open(self) -> bool:
        """Opens an application and creates a new subwindow for it.
        
        Calls QFileDialog.getSaveFileName() to allow the user to set the Path
        to open the application from.

        Returns:
            Whether opening was successful.
        """
        self._clean_init_canvas() 
        
        cwd = str(Path.cwd())
        in_path = Path(QFileDialog.getOpenFileName(self, 'Save to file', cwd, '*')[0])
        if not in_path.is_file(): return False
        try:
            node_editor_widget = NodeEditorWidget.open(in_path, window=self)
            self.mdiArea.addSubWindow(node_editor_widget)
            node_editor_widget.show()
            return True
        except Exception as e:
            self._warning_dialog.setWindowTitle("Open File Error")
            self._warning_dialog.setText(f'Unable to open file: <br> "{in_path}"')
            self._warning_dialog.setDetailedText(str(e))
            self._warning_dialog.show()
        return False

    def paste(self) -> None:
        """Paste QGraphicItems that are currently in the Scene's clipboard.
        
        Calls Scene's paste() method to paste its clipboard's items.
        """
        scene = self.get_active_widget().scene
        view = self.get_active_widget().view
        if view.is_entered:
            pos = view.get_cursor_pos()
        else:
            pos = None
        scene.paste(pos=pos)

    def set_menus(self) -> None:
        """Set the main menubar menu items.
        
        Either enables or disables the main menubar menu items depending
        on whether there's an active subwindow.
        """
        subwindow = self.get_active_subwindow()
        self._menu_edit.clear()
        if not subwindow:
        # No active subwindow; disable saving, exporting, and editing
            self._action_save.setDisabled(True)
            self._action_save_as.setDisabled(True)
            self._action_export.setDisabled(True)
            self._menu_edit.setDisabled(True)
        else:
        # Active subwindow; enable saving, exporting, and editing
            self._menu_edit.setEnabled(True)
            self._action_export.setDisabled(False)
            self._action_save_as.setDisabled(False)
            node_editor_widget = self.get_active_widget()
            view = node_editor_widget.view
            scene = node_editor_widget.scene
            if node_editor_widget.path:
                self._action_save.setDisabled(False)
            else:
                self._action_save.setDisabled(True)
            action_paste = QAction('&Paste', self, shortcut='Ctrl+V', 
                statusTip="Paste from clipboard", triggered=self.paste, 
                enabled=bool(scene._clipboard)) #type: ignore
            scene.clipboard_signal.connect( # type: ignore
                lambda: action_paste.setEnabled(bool(scene._clipboard)))
            action_edit = QAction('Edit Properties', self, shortcut='Ctrl+P',
                statusTip="Open properties in Properties Panel",
                triggered=lambda: view.set_properties_widget(),
                enabled=len(scene.selectedItems()) == 1) # type: ignore
            scene.selectionChanged.connect( # type: ignore
                lambda: action_edit.setEnabled(self.is_single_item_selected(scene)))
            self._menu_edit.addActions([
                view.action_cut,
                view.action_copy,
                action_paste,
                view.action_delete,
                action_edit
            ])

    def is_single_item_selected(self, scene: 'QGraphicsScene') -> bool:
        """Returns whether exactly one item was selected in a scene."""
        try:
            return len(scene.selectedItems()) == 1
        except RuntimeError:
            return False

    def get_active_subwindow(self) -> 'QMdiSubWindow':
        """Return the currently active QMdiSubdindow."""
        return self.mdiArea.activeSubWindow()

    def get_active_widget(self) -> NodeEditorWidget:
        """Return the currently active NodeEditorWidget."""
        return self.get_active_subwindow().widget() #type: ignore
