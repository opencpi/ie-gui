# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/NodeGraph.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1080, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.mdiArea = QtWidgets.QMdiArea(self.centralwidget)
        self.mdiArea.setViewMode(QtWidgets.QMdiArea.TabbedView)
        self.mdiArea.setTabsClosable(True)
        self.mdiArea.setTabsMovable(True)
        self.mdiArea.setObjectName("mdiArea")
        self.node_editor_window = QtWidgets.QWidget()
        self.node_editor_window.setObjectName("node_editor_window")
        self.horizontalLayout.addWidget(self.mdiArea)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.node_dock = QtWidgets.QDockWidget(MainWindow)
        self.node_dock.setObjectName("node_dock")
        self.dockWidgetContents_2 = QtWidgets.QWidget()
        self.dockWidgetContents_2.setObjectName("dockWidgetContents_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.dockWidgetContents_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.node_search_label = QtWidgets.QLabel(self.dockWidgetContents_2)
        self.node_search_label.setObjectName("node_search_label")
        self.verticalLayout.addWidget(self.node_search_label)
        self.node_search_field = QtWidgets.QLineEdit(self.dockWidgetContents_2)
        self.node_search_field.setObjectName("node_search_field")
        self.verticalLayout.addWidget(self.node_search_field)
        self.node_tree = NodeTreeWidget(self.dockWidgetContents_2)
        self.node_tree.setObjectName("node_tree")
        self.verticalLayout.addWidget(self.node_tree)
        self.node_dock.setWidget(self.dockWidgetContents_2)
        MainWindow.addDockWidget(QtCore.Qt.DockWidgetArea(2), self.node_dock)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 1080, 31))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)

        self.retranslateUi(MainWindow)
        self.node_search_field.textChanged['QString'].connect(self.node_tree.filter_items)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.node_editor_window.setWindowTitle(_translate("MainWindow", "Subwindow"))
        self.node_search_label.setText(_translate("MainWindow", "Search:"))
        self.node_tree.headerItem().setText(0, _translate("MainWindow", "Components"))
from node_tree_widget import NodeTreeWidget
