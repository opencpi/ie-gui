from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import List

from PyQt5.QtWidgets import QWidget, QVBoxLayout

class MetaClass(type(QWidget), ABCMeta):
    pass

class BasePropertiesWidget(QWidget, metaclass=MetaClass):
    """Container widget for BaseGraphicsItem's properties widgets.
    
    This abstract base class is not intended to be instantiated. It is intended
    to be inherited by a concrete subclass.
    """
    def __init__(self) -> None:
        super().__init__()
        layout = QVBoxLayout()
        self.setLayout(layout)
        self._init_widgets()

    @abstractmethod
    def _init_widgets(self) -> None:
        """Initializes the QWidgets for the BaseGraphicsItem's properties.
        
        This method must be implemented by classes inheriting this abstract
        base class.
        """
        pass
    
    def addWidgets(self, widgets: 'List[QWidget]') -> None:
        """Add a list of QWidgets to this BasePropertiesWidget.
        
        Args:
            widgets: A List of QWidgets to add to this BasePropertiesWidget.
        """
        list(map(self.layout().addWidget, widgets))
