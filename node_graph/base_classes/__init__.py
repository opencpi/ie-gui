"""Contains abstract base classes used in other modules.

Classes in this module are not intended to be instantiated themselves. They are
intended to be inherited by child classes.

BaseGraphicsItem: Abstract base class that extends the QGraphicsItem class.
    Used for displaying graphic items in the node editor scene.
BasePropertiesWidget: Abstract base class that extends the QWidget class.
    Used as a constainer for widgets related to a model's properties.
BaseModel: Abstract base class for classes representing data models.
"""

from .base_graphics_item import BaseGraphicsItem
from .base_properties_widget import BasePropertiesWidget
from .model import BaseModel
