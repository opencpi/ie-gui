from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Optional, Dict

from PyQt5.QtCore import QRectF
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsSceneHoverEvent
if TYPE_CHECKING:
    from .base_properties_widget import BasePropertiesWidget
    from .model import BaseModel

class MetaClass(type(QGraphicsItem), ABCMeta):
    """Metaclass to enable creating an abstract base QGraphicsItem class."""
    pass

class BaseGraphicsItem(QGraphicsItem, metaclass=MetaClass):
    """Abstract base class for QGraphicsItems to add to a NodeEditor.

    This abstract base class is not intended to be instantiated. It is intended
    to be inherited by a concrete subclass.
    """
    _flags = (QGraphicsItem.ItemIsSelectable)
    def __init__(self, parent: 'Optional[QGraphicsItem]' = ...) -> None:
        if parent is ...:
            parent = None
        super().__init__(parent)
        self.is_highlighted = False
        self.setAcceptHoverEvents(True)
        self.setFlags(self.__class__._flags)
        self._model: 'BaseModel'

    @property
    @abstractmethod
    def properties_widget(self) -> 'BasePropertiesWidget':
        """QWidget containing the BaseGraphicsItem's properties.
        
        This property must be implemented by classes inheriting this abstract
        base class.
        """
        pass

    def save(self, property: 'Dict[str, str]', value: str, attribute: str = 'value') -> None:
        """Set attribute in property dictionary to value.

        Calls the save() method of the BaseGraphicsItem's model.

        Args:
            property: Property whose attribute to be set.
            value: Value to set attribute to.
            attribute: Attribute to be set to value.
        """
        if self._model:
            self._model.save(property, value, attribute=attribute)

    def hoverLeaveEvent(self, event: QGraphicsSceneHoverEvent) -> None:
        """Handles the event of mouse moving off of a BaseGraphicsItem.
        
        Overrides super method. Calls super method, then sets is_highlighted
        attribute to False.
        """
        super().hoverLeaveEvent(event)
        self.is_highlighted = False
        self.update()
        event.accept()
        
    def hoverEnterEvent(self, event: QGraphicsSceneHoverEvent) -> None:
        """Handles the event of mouse moving over a BaseGraphicsItem.
        
        Overrides super method. Calls super method, then sets is_highlighted
        attribute to True.
        """
        super().hoverEnterEvent(event)
        self.is_highlighted = True
        self.update()
        event.accept()
