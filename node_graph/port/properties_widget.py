from PyQt5.QtWidgets import QLabel

from base_classes import BasePropertiesWidget

class PortPropertiesWidget(BasePropertiesWidget):
    """Container widget for widgets related to a Port's properties."""
    def __init__(self, port) -> None:
        self._port = port
        super().__init__()
    
    def _init_widgets(self) -> None:
        """Initializes the QWidgets for the Ports's properties."""
        widgets = []
        name_label = QLabel(self._port.name)
        widgets.append(name_label)
        for key,val in self._port._model.properties.items():
            widgets.append(QLabel(f'{key}:\n{val}'))
        self.addWidgets(widgets)
        