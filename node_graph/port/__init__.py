"""Contains classes that represent an OpenCPI component port.

The Port class is typically instantiated automatically when creating a Node,
but can be manually created.

Port: Graphical representation of a component port.

Typical usage:
    Create a Port:
        port = (port_name, properties_dict, node)
    Create a Port from a dictionary representation:
        port = port.from_dict(dictionary)
    Convert a Port to a dictionary representation:
        dictionary = port.to_dict()
"""

from .port import Port
