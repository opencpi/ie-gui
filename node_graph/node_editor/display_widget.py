from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QWidget, QScrollArea, QLayout, QVBoxLayout

if TYPE_CHECKING:
    from base_classes import BasePropertiesWidget

class DisplayWidget(QWidget):
    def __init__(self) -> None:
        """QWidget for displaying a BasePropertiesWidget.
        
        Attributes:
            properties_widget: The BasePropertiesWidget that is currently
                being displayed in the DisplayWidget.
        """
        super().__init__()
        self._scroll_area = QScrollArea()
        self._scroll_area.setWidgetResizable(True)
        layout = QVBoxLayout()
        layout.addWidget(self._scroll_area)
        self.setLayout(layout)
        self._content = QWidget(self._scroll_area)
        self._content_layout = QVBoxLayout(self._content)
        self._content_layout.setSizeConstraint(QLayout.SetFixedSize)
        self._content.setLayout(self._content_layout)
        self._scroll_area.setWidget(self._content)
        self.properties_widget = None
        self.setHidden(True)

    def set_properties_widget(self, properties_widget: 'BasePropertiesWidget') -> bool:
        """Sets the BasePropertiesWidget to display.
        
        Returns:
            Whether the DisplayWidget's layout contains any widgets.
        """
        if properties_widget in [None, self.properties_widget]:
            return False
        self.unset_properties_widget()
        self.properties_widget = properties_widget
        self._content_layout.addWidget(self.properties_widget)
        self.setHidden(False)
        return bool(self.layout().count())

    def unset_properties_widget(self) -> bool:
        """Unsets the BasePropertiesWidget currently being displayed.
        
        Returns:
            Whether the DisplayWidget's layout is empty.
        """
        if not self.properties_widget:
            return False
        self._content_layout.removeWidget(self.properties_widget)
        self.properties_widget.setParent(None)
        self.properties_widget = None
        self.hide()
        return not bool(self.layout().count())
        