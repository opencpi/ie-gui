"""Contains classes related to the node editor widget.

NodeEditorWidget: QWidget which acts as the actual node graph for creating 
    connections between node ports.

Typical usage:
    Create a new NodeEditorWidget and add it to a MainWindow:
        window = MainWindow()
        node_editor_widget = NodeEditorWidget(window)
        window.mdiArea.addSubWindow(node_editor_widget)
        node_editor_widget.show()
    Save a NodeEditorWidget application:
        node_editor_widget.save_as(out_path)
    Create a NodeEditorWidget from a saved application:
        node_editor_widget = NodeEditorWidget.open(in_path, window)
"""

from .node_editor_widget import NodeEditorWidget