import json
import math
from pathlib import Path
import re
from statistics import mean
from typing import TYPE_CHECKING
from xml.dom.minidom import Document
import xml.etree.ElementTree as ET
if TYPE_CHECKING:
    import xml.dom.minidom as MD
    from typing import List, Union, Iterable, Dict, Any, Optional

from PyQt5.QtCore import QLine, QPointF, pyqtSignal
from PyQt5.QtGui import QColor, QPen, QFont
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsItem
if TYPE_CHECKING:
    from PyQt5.QtCore import QRectF
    from PyQt5.QtGui import QPainter

from connection import Connection
from node import Node
if TYPE_CHECKING:
    from base_classes import BaseGraphicsItem
    from connection import Line
    from .node_editor_widget import NodeEditorWidget

class Scene(QGraphicsScene):
    """The QGraphicsScene for the NodeEditorWidget
    
    Attributes:
        nodes: A list of Nodes contained within the Scene.
        nodes_dict: A dictionary of Nodes as values and the name of the Nodes
            as keys.
        connections: A list of Connections contained within the Scene.
    """
    clipboard_signal = pyqtSignal(int)
    def __init__(
        self, 
        node_editor_widget: 'Optional[NodeEditorWidget]' = None, 
        nodes: 'Optional[List[Node]]' = None, 
        connections: 'Optional[List[Connection]]' = None
    ) -> None:
        super().__init__(node_editor_widget)
        self._clipboard: 'List[QGraphicsItem]' = []
        self.nodes: 'List[Node]' = []
        self.nodes_dict: 'Dict[str, Node]' = {}
        self.connections: 'List[Line]' = []
        self._paste_offset = 0
        if nodes:
            self.add_nodes(nodes)
        if connections:
            self.add_connections(connections)
        self._grid_size = 20
        self._grid_squares = 5
        self._init_assets()
        self.setBackgroundBrush(self._color_background)
        width = 4600
        height = 4600
        self.setSceneRect(-width // 2, -height // 2, width, height)
        self.selectionChanged.connect(self.on_selection_change)

    def _init_assets(self) -> None:
        """Initiate colors for the Scene."""
        self._color_background = QColor("#939799")
        self._color_light = QColor("#616161")
        self._color_dark = QColor("#616161")
        self._color_state = QColor("#ccc")
        self._pen_light = QPen(self._color_light)
        self._pen_light.setWidth(1)
        self._pen_dark = QPen(self._color_dark)
        self._pen_dark.setWidth(2)
        self._pen_state = QPen(self._color_state)
        self._font_state = QFont("Ubuntu", 16)

    def add_node(self, node: Node) -> None:
        """Adds a Node to the Scene.

        Args:
            node: The Node to add to the Scene.
        
        Calls addItem() to add the Node to the Scene. Handles Node instance
        name conflicts. Updates the nodes and nodes_dict attributes with the
        new Node.
        """
        if not node:
            return
        self.addItem(node)
        self.nodes.append(node)
        search = re.search(r'(.*)(_\d+)', node.name)
        if search:
        # If name ended with a '_' and at least 1 digit, chop it off.
            name = search.group(1)
        else:
            name = node.name
        if name in self.nodes_dict:
        # If the name is already associated with a Node in the Scene, add a
        # suffix of '_2', incrementing the digit as needed.
            i = 2
            while f'{name}_{i}' in self.nodes_dict:
                i+=1
            node.name = f'{name}_{i}'
        self.nodes_dict[node.name] = node

    def add_nodes(self, nodes: 'List[Node]') -> None:
        """Adds a list of Nodes to the Scene.
        
        Args:
            nodes: The list of Nodes to add to the Scene.
        """
        for node in nodes:
            self.add_node(node)

    def add_connection(self, connection: 'Line') -> None:
        """Adds a Connection or Line to the Scene.

        Args:
            connections: The Connection or Line to add to the Scene.
        
        Updates the connections attribute.
        """
        if connection:
            self.addItem(connection)
            self.connections.append(connection)
            connection.update()

    def add_connections(self, connections: 'Iterable[Line]') -> None:
        """Adds a list of Connections or Lines to the Scene.
        
        Args:
            connections: The list of Connections to add to the Scene.
        """
        for connection in connections:
            self.add_connection(connection)

    def remove_node(self, node: Node) -> None:
        """Removes a Node from the Scene.
        
        Updates the nodes and nodes_dict attribute and unsets the 
        DisplayWidget's PropertyWidget if it belongs to the Node being removed.

        Args:
            node: The Node to removed from the Scene.
        """
        if not node:
            return
        self.nodes_dict.pop(node.name)
        if node in self.nodes:
            self.nodes.remove(node)
        if node in self.items():
            self.removeItem(node)
        if node.properties_widget == self.views()[0].display_widget.properties_widget:
            self.views()[0].display_widget.unset_properties_widget()

    def remove_nodes(self, nodes: 'List[Node]') -> None:
        """Removes a list of Nodes from the Scene.
        
        Args:
            nodes: The list of Nodes to remove from the Scene.
        """
        for node in nodes:
            self.remove_node(node)

    def remove_connection(self, connection: 'Line') -> None:
        """Removes a Connection or Line from the Scene.
        
        Updates the connections attribute and unsets the  DisplayWidget's 
        PropertyWidget if it belongs to the Connection being removed.

        Args:
            connection: The Connection or Line to remove from the Scene.
        """
        if not connection:
            return
        if connection in self.connections:
            self.connections.remove(connection)
        if connection in self.items():
            self.removeItem(connection)
        if isinstance(connection, Connection):
            if connection.properties_widget == self.views()[0].display_widget.properties_widget:
                self.views()[0].display_widget.unset_properties_widget()

    def remove_connections(self, connections: 'Iterable[Line]') -> None:
        """Remove a list of Connections from the Scene.
        
        Args:
            connections: The Connections to remove from the Scene.
        """
        for connection in connections:
            self.remove_connection(connection)

    def remove_item(self, item: 'BaseGraphicsItem') -> None:
        """Removes a BaseGraphicsItems from the Scene.
        
            item: The BaseGraphicsItem to remove from the Scene.
        """
        if isinstance(item, Node):
            self.remove_node(item)
        elif isinstance(item, Connection):
            self.remove_connection(item)

    def remove_items(self, items: 'List[BaseGraphicsItem]') -> None:
        """Removes a list of BaseGraphicsItems from the Scene.
        
        Args:
            item: The list of BaseGraphicsItems to remove from the Scene.
        """
        for item in items:
            self.remove_item(item)

    @classmethod
    def open(cls, in_path: Path) -> 'Scene':
        """Creates a Scene from a json file.
        
        Args:
            in_path: The json file Path to create the Scene from.
        """
        with in_path.open('r+') as in_file:
            in_dict = json.load(in_file)
            scene = Scene.from_dict(in_dict)
        return scene

    def export(self, out_path: Path) -> None:
        """Converts the Scene to an xml file.

        Args:
            out_path: The xml file Path to save the scene to.
        """
        root = Document()
        self.to_xml(root)
        with out_path.open('w+') as out_file:
            out_file.write(root.toprettyxml(indent="\t"))

    @classmethod
    def import_(cls, in_path: Path) -> 'Optional[Scene]':
        """Creates a Scene from an xml file.
        
        Args:
            in_path: The xml file Path to create the scene from.
        """
        if not in_path.suffix == '.xml':
            raise Exception('OpenCPI application file must contain the ".xml" extension.')
        tree = ET.parse(in_path)
        root = tree.getroot()
        if root.tag.lower() != 'application':
            raise Exception('File does not appear to be an OpenCPI application.')
        if isinstance(root, ET.Element):
            scene = cls.from_xml(root)
        else:
            scene = None      
        return scene

    def save(self, out_path: Path) -> None:
        """Converts the Scene to a json file.
        
        Args:
            out_path: The json file Path to save the scene to.
        """
        with out_path.open('w+') as out_file:
            json.dump(self.to_dict(), out_file, indent=4)

    def copy(self, items: 'Union[Iterable[QGraphicsItem], QGraphicsItem]' = ...) -> bool:
        """Copies Connections and Nodes in the Scene.
        
        Copied items are added to the clipboard attribute. If the items arg is
        not prodived, will call selectedItems() method to use as the items to
        copy.

        Args:
            items: A list of QGraphicsItems to copy. If not provided, will
                default to currently selected items.

        Returns:
            Whether items were copied.
        """
        if not items or items is ...:
            items_list = self.selectedItems()
        elif not isinstance(items, list):
            items_list = [items]
        else:
            items_list = items
        if not items_list:
            return False
        nodes = [item for item in items_list if isinstance(item, Node)]
        connections = [item for item in items_list if isinstance(item, Connection)]
        self._clipboard.clear()
        node_map = {} # Used to map a connection's nodes to the copies
        for node in nodes:
            copy = node.copy()
            node_map[node] = copy
            self._clipboard.append(copy)
        for connection in connections:
            if not connection.consumer_port or not connection.producer_port:
                continue
            in_node = connection.consumer_port.node
            out_node = connection.producer_port.node
            if in_node not in node_map or out_node not in node_map:
            # Nodes not in node_map, so no copied Nodes available
                continue
            # Get the ports of the copied Nodes
            consumer_port = node_map[in_node].get_port(connection.consumer_port.name)
            producer_port = node_map[out_node].get_port(connection.producer_port.name)
            copy = connection.copy(ports=[consumer_port, producer_port])
            if not copy: 
                continue
            self._clipboard.append(copy)
        self._paste_offset = 20
        self.clipboard_signal.emit(1) # type: ignore
        return True

    def cut(self, items: 'Union[Iterable[QGraphicsItem], QGraphicsItem]' = ...) -> bool:
        """Cuts Connections and Nodes from the Scene.
        
        The copy() method is called on the items before removing them from
        the Scene. If the items arg is not prodived, will call selectedItems() 
        method to use as the items to cut.

        Args:
            items: A list of QGraphicsItems to cut. If not provided, will
                default to currently selected items.

        Returns:
            Whether items were copied.
        """
        items_list = []
        if not items or items is ...:
            items_list = self.selectedItems()
        elif not isinstance(items, list):
            items_list.append(items)
        if not items_list:
            return False
        if not self.copy(items=items_list):
            return False
        for item in items_list:
            if isinstance(item, Node):
                item.delete_connections()
                self.remove_node(item)
        self._paste_offset = 0
        return True

    def paste(self, pos: 'Optional[QPointF]' = ...) -> bool:
        """Pastes items in clipboard attribute into the Scene.
        
        Items will be pasted using the pos arg as the center point. If pos arg 
        is not set, will use the center point of the items in the clipboard.
        Calls copy() method to make new copies of pasted items.

        Args:
            pos: QPointF representing the position to paste the items to.

        Returns:
            Whether items were pasted.
        """
        if not self._clipboard:
            return False
        nodes = [item for item in self._clipboard if isinstance(item, Node)]
        connections = [item for item in self._clipboard if isinstance(item, Connection)]
        if pos and pos is not ...:
        # Set center position to passed in position
            to_point = pos
        else:
        # Set center to current center + offset
            center_point = self.get_items_center_point(nodes)
            if not center_point:
                return False
            to_point = QPointF(
                center_point.x() + self._paste_offset, 
                center_point.y() + self._paste_offset
            )
        self.move_items_center_point(nodes, to_point=to_point)
        self.add_nodes(nodes)
        connection_copies = []
        for connection in connections:
            if not connection.consumer_port or not connection.producer_port:
                continue
            in_node = connection.consumer_port.node
            out_node = connection.producer_port.node
            if all(node in self.nodes for node in [in_node, out_node]):
            # Add Connections if its nodes exist within scene or clipboard
                self.add_connection(connection)
                connection_copies.append(connection)
        self.copy(list(nodes+connection_copies)) # Create new copies of items
        self.deselect_all()
        self.select_all(items=nodes+connection_copies)
        return True

    def delete(self, items: 'Union[Iterable[QGraphicsItem], QGraphicsItem]' = ...) -> bool:
        """Deletes Connections and Nodes from the Scene.
        
        Calls the delete() method of each item in the list of items to delete.
        If items areg is not passed, will use the selectedItems() method to get
        the list of items to delete.

        Args:
            items: A list of QGraphicsItems to delete. If not provided, will
                default to currently selected items.

        Returns:
            Whether items were deleted.
        """
        items_list = []
        if not items or items is ...:
            items_list = self.selectedItems()
        elif not isinstance(items, list):
            items_list.append(items)
        if not items_list:
            return False
        for item in items_list:
            if isinstance(item, Node) or isinstance(item, Connection):
                item.delete()
        return True

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the Scene to a dictionary representation.

        Calls the to_dict() method on every Node and Connection in the Scene.
        
        Returns:
            A dictionary representation of the Scene.
        """
        out_dict = {
            'nodes': [node.to_dict() for node in self.nodes],
            'connections': [conn.to_dict() for conn in self.connections 
                            if isinstance(conn, Connection)]
        }
        return out_dict

    @classmethod
    def from_dict(cls, in_dict) -> 'Scene':
        """Creates a Scene from a dictionary.
        
        Returns:
            A Scene created from the in_dict arg.
        """
        scene = cls()
        nodes = [Node.from_dict(node) for node in in_dict['nodes']]
        scene.add_nodes(nodes)
        for connection_dict in in_dict['connections']:
            connection = Connection.from_dict(connection_dict, scene)
            if connection:
                scene.add_connection(connection)
        return scene

    def to_xml(self, root: 'Document') -> 'MD.Element':
        """Converts the Scene to an xml representation.

        Calls the to_xml() method on every Node and Connection in the Scene.
        
        Args:
            root: The minidom Document to add the Connection and the Connection
                Ports to.

        Returns:
            An xml minidom Element representation of the Connection.
        """
        app_xml = root.createElement('Application')
        root.appendChild(app_xml)
        for node in self.nodes:
            node_xml = node.to_xml(root)
            app_xml.appendChild(node_xml)
        for connection in self.connections:
            if not isinstance(connection, Connection):
                continue
            connection_xml = connection.to_xml(root)
            app_xml.appendChild(connection_xml)
        return app_xml

    @classmethod
    def from_xml(cls, element: 'ET.Element') -> 'Scene':
        """Creates a Scene from an xml minidom Element.

        Args:
            element: The xml minidom Element representation of a Scene.

        Returns:
            A Connection based on the element arg.
        """
        scene = cls()
        x_pos, y_pos, node_counter = 50, -75, 0
        package = element.attrib.get('package', None)
        for item in element:
            item.tag = item.tag.lower()

        # Dictionary to track connect property connections
        connections_dict = {}
        regx = re.compile(r'\d+$')

        # Get all instances of nodes and connetions in xml
        et_nodes = element.findall('instance')
        for et_node in et_nodes:
            node_counter += 1 

            node = Node.from_xml(et_node, package_id=package)
            scene.add_node(node)
            node.setPos(QPointF(x_pos, y_pos))
            x_pos += 150
            if (node_counter % 2) == 0:
                y_pos -= 125
            else:
                y_pos += 125

            # If connect is a component property    
            #   handled this way to protect against capital letters ex: Connect, cOnnect, etc.
            attribute_dict = dict((k.lower(), v) for k,v in et_node.attrib.items())
            if "connect" in attribute_dict:
                producer = node.name
                consumer = attribute_dict['connect']
                
                # Check for connect=<component_name><number> case, 
                # where <number> needs to be incrimented by 1 or removed if 0
                if consumer[-1].isdigit():
                    match = regx.search(consumer)
                    if match is None:
                        continue
                    
                    suffix = match.group()
                    # remove suffix of 0 for component name
                    if suffix == '0':
                        consumer = regx.sub('', consumer)
                    # add 1 to suffix value for component name
                    else:
                        consumer = regx.sub('_' + str(int(suffix)+1), consumer)

                # Check if consumer is already listed as a consumer, if so add suffix
                if consumer in connections_dict.values():
                    # duplicate_num = sum + 1 because suffix start at _2, see add_node method
                    duplicate_num = sum(str(v).startswith(consumer) for v 
                                        in connections_dict.values()) + 1
                    consumer = f'{consumer}_{duplicate_num}'
                
                # add producer and consumer to connection dictionary
                connections_dict[producer] = consumer
                
        # Create connections based on 'connect=' component property
        for prod, cons in connections_dict.items():
            # Check that both nodes exist on the scene
            if not prod in scene.nodes_dict or not cons in scene.nodes_dict:
                raise Exception(f"Error: Node not found on scene, can't connect {prod} to {cons}")

            # Check producer port count is exactly 1 and get prod_port
            if len(scene.nodes_dict[prod].producer_ports) != 1:
                raise Exception(f"Error: {prod} node must have one producing port to use "
                                f"the connect property")
            prod_port = scene.nodes_dict[prod].producer_ports[0]

            # Check consumer port count is exactly 1 and get cons_port
            if len(scene.nodes_dict[cons].consumer_ports) != 1:
                raise Exception(f"Error: {cons} node must have one producing port to use "
                                f"the connect property")
            cons_port = scene.nodes_dict[cons].consumer_ports[0]

            # Check that the connection is valid then add the connection
            if not Connection.is_valid_connection(cons_port, prod_port):
                raise Exception(f"Error: Connection between producer {prod} and consumer {cons} "
                                f"is not valid")
            scene.add_connection(Connection(cons_port, prod_port))

        # Create connections from connection instance
        connections = element.findall('connection')
        for connection in connections:
            connection = Connection.from_xml(connection, scene)
            if connection:
                scene.add_connection(connection)
        return scene

    def drawBackground(self, painter: 'QPainter', rect: 'QRectF') -> None:
        """Draws the background of the Scene.
        
        Args:
            painter: QPainter used to draw the Scene.
            rect: QRectF used to create shape of Scene.
        """
        super().drawBackground(painter, rect)
        # here we create our grid
        left = int(math.floor(rect.left()))
        right = int(math.ceil(rect.right()))
        top = int(math.floor(rect.top()))
        bottom = int(math.ceil(rect.bottom()))
        first_left = left - (left % self._grid_size)
        first_top = top - (top % self._grid_size)
        # compute all lines to be drawn
        lines_light, lines_dark = [], []
        for x in range(first_left, right, self._grid_size):
            if (x % (self._grid_size*self._grid_squares) != 0): 
                lines_light.append(QLine(x, top, x, bottom))
            else: lines_dark.append(QLine(x, top, x, bottom))
        for y in range(first_top, bottom, self._grid_size):
            if (y % (self._grid_size*self._grid_squares) != 0): 
                lines_light.append(QLine(left, y, right, y))
            else: lines_dark.append(QLine(left, y, right, y))
        # draw the lines
        painter.setPen(self._pen_light)
        painter.drawLines(*lines_light)
        painter.setPen(self._pen_dark)
        painter.drawLines(*lines_dark)

    def get_items_center_point(self, items: 'Iterable[QGraphicsItem]') -> QPointF:
        """Finds the center point of items in the items arg.
        
        Args:
            items: List of QGraphicsItems to find the center point of.

        Returns:
            QPointF representing the center point of items in the items arg.
        """
        avg_x = mean([item.x() for item in items])
        avg_y = mean([item.y() for item in items])
        return QPointF(avg_x, avg_y)

    def move_items_center_point(
        self, 
        items: 'Iterable[QGraphicsItem]', 
        to_point: QPointF, 
        from_point: QPointF = ...
    ) -> None:
        """Changes the center point of all items in the items arg.
        
        Moves every item in the items arg to new position so that the center
        point of all items is moved from the from_point arg to the to_point 
        arg. If the from_point arg is not proviced, calls 
        get_items_center_point() method to get the center point of the items
        in the items arg.

        Args:
            items: List of QGraphicsItems to move.
            to_point: The QPointF representing the center point to move to.
            from_point: The QPointF representing the center point to move from.
                Defaults to the current center point of the passed items.
        """
        if from_point is ...:
            center_point = self.get_items_center_point(items)
            if not center_point:
                return
            else:
                from_point = center_point
        for item in items:
            x_offset = item.x() - from_point.x()
            y_offset = item.y() - from_point.y()
            x = to_point.x() + x_offset
            y = to_point.y() + y_offset
            item.setPos(QPointF(x, y))

    def on_selection_change(self) -> None:
        """Enables/disables View actions based on currently selected items."""
        is_node_selected = False
        is_connection_selected = False
        for item in self.selectedItems():
            if isinstance(item, Node):
                is_node_selected = True
                break
            elif isinstance(item, Connection):
                is_connection_selected = True
        view = self.views()[0]
        view.action_cut.setEnabled(is_node_selected)
        view.action_copy.setEnabled(is_node_selected)
        view.action_delete.setEnabled(is_node_selected or is_connection_selected)

    def deselect_all(self, items: 'Iterable[QGraphicsItem]' = ...) -> None:
        """Deselects all items in the items arg.
        
        If the items arg is not provided, will deselect all currently selected
        items.

        Args:
            items: The list of QGraphicsItems to deselect. Defaults to
                currently selected items.
        """
        if items is ...:
            items = self.selectedItems()
        for item in items:
            item.setSelected(False)
    
    def select_all(self, items: 'Iterable[QGraphicsItem]' = ...) -> None:
        """Deselects all items in the items arg.
        
        If the items arg is not provided, will select all items in the Scene.

        Args:
            items: The list of QGraphicsItems to select. Defaults to currently 
                selected items.
        """
        if items is ...:
            items = self.items()
        for item in items:
            item.setSelected(True)
