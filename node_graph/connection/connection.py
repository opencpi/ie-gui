from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Dict, List, Any, Optional
    import xml.dom.minidom as MD
    import xml.etree.ElementTree as ET

from PyQt5.QtGui import QPen, QColor, QPainterPath
from PyQt5.QtCore import Qt, QRectF
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsPathItem, QStyle
if TYPE_CHECKING:
    from PyQt5.QtCore import QPointF
    from PyQt5.QtGui import QPainter
    from PyQt5.QtWidgets import QStyleOptionGraphicsItem, QWidget

from .model import ConnectionModel
from .properties_widget import ConnectionPropertiesWidget, ConnectionPropertiesWidget
from base_classes import BaseGraphicsItem
from port import Port
if TYPE_CHECKING:
    from node_editor.scene import Scene

class Line(QGraphicsPathItem):
    """Graphical representation of a line between two QPointF objects.

    This class is used to graphically create a fake "connection" before being
    connected to two Ports.

    Attributes:
        is_highlighted: Whether the line is hovered over by cursor.
    """
    _snap_range = 15
    def __init__(self, start_pos: 'QPointF', end_pos: 'QPointF', 
                 style=Qt.SolidLine, color=Qt.black) -> None:
        super().__init__()
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.color = color
        self.style = style
        _painter_path = self._init_path()
        if _painter_path is not None:
            self.setPath(_painter_path)
        self.setZValue(-1)
        self._size = 5
        self.is_highlighted = False

    @property
    def start_pos(self) -> 'Optional[QPointF]':
        """QPointF representing the starting position of the line."""
        return self._start_pos

    @start_pos.setter
    def start_pos(self, start_pos: 'Optional[QPointF]') -> None:
        self._start_pos = start_pos
        if self.start_pos and self.path().elementCount() > 0:
            self._update_path(0, self.start_pos)

    @property
    def end_pos(self) -> 'Optional[QPointF]':
        """QPointF representing the ending position of the line."""
        return self._end_pos

    @end_pos.setter
    def end_pos(self, end_pos: 'Optional[QPointF]') -> None:
        self._end_pos = end_pos
        if self.end_pos is not None and self.path().elementCount() > 1:
            self._update_path(1, self.end_pos)

    @property
    def scene(self) -> 'Scene':
        """The line's Scene."""
        return super().scene() #type: ignore

    def disconnect(self) -> None:
        """Sets start_pos and end_pos attributes to None."""
        self.start_pos = None
        self.end_pos = None

    def delete(self) -> None:
        """Removes Line from Scene and unsets positions.

        Calls disconnect() to set positions to None.
        """
        if self.scene:
            self.scene.remove_connection(self)
        self.disconnect()

    def _init_path(self) -> 'Optional[QPainterPath]':
        """Creates a QPainter with self.start_pos and self.end_pos.

        Returns:
            A QPainterPath extending from start_pos to end_pos if both are set;
            otherwise None.
        """
        if self.start_pos is None or self.end_pos is None:
            return None
        path = QPainterPath(self.start_pos)
        path.lineTo(self.end_pos)
        return path

    def _update_path(self, idx: int, pos: 'QPointF') -> None:
        """Updates QPainterPath at idx with pos."""
        path = self.path()
        path.setElementPositionAt(idx, pos.x(), pos.y())
        self.setPath(path)

    def paint(
        self, 
        painter: 'QPainter', 
        option: 'QStyleOptionGraphicsItem', 
        widget: 'QWidget'
    ) -> None:
        """Required to be implemented by QT for a QGraphicsItem.
        
        Also sets color of Connection dependant on whether Connection is 
        currently highlighted, selected, or neither.

        Args:
            painter: Required for call to super.
            option: Required for call to super.
            widget: Required for call to super.
        """
        option.state &= ~QStyle.State_Selected # type: ignore
        super().paint(painter, option, widget)
        if self.is_highlighted:
            self.setPen(QPen(QColor("#FFFFFF"), self._size*1.5, style=self.style))
        elif self.isSelected():
            self.setPen(QPen(QColor("#0FFF50"), self._size*1.5, style=self.style))
        else:
            self.setPen(QPen(self.color, self._size, style=self.style))
        
    def boundingRect(self) -> QRectF:
        """Required to be implemented by QT for a QGraphicsItem. 

        Returns:
            QRectF from call to super method.
        """
        return super().boundingRect()

class Connection(Line, BaseGraphicsItem):
    """Graphical representation of a connection between to Ports."""
    def __init__(
        self, 
        consumer_port: Port, 
        producer_port: Port, 
        model: 'Optional[ConnectionModel]' = ...,
    ) -> None:
        super().__init__(consumer_port.scene_pos, producer_port.scene_pos)
        if model is ... or model is None:
            model = ConnectionModel()
        self._model: ConnectionModel = model
        self._properties_widget = None
        self._consumer_port = consumer_port
        self._producer_port = producer_port
        self.consumer_port = consumer_port
        self.producer_port = producer_port
        self.setFlag(QGraphicsItem.ItemIsSelectable)

    @property
    def properties_widget(self) -> ConnectionPropertiesWidget:
        """QWidget containing the connection's properties."""
        if self._properties_widget: return self._properties_widget
        self._properties_widget = ConnectionPropertiesWidget(self)
        return self._properties_widget

    @property
    def ports(self) -> 'List[Optional[Port]]':
        """List containing consumer_port and producer_port respectively."""
        return [self.consumer_port, self.producer_port]

    @property
    def consumer_port(self) -> 'Optional[Port]':
        """The Port representing the consumer port of the connection."""
        return self._consumer_port

    @consumer_port.setter
    def consumer_port(self, port) -> None:
        if self.consumer_port:
        # Unset property widgets
            self.consumer_port.connection = None
            self.properties_widget.consumer_instance_widget.setText('')
            self.properties_widget.consumer_component_widget.setText('')
            self.properties_widget.consumer_port_widget.setText('')
        self._consumer_port = port
        if self.consumer_port is not None:
        # Set property widgets
            self.consumer_port.connection = self
            self.properties_widget.consumer_instance_widget.setText(
                f'Instance: {self.consumer_port.node.name}')
            self.properties_widget.consumer_component_widget.setText(
                f'Component: {self.consumer_port.node.component}')
            self.properties_widget.consumer_port_widget.setText(f'Port: {self.consumer_port.name}')
            self.end_pos = self.consumer_port.scene_pos

    @property
    def producer_port(self) -> 'Optional[Port]':
        """The Port representing the consumer port of the connection."""
        return self._producer_port

    @producer_port.setter
    def producer_port(self, port) -> None:
        if self.producer_port:
        # Unset property widgets
            self.producer_port.connection = None
            self.properties_widget.producer_component_widget.setText('')
            self.properties_widget.producer_port_widget.setText('')
        self._producer_port = port
        if self.producer_port is not None:
        # Set property widgets
            self.producer_port.connection = self
            self.properties_widget.producer_instance_widget.setText(
                f'Instance: {self.producer_port.node.name}')
            self.properties_widget.producer_component_widget.setText(
                f'Component: {self.producer_port.node.component}')
            self.properties_widget.producer_port_widget.setText(f'Port: {self.producer_port.name}')
            self.start_pos = self.producer_port.scene_pos

    def get_other_port(self, port: Port) -> 'Optional[Port]':
        """Returns Port on the opposite end of the connection from port arg.

        Args:
            port: Port to find the opposite of in the connection.

        Returns:
            The Port opposite the port arg if there is one; otherwise None.
        """
        if port is self.consumer_port:
            return self.producer_port
        if port is self.producer_port:
            return self.consumer_port
        return None

    def disconnect(self) -> None:
        """Sets the producer_port and consumer_port attributes to None."""
        self.producer_port = None
        self.consumer_port = None
    
    def update(self) -> None:
        """Resets start_pos and end_pos properties of the connection.
        
        Causes the QPainterPath to be re-drawn. Useful if the positions of
        either end of the connection are moved from the positions of the Port
        attributes, such as when dragging a connection from one Port to 
        another.
        """
        if self.producer_port is not None:
            self.start_pos = self.producer_port.scene_pos
        if self.consumer_port is not None:
            self.end_pos = self.consumer_port.scene_pos

    def copy(self, ports: 'Optional[List[Port]]' = None) -> 'Optional[Connection]':
        """Returns a copy of the Connection.
        
        Gets a dictionary representation of the Connection by calling the
        to_dict() method, then passes the dictionary to the from_dict() method
        to make the copy.

        Args:
            ports: The Ports for the Connection to be added to.
        """
        copy = self.from_dict(self.to_dict(), scene=self.scene, ports=ports)
        return copy

    @classmethod
    def find_nearest_port(cls, scene: 'Scene', pos: 'QPointF') -> 'Optional[Port]':
        """Return the Port that is nearest to the pos arg in the scene arg.

        Args:
            scene: The Scene to search within for the nearest Port
            pos: The position to find the nearest Port to

        Returns:
            Nearest Port to the pos arg in the scene arg if there is one within 
            range; otherwise None.
        """
        scan_rect = QRectF(
            pos.x() - cls._snap_range, 
            pos.y() - cls._snap_range, 
            cls._snap_range * 2, 
            cls._snap_range * 2
        )
        items = scene.items(scan_rect)
        nearest_port = None
        nearest = 10000000000
        for port in items:
            if not isinstance(port, Port):
                continue
            qpdist = port.scenePos() - pos #type: ignore
            dist = qpdist.x() * qpdist.x() + qpdist.y() * qpdist.y()
            if dist < nearest:
                nearest_port, nearest = port, dist
        return nearest_port

    @classmethod
    def is_valid_connection(
        cls, 
        port1: QGraphicsItem, 
        port2: QGraphicsItem, 
        reconnection: 'Optional[Connection]' = None
    ) -> bool:
        """Determines whether a Connection between two Ports would be valid.

        If changing the producer Port in a Connection, the original 
        Connection can be passed to allow the consumer Port to ignore the
        fact that it already contains a Connection.

        Args:
            port1: One of two Ports to determine if a Connection would be 
                valid for.
            port2: One of two Ports to determine if a Connection would be 
                valid for.
            reconnection: The original Connection if validating a Port change
                for an existing Connection.

        Returns:
            Whether a Connection between two Port args would be valid.
        """
        if not isinstance(port1, Port) or not isinstance(port2, Port):
        # Ensure both ports are in facts Ports
            return False
        if port1.node is port2.node:
        # Don't allow ports to connect to ports in same node
            return False
        if port1.type == port2.type:
        # Ensure only in-to-out (or vice versa) port connection
            return False
        for port in [port1, port2]:
            if port.connection:
            # Ensure no other connections exist in port
                if not reconnection or port not in reconnection.ports:
                # If we're reconnecting, it's ok for port to contain
                # a connection if it was part of the original connection
                    return False
        return True

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the Connection to a dictionary representation.

        Calls _model.to_dict() to get a dictionary representation of the
        ConnectionModel.

        Returns:
            A Dictionary representation of the Connection.
        """
        out_dict = {'model': self._model.to_dict()}
        if self.producer_port:
            out_dict['producer'] = self.producer_port.to_dict()
        if self.consumer_port:
            out_dict['consumer'] = self.consumer_port.to_dict()
        return out_dict

    @classmethod
    def from_dict(cls, in_dict: 'Dict[str, Any]', scene: 'Optional[Scene]' = None, 
        ports: 'Optional[List[Port]]' = None) -> 'Optional[Connection]':
        """Creates a Connection from a dictionary.

        Args:
            in_dict: The dictionary representation of a Connection.
            scene: The scene to add the Connection to. Used for looking 
                up Nodes within the scene to make the connection 
                between. If not provided, the ports arg must be 
                provided.
            ports: The Ports for the Connection. If not provided, will
                attempt to find Ports in the scene arg's node_dict
                attribute.

        Returns:
            A Connection based on the in_dict arg.
        """
        if ports and len(ports) == 2:
            if ports[0].is_consumer and ports[1].is_producer:
                consumer_port = ports[0]
                producer_port = ports[1]
            elif ports[1].is_consumer and ports[0].is_producer:
                consumer_port = ports[1]
                producer_port = ports[0]
            else:
                return None
        elif not scene:
            return None
        else:
            in_node = scene.nodes_dict.get(in_dict['consumer']['instance'], None)
            out_node = scene.nodes_dict.get(in_dict['producer']['instance'], None)
            if not in_node or not out_node:
                return None
            consumer_port = in_node.get_port(in_dict['consumer']['model']['name'])
            producer_port = out_node.get_port(in_dict['producer']['model']['name'])
        if not consumer_port or not producer_port:
            return None
        model = ConnectionModel.from_dict(in_dict['model'])
        return cls(consumer_port, producer_port, model=model)

    def to_xml(self, root: 'MD.Document') -> 'MD.Element':
        """Converts the Connection to an xml minidom Element.

        Args:
            root: The minidom Document to add the Connection and the Connection
                Ports to.

        Returns:
            An xml minidom Element representation of the Connection.
        """
        connection_dict = self.to_dict()
        buffer_size = connection_dict['model']['properties']['bufferSize']['value']
        xml = root.createElement('connection')
        if buffer_size:
            xml.setAttribute('bufferSize', buffer_size)
        for port_type in ['producer', 'consumer']:
            port = root.createElement('port')
            instance = connection_dict[port_type]['instance']
            name = connection_dict[port_type]['model']['name']
            port.setAttribute('Instance', instance)
            port.setAttribute('Name', name)
            props_dict = connection_dict['model']['properties']
            buffer_count = props_dict[f'{port_type}_bufferCount']['value']
            if buffer_count:
                port.setAttribute('bufferCount', buffer_count)
            xml.appendChild(port)
        return xml

    @classmethod
    def from_xml(cls, element: 'ET.Element', scene: 'Scene') -> 'Optional[Connection]':
        """Creates a Connection from an xml minidom Element.

        Args:
            element: The xml minidom Element representation of a Connection.
            scene: The scene to add the Connection to. Used for looking up
                Nodes within the scene to make the connection between.

        Returns:
            A Connection based on the element arg.
        """
        prop_dict = {}
        in_port = out_port = None
        if element.attrib:
            element.attrib = dict((k.lower(), v) for k,v in element.attrib.items())
            bad_attributes = [x for x in element.attrib if x not in ['buffersize']]
            if bad_attributes:  
                raise Exception(f"Unsupported attributes: {', '.join(bad_attributes)}")
        buffer_size = element.attrib.get('buffersize', '')
        prop_dict['bufferSize'] = {'value': buffer_size}
        children = element.getchildren()
        for child in children:
            child.attrib = dict((k.lower(), v) for k,v in child.attrib.items())
            bad_attributes = [x for x in child.attrib 
                              if x not in ['instance', 'name', 'buffercount']]
            if bad_attributes:  
                raise Exception(f"Unsupported attributes: {', '.join(bad_attributes)}")
            node = scene.nodes_dict.get(child.attrib['instance'], None)
            port_name = f'{child.attrib["name"]}_{child.tag}'
            prop_dict[port_name] = child.attrib
            if node is not None:
                port = node.get_port(child.attrib['name'])
                if not port:
                    continue
                if port.is_consumer:
                    in_port = port
                elif port.is_producer:
                    out_port = port
                buffer_count = child.attrib.get('buffercount', '')
                prop_dict[f'{port.type}_bufferCount'] = {'value': buffer_count}
            else:
                raise Exception(f'Error: Unknown component instance: {child.attrib["instance"]}')
        if in_port and out_port:
            model = ConnectionModel.from_dict({'properties': prop_dict})
            return cls(in_port, out_port, model=model)
        return None
