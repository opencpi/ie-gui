"""Contains classes that represent a connection between nodes or points.

Line: Graphical representation of the path between two QPointF positions. 
Connection: Extends the Line class to graphically represent the path between
    two Port objects.

Typical usage:
    Create a Line from two QPointF positions:
        line = Line(QPointF(...), QPointF(...))
    Update Line position:
        line.start_pos = QPointF(...)
    Create a Connection using the default model and two Ports:
        connection = Connection(Port(...), Port(...))
    Create a Connection from a dictionary representation:
        connection = Connection.from_dict(dictionary)
    Convert a Connection to a dictionary representation:
        dictionary = connection.to_dict()
    Change Connection's Ports:
        connection.consumer_port = Port(...)
        connection.producer_port = Port(...)
"""

from .connection import Line, Connection
