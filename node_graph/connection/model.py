from copy import deepcopy
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Dict, Any, Optional

from base_classes import BaseModel

class ConnectionModel(BaseModel):
    """Models the data of a Connection.

    Attributes:
        properties: A dictionary containing the ConnectionModel's properties.
    """
    def __init__(self, properties: 'Optional[Dict[str, Dict[str, str]]]' = ...) -> None:
        super().__init__()
        if properties is ... or properties is None:
            properties = {
                'bufferSize': {'value': ''},
                'producer_bufferCount': {'value': ''},
                'consumer_bufferCount': {'value': ''}
            }
        self.properties = properties

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the ConnectionModel to a dictionary representation.

        Returns:
            A copy of the ConnectionModel's properties attribute.
        """
        return {'properties': deepcopy(self.properties)}

    @classmethod
    def from_dict(cls, in_dict: 'Dict[str, Dict[str, Dict[str, str]]]') -> 'ConnectionModel':
        """Creates a ConnectionModel from a dictionary.

        Args:
            in_dict: The dictionary representation of a ConnectionModel.

        Returns:
            A ConnectionModel based on the in_dict arg.
        """
        properties = in_dict['properties']
        return cls(properties)
